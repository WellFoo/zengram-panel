Share = {
	vkontakte: function(purl, ptitle, pimg, text) {
		var url  = 'http://vkontakte.ru/share.php?';
		url += 'url='          + encodeURIComponent(purl);
		url += '&title='       + encodeURIComponent(ptitle);
		url += '&description=' + encodeURIComponent(text);
		url += '&image='       + encodeURIComponent(pimg);
		url += '&noparse=true';
		url += '&app=4987163';
		Share.popup(url, 'vkontakte');
	},
	odnoklassniki: function(purl, text) {
		var url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
		url += '&st.comments=' + encodeURIComponent(text);
		url += '&st._surl='    + encodeURIComponent(purl);
		Share.popup(url, 'odnoklassniki');
	},
	facebook: function(purl, ptitle, pimg, text) {
		var url  = 'http://www.facebook.com/sharer.php?s=100';
		url += '&p[title]='     + encodeURIComponent(ptitle);
		url += '&p[summary]='   + encodeURIComponent(text);
		url += '&p[url]='       + encodeURIComponent(purl);
		url += '&p[images][0]=' + encodeURIComponent(pimg);
		Share.popup(url, 'facebook');
	},
	twitter: function(purl, ptitle) {
		var url  = 'http://twitter.com/share?';
		url += 'text='      + encodeURIComponent(ptitle);
		url += '&url='      + encodeURIComponent(purl);
		url += '&counturl=' + encodeURIComponent(purl);
		Share.popup(url, 'twitter');
	},
	mailru: function(purl, ptitle, pimg, text) {
		var url  = 'http://connect.mail.ru/share?';
		url += 'url='          + encodeURIComponent(purl);
		url += '&title='       + encodeURIComponent(ptitle);
		url += '&description=' + encodeURIComponent(text);
		url += '&imageurl='    + encodeURIComponent(pimg);
		Share.popup(url, 'mailru')
	},

	popup: function(url, soc) {
		window.open(url,'','toolbar=0,status=0,width=626,height=436');
		$.post('/actions/share', {social:soc}, function (data){});
		$('#action').hide('hide');
		$('#actionInfo').modal('hide');
	}
};

function closeAction(){
	$('#action').hide('hide');
	$.post('/actions/close', function (data){});
}

jQuery(function($){
	var $button = $('#action-paste-ad');
	var actionRun = false;

	$button.on('click', function(){
		if (actionRun) {
			return;
		}
		actionRun = true;
		$button.addClass('disabled').css({cursor: 'progress'});

		$.getJSON('/actions/post-ad', function(response){
			if (response.status === 'ok') {
				updateBalance();
				$('#actionInfo').modal('hide');
				$('#action').remove();
			} else {
				$('#actionInfoError')
					.text(response.message)
					.show();
			}
		}).always(function(){
			actionRun = false;
			$button.removeClass('disabled').css({cursor: 'pointer'});
		});
	});
});