function loadMore(id, max_id)
{
	max_id = $('#media-list .modal.instagram-media-modal').last().data('mediaid');
	$.ajax({
		url: "/content/more/" + id,
		data: {'max_id': max_id, filter: $('#media_filter').data('filter')},
		beforeSend: function () {
			$('.panel-more button').hide();
			$('.panel-more .loading-more').show();
		},
		success: function (data) {
			$('.panel-medias').append(data);
			$('.instagram-comment:not(.is-truncated)').dotdotdot({
				callback: function (isTruncated, orgContent) {
					if (isTruncated) {
						$(this).prop('title', orgContent);
					}
				},
				height: 35
			});
			$(window).trigger('media-list-loaded');
		}
	}).done(function () {
		$('#showMedia' + id + ' .next').show();
		$('.comment-text').minEmoji();
		$('.panel-more button').show();
		$('.panel-more .loading-more').hide();
		navButtons();
	});
}

function loadMoreComments(id, media_id, max_id)
{
	var wrapper = $('#modal-wrapper' + media_id);
	$.ajax({
		url: "/content/comments/" + id,
		data: {'media_id': media_id, max_id: max_id},
		beforeSend: function () {
			wrapper.find('.panel-more-comments button').hide();
			wrapper.find('.panel-more-comments .loading-more-comments').show();
		},
		success: function (data) {
			var commentWrapper = $('#commentWrapper' + media_id);
			commentWrapper.find('.panel-more-comments').remove();
			commentWrapper.find('.instagram-photo-title').after(data);
			commentWrapper.find('div.comment-text:not(.emoji)').minEmoji().addClass('emoji');
			$('.next').show();
			setTimeout(function() {
				commentWrapper.find('.instagram-modal-comment').each(function(){
					if ($(this).data('comment-id')) {
						commentWrapper.find('.instagram-modal-comment[data-comment-id="' + $(this).data('comment-id') + '"]').slice(1).remove();
					}
				});
				commentWrapper.find('.commenter-login').dotdotdot();
			}, 200);
		},
		error: function () {
			wrapper.find('.panel-more-comments button').show();
			wrapper.find('.panel-more-comments .loading-more-comments').hide();
		}
	});
}

function sendComment(id, media_id)
{
	var input = $('#modal-comment-input' + media_id);
	var wrapper = $('#modal-wrapper' + media_id);
	var clone = input.clone();
	clone.find('img.repl-img').each(function (x, y) {
		$(y).replaceWith($(y).data('code'));
	});
	$.ajax({
		url: "/content/comment/" + id,
		data: {'media_id': media_id, comment: clone.text()},
		method: 'post',
		beforeSend: function () {
			wrapper.find('.modal-send-comment button').hide();
			wrapper.find('.modal-send-comment .loading-send').show();
			$('#send-comment-error' + media_id).hide();
		},
		success: function (data) {
			if (data.indexOf(window.Zengram.t('Instagram error.')) === -1) {
				if (data.indexOf(window.Zengram.t('Error')) === 0) {
					$('#send-comment-error' + media_id).html(data).show();
				} else {
					var commentWrapper = $('#commentWrapper' + media_id);
					commentWrapper.html(data);
					commentWrapper.find('div.comment-text').minEmoji().addClass('emoji');
					input.html('');
				}
			} else {
				if (data.indexOf('checkpoint_required') !== -1) {
					$("#checkpointError").modal('show');
				} else if (data.indexOf('Invalid username/password') !== -1) {
					$("#passwordError").modal('show');
				} else {
					$('#send-comment-error' + media_id).html(data).show();
				}
			}
		},
		complete: function () {
			wrapper.find('.modal-send-comment button').show();
			wrapper.find('.modal-send-comment .loading-send').hide();
		}
	});
}

function quoteUser(media_id, username)
{
	var input = $('#modal-comment-input' + media_id);
	input.prepend(username + ' ')
}

function sendImage()
{
	var $form = $('#photo-upload');
	var $input = $form.find('#image-upload-input');
	var id = $form.find('#upload-id').val();
	var $container = $form.find('#container_image');
	$.ajax({
		url: "/content/images/" + id,
		data: {'image': $form.find('#container_image').cropit('export'), caption: $input.val()},
		method: 'post',
		dataType: 'json',
		beforeSend: function () {
			$container.find('button').hide();
			$container.find('.loading-send').show();
			$form.find('#upload-image-alert').hide().removeClass('alert-danger alert-success');
		},
		success: function (data) {
			if (data.error == undefined) {
				$form.find('#upload-image-alert').show().addClass('alert-success').html(window.Zengram.t('Image uploaded'));
				$input.val('');
			} else {
				$form.modal('hide');
				updateAccountError(id, data.error, true);
			}
		},
		complete: function () {
			$container.find('button').show();
			$container.find('.loading-send').hide();
		}
	});
}

$(document).ready(function () {
	var all_emojis = '\ud83d\ude04\ud83d\ude03\ud83d\ude00\ud83d\ude0a\u263a\ufe0f\ud83d\ude09\ud83d\ude0d\ud83d\ude18\ud83d\ude1a\ud83d\ude17\ud83d\ude19\ud83d\ude1c\ud83d\ude1d\ud83d\ude1b\ud83d\ude33\ud83d\ude01\ud83d\ude14\ud83d\ude0c\ud83d\ude12\ud83d\ude1e\ud83d\ude23\ud83d\ude22\ud83d\ude02\ud83d\ude2d\ud83d\ude2a\ud83d\ude25\ud83d\ude30\ud83d\ude05\ud83d\ude13\ud83d\ude29\ud83d\ude2b\ud83d\ude28\ud83d\ude31\ud83d\ude20\ud83d\ude21\ud83d\ude24\ud83d\ude16\ud83d\ude06\ud83d\ude0b\ud83d\ude37\ud83d\ude0e\ud83d\ude34\ud83d\ude35\ud83d\ude32\ud83d\ude1f\ud83d\ude26\ud83d\ude27\ud83d\ude08\ud83d\udc7f\ud83d\ude2e\ud83d\ude2c\ud83d\ude10\ud83d\ude15\ud83d\ude2f\ud83d\ude36\ud83d\ude07\ud83d\ude0f\ud83d\ude11\ud83d\udc72\ud83d\udc73\ud83d\udc6e\ud83d\udc77\ud83d\udc82\ud83d\udc76\ud83d\udc66\ud83d\udc67\ud83d\udc68\ud83d\udc69\ud83d\udc74\ud83d\udc75\ud83d\udc71\ud83d\udc7c\ud83d\udc78\ud83d\ude3a\ud83d\ude38\ud83d\ude3b\ud83d\ude3d\ud83d\ude3c\ud83d\ude40\ud83d\ude3f\ud83d\ude39\ud83d\ude3e\ud83d\udc79\ud83d\udc7a\ud83d\ude48\ud83d\ude49\ud83d\ude4a\ud83d\udc80\ud83d\udc7d\ud83d\udca9\ud83d\udd25\u2728\ud83c\udf1f\ud83d\udcab\ud83d\udca5\ud83d\udca2\ud83d\udca6\ud83d\udca7\ud83d\udca4\ud83d\udca8\ud83d\udc42\ud83d\udc40\ud83d\udc43\ud83d\udc45\ud83d\udc44\ud83d\udc4d\ud83d\udc4e\ud83d\udc4c\ud83d\udc4a\u270a\u270c\ufe0f\ud83d\udc4b\u270b\ud83d\udc50\ud83d\udc46\ud83d\udc47\ud83d\udc49\ud83d\udc48\ud83d\ude4c\ud83d\ude4f\u261d\ufe0f\ud83d\udc4f\ud83d\udcaa\ud83d\udeb6\ud83c\udfc3\ud83d\udc83\ud83d\udc6b\ud83d\udc6a\ud83d\udc6c\ud83d\udc6d\ud83d\udc8f\ud83d\udc91\ud83d\udc6f\ud83d\ude46\ud83d\ude45\ud83d\udc81\ud83d\ude4b\ud83d\udc86\ud83d\udc87\ud83d\udc85\ud83d\udc70\ud83d\ude4e\ud83d\ude4d\ud83d\ude47\ud83c\udfa9\ud83d\udc51\ud83d\udc52\ud83d\udc5f\ud83d\udc5e\ud83d\udc61\ud83d\udc60\ud83d\udc62\ud83d\udc55\ud83d\udc54\ud83d\udc5a\ud83d\udc57\ud83c\udfbd\ud83d\udc56\ud83d\udc58\ud83d\udc59\ud83d\udcbc\ud83d\udc5c\ud83d\udc5d\ud83d\udc5b\ud83d\udc53\ud83c\udf80\ud83c\udf02\ud83d\udc84\ud83d\udc9b\ud83d\udc99\ud83d\udc9c\ud83d\udc9a\u2764\ufe0f\ud83d\udc94\ud83d\udc97\ud83d\udc93\ud83d\udc95\ud83d\udc96\ud83d\udc9e\ud83d\udc98\ud83d\udc8c\ud83d\udc8b\ud83d\udc8d\ud83d\udc8e\ud83d\udc64\ud83d\udc65\ud83d\udcac\ud83d\udc63\ud83d\udcad\ud83d\udc36\ud83d\udc3a\ud83d\udc31\ud83d\udc2d\ud83d\udc39\ud83d\udc30\ud83d\udc38\ud83d\udc2f\ud83d\udc28\ud83d\udc3b\ud83d\udc37\ud83d\udc3d\ud83d\udc2e\ud83d\udc17\ud83d\udc35\ud83d\udc12\ud83d\udc34\ud83d\udc11\ud83d\udc18\ud83d\udc3c\ud83d\udc27\ud83d\udc26\ud83d\udc24\ud83d\udc25\ud83d\udc23\ud83d\udc14\ud83d\udc0d\ud83d\udc22\ud83d\udc1b\ud83d\udc1d\ud83d\udc1c\ud83d\udc1e\ud83d\udc0c\ud83d\udc19\ud83d\udc1a\ud83d\udc20\ud83d\udc1f\ud83d\udc2c\ud83d\udc33\ud83d\udc0b\ud83d\udc04\ud83d\udc0f\ud83d\udc00\ud83d\udc03\ud83d\udc05\ud83d\udc07\ud83d\udc09\ud83d\udc0e\ud83d\udc10\ud83d\udc13\ud83d\udc15\ud83d\udc16\ud83d\udc01\ud83d\udc02\ud83d\udc32\ud83d\udc21\ud83d\udc0a\ud83d\udc2b\ud83d\udc2a\ud83d\udc06\ud83d\udc08\ud83d\udc29\ud83d\udc3e\ud83d\udc90\ud83c\udf38\ud83c\udf37\ud83c\udf40\ud83c\udf39\ud83c\udf3b\ud83c\udf3a\ud83c\udf41\ud83c\udf43\ud83c\udf42\ud83c\udf3f\ud83c\udf3e\ud83c\udf44\ud83c\udf35\ud83c\udf34\ud83c\udf32\ud83c\udf33\ud83c\udf30\ud83c\udf31\ud83c\udf3c\ud83c\udf10\ud83c\udf1e\ud83c\udf1d\ud83c\udf1a\ud83c\udf11\ud83c\udf12\ud83c\udf13\ud83c\udf14\ud83c\udf15\ud83c\udf16\ud83c\udf17\ud83c\udf18\ud83c\udf1c\ud83c\udf1b\ud83c\udf19\ud83c\udf0d\ud83c\udf0e\ud83c\udf0f\ud83c\udf0b\ud83c\udf0c\ud83c\udf20\u2b50\ufe0f\u2600\ufe0f\u26c5\ufe0f\u2601\ufe0f\u26a1\ufe0f\u2614\ufe0f\u2744\ufe0f\u26c4\ufe0f\ud83c\udf00\ud83c\udf01\ud83c\udf08\ud83c\udf0a\ud83c\udf8d\ud83d\udc9d\ud83c\udf8e\ud83c\udf92\ud83c\udf93\ud83c\udf8f\ud83c\udf86\ud83c\udf87\ud83c\udf90\ud83c\udf91\ud83c\udf83\ud83d\udc7b\ud83c\udf85\ud83c\udf84\ud83c\udf81\ud83c\udf8b\ud83c\udf89\ud83c\udf8a\ud83c\udf88\ud83c\udf8c\ud83d\udd2e\ud83c\udfa5\ud83d\udcf7\ud83d\udcf9\ud83d\udcfc\ud83d\udcbf\ud83d\udcc0\ud83d\udcbd\ud83d\udcbe\ud83d\udcbb\ud83d\udcf1\u260e\ufe0f\ud83d\udcde\ud83d\udcdf\ud83d\udce0\ud83d\udce1\ud83d\udcfa\ud83d\udcfb\ud83d\udd0a\ud83d\udd09\ud83d\udd08\ud83d\udd07\ud83d\udd14\ud83d\udd15\ud83d\udce2\ud83d\udce3\u23f3\u231b\ufe0f\u23f0\u231a\ufe0f\ud83d\udd13\ud83d\udd12\ud83d\udd0f\ud83d\udd10\ud83d\udd11\ud83d\udd0e\ud83d\udca1\ud83d\udd26\ud83d\udd06\ud83d\udd05\ud83d\udd0c\ud83d\udd0b\ud83d\udd0d\ud83d\udec1\ud83d\udec0\ud83d\udebf\ud83d\udebd\ud83d\udd27\ud83d\udd29\ud83d\udd28\ud83d\udeaa\ud83d\udeac\ud83d\udca3\ud83d\udd2b\ud83d\udd2a\ud83d\udc8a\ud83d\udc89\ud83d\udcb0\ud83d\udcb4\ud83d\udcb5\ud83d\udcb7\ud83d\udcb6\ud83d\udcb3\ud83d\udcb8\ud83d\udcf2\ud83d\udce7\ud83d\udce5\ud83d\udce4\u2709\ufe0f\ud83d\udce9\ud83d\udce8\ud83d\udcef\ud83d\udceb\ud83d\udcea\ud83d\udcec\ud83d\udced\ud83d\udcee\ud83d\udce6\ud83d\udcdd\ud83d\udcc4\ud83d\udcc3\ud83d\udcd1\ud83d\udcca\ud83d\udcc8\ud83d\udcc9\ud83d\udcdc\ud83d\udccb\ud83d\udcc5\ud83d\udcc6\ud83d\udcc7\ud83d\udcc1\ud83d\udcc2\u2702\ufe0f\ud83d\udccc\ud83d\udcce\u2712\ufe0f\u270f\ufe0f\ud83d\udccf\ud83d\udcd0\ud83d\udcd5\ud83d\udcd7\ud83d\udcd8\ud83d\udcd9\ud83d\udcd3\ud83d\udcd4\ud83d\udcd2\ud83d\udcda\ud83d\udcd6\ud83d\udd16\ud83d\udcdb\ud83d\udd2c\ud83d\udd2d\ud83d\udcf0\ud83c\udfa8\ud83c\udfac\ud83c\udfa4\ud83c\udfa7\ud83c\udfbc\ud83c\udfb5\ud83c\udfb6\ud83c\udfb9\ud83c\udfbb\ud83c\udfba\ud83c\udfb7\ud83c\udfb8\ud83d\udc7e\ud83c\udfae\ud83c\udccf\ud83c\udfb4\ud83c\udc04\ufe0f\ud83c\udfb2\ud83c\udfaf\ud83c\udfc8\ud83c\udfc0\u26bd\ufe0f\u26be\ufe0f\ud83c\udfbe\ud83c\udfb1\ud83c\udfc9\ud83c\udfb3\u26f3\ufe0f\ud83d\udeb5\ud83d\udeb4\ud83c\udfc1\ud83c\udfc7\ud83c\udfc6\ud83c\udfbf\ud83c\udfc2\ud83c\udfca\ud83c\udfc4\ud83c\udfa3\u2615\ufe0f\ud83c\udf75\ud83c\udf76\ud83c\udf7c\ud83c\udf7a\ud83c\udf7b\ud83c\udf78\ud83c\udf79\ud83c\udf77\ud83c\udf74\ud83c\udf55\ud83c\udf54\ud83c\udf5f\ud83c\udf57\ud83c\udf56\ud83c\udf5d\ud83c\udf5b\ud83c\udf64\ud83c\udf71\ud83c\udf63\ud83c\udf65\ud83c\udf59\ud83c\udf58\ud83c\udf5a\ud83c\udf5c\ud83c\udf72\ud83c\udf62\ud83c\udf61\ud83c\udf73\ud83c\udf5e\ud83c\udf69\ud83c\udf6e\ud83c\udf66\ud83c\udf68\ud83c\udf67\ud83c\udf82\ud83c\udf70\ud83c\udf6a\ud83c\udf6b\ud83c\udf6c\ud83c\udf6d\ud83c\udf6f\ud83c\udf4e\ud83c\udf4f\ud83c\udf4a\ud83c\udf4b\ud83c\udf52\ud83c\udf47\ud83c\udf49\ud83c\udf53\ud83c\udf51\ud83c\udf48\ud83c\udf4c\ud83c\udf50\ud83c\udf4d\ud83c\udf60\ud83c\udf46\ud83c\udf45\ud83c\udf3d\ud83c\udfe0\ud83c\udfe1\ud83c\udfeb\ud83c\udfe2\ud83c\udfe3\ud83c\udfe5\ud83c\udfe6\ud83c\udfea\ud83c\udfe9\ud83c\udfe8\ud83d\udc92\u26ea\ufe0f\ud83c\udfec\ud83c\udfe4\ud83c\udf07\ud83c\udf06\ud83c\udfef\ud83c\udff0\u26fa\ufe0f\ud83c\udfed\ud83d\uddfc\ud83d\uddfe\ud83d\uddfb\ud83c\udf04\ud83c\udf05\ud83c\udf03\ud83d\uddfd\ud83c\udf09\ud83c\udfa0\ud83c\udfa1\u26f2\ufe0f\ud83c\udfa2\ud83d\udea2\u26f5\ufe0f\ud83d\udea4\ud83d\udea3\u2693\ufe0f\ud83d\ude80\u2708\ufe0f\ud83d\udcba\ud83d\ude81\ud83d\ude82\ud83d\ude8a\ud83d\ude89\ud83d\ude9e\ud83d\ude86\ud83d\ude84\ud83d\ude85\ud83d\ude88\ud83d\ude87\ud83d\ude9d\ud83d\ude8b\ud83d\ude83\ud83d\ude8e\ud83d\ude8c\ud83d\ude8d\ud83d\ude99\ud83d\ude98\ud83d\ude97\ud83d\ude95\ud83d\ude96\ud83d\ude9b\ud83d\ude9a\ud83d\udea8\ud83d\ude93\ud83d\ude94\ud83d\ude92\ud83d\ude91\ud83d\ude90\ud83d\udeb2\ud83d\udea1\ud83d\ude9f\ud83d\udea0\ud83d\ude9c\ud83d\udc88\ud83d\ude8f\ud83c\udfab\ud83d\udea6\ud83d\udea5\u26a0\ufe0f\ud83d\udea7\ud83d\udd30\u26fd\ufe0f\ud83c\udfee\ud83c\udfb0\u2668\ufe0f\ud83d\uddff\ud83c\udfaa\ud83c\udfad\ud83d\udccd\ud83d\udea9\ud83c\uddef\ud83c\uddf5\ud83c\uddf0\ud83c\uddf7\ud83c\udde9\ud83c\uddea\ud83c\udde8\ud83c\uddf3\ud83c\uddfa\ud83c\uddf8\ud83c\uddeb\ud83c\uddf7\ud83c\uddea\ud83c\uddf8\ud83c\uddee\ud83c\uddf9\ud83c\uddf7\ud83c\uddfa\ud83c\uddec\ud83c\udde7\n1\u20e32\u20e33\u20e34\u20e35\u20e36\u20e37\u20e3\n8\u20e39\u20e30\u20e3\ud83d\udd1f\ud83d\udd22#\u20e3\ud83d\udd23\u2b06\ufe0f\u2b07\ufe0f\u2b05\ufe0f\u27a1\ufe0f\ud83d\udd20\ud83d\udd21\ud83d\udd24\u2197\ufe0f\u2196\ufe0f\u2198\ufe0f\u2199\ufe0f\u2194\ufe0f\u2195\ufe0f\ud83d\udd04\u25c0\ufe0f\u25b6\ufe0f\ud83d\udd3c\ud83d\udd3d\u21a9\ufe0f\u21aa\ufe0f\u2139\ufe0f\u23ea\u23e9\u23eb\u23ec\u2935\ufe0f\u2934\ufe0f\ud83c\udd97\ud83d\udd00\ud83d\udd01\ud83d\udd02\ud83c\udd95\ud83c\udd99\ud83c\udd92\ud83c\udd93\ud83c\udd96\ud83d\udcf6\ud83c\udfa6\ud83c\ude01\ud83c\ude2f\ufe0f\ud83c\ude33\ud83c\ude35\ud83c\ude34\ud83c\ude32\ud83c\ude50\ud83c\ude39\ud83c\ude3a\ud83c\ude36\ud83c\ude1a\ufe0f\ud83d\udebb\ud83d\udeb9\ud83d\udeba\ud83d\udebc\ud83d\udebe\ud83d\udeb0\ud83d\udeae\ud83c\udd7f\ufe0f\u267f\ufe0f\ud83d\udead\ud83c\ude37\ud83c\ude38\ud83c\ude02\u24c2\ufe0f\ud83d\udec2\ud83d\udec4\ud83d\udec5\ud83d\udec3\ud83c\ude51\u3299\ufe0f\u3297\ufe0f\ud83c\udd91\ud83c\udd98\ud83c\udd94\ud83d\udeab\ud83d\udd1e\ud83d\udcf5\ud83d\udeaf\ud83d\udeb1\ud83d\udeb3\ud83d\udeb7\ud83d\udeb8\u26d4\ufe0f\u2733\ufe0f\u2747\ufe0f\u274e\u2705\u2734\ufe0f\ud83d\udc9f\ud83c\udd9a\ud83d\udcf3\ud83d\udcf4\ud83c\udd70\ud83c\udd71\ud83c\udd8e\ud83c\udd7e\ud83d\udca0\u27bf\u267b\ufe0f\u2648\ufe0f\u2649\ufe0f\u264a\ufe0f\u264b\ufe0f\u264c\ufe0f\u264d\ufe0f\u264e\ufe0f\u264f\ufe0f\u2650\ufe0f\u2651\ufe0f\u2652\ufe0f\u2653\ufe0f\u26ce\ud83d\udd2f\ud83c\udfe7\ud83d\udcb9\ud83d\udcb2\ud83d\udcb1\u00a9\u00ae\u2122\u274c\u203c\ufe0f\u2049\ufe0f\u2757\ufe0f\u2753\u2755\u2754\u2b55\ufe0f\ud83d\udd1d\ud83d\udd1a\ud83d\udd19\ud83d\udd1b\ud83d\udd1c\ud83d\udd03\ud83d\udd5b\ud83d\udd67\ud83d\udd50\ud83d\udd5c\ud83d\udd51\ud83d\udd5d\ud83d\udd52\ud83d\udd5e\ud83d\udd53\ud83d\udd5f\ud83d\udd54\ud83d\udd60\ud83d\udd55\ud83d\udd56\ud83d\udd57\ud83d\udd58\ud83d\udd59\ud83d\udd5a\ud83d\udd61\ud83d\udd62\ud83d\udd63\ud83d\udd64\ud83d\udd65\ud83d\udd66\u2716\ufe0f\u2795\u2796\u2797\u2660\ufe0f\u2665\ufe0f\u2663\ufe0f\u2666\ufe0f\ud83d\udcae\ud83d\udcaf\u2714\ufe0f\u2611\ufe0f\ud83d\udd18\ud83d\udd17\u27b0\u3030\u303d\ufe0f\ud83d\udd31\u25fc\ufe0f\u25fb\ufe0f\u25fe\ufe0f\u25fd\ufe0f\u25aa\ufe0f\u25ab\ufe0f\ud83d\udd3a\ud83d\udd32\ud83d\udd33\u26ab\ufe0f\u26aa\ufe0f\ud83d\udd34\ud83d\udd35\ud83d\udd3b\u2b1c\ufe0f\u2b1b\ufe0f\ud83d\udd36\ud83d\udd37\ud83d\udd38\ud83d\udd39';

	$('.emoji-widget').html(all_emojis).minEmoji(true);
	$.trim($('.emoji-widget'));

	$(document).on('click', '.emoji-button', function (event) {
		$('#' + $(this).data('id')).toggle();
		return false;
	});

	$(document).click(function () {
		$(".emoji-widget").hide();
	});

	$('.modal-comment-wysiwyg').change(function () {
		var id = $(this).data('id');
		var comment = $(this).html();

		$('#' + id).val(comment);
	});

	$(document).on('click', '.emoji-widget span', function (event) {
		var $input = $(this).parent().parent().find('div.modal-comment-input');
		var span = $(this);
		$input.append(
			"<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNgYAAAAAMAASsJTYQAAAAASUVORK5CYII=' class='repl-img " + span.attr('class')
			+ "' data-code='" + span.data("code") + "'>");
		$(this).parent().hide();
		return false;
	});

	$('.emoji').minEmoji();

	$('.instagram-comment:not(.is-truncated)').dotdotdot({
		callback: function (isTruncated, orgContent) {
			if (isTruncated) {
				$(this).prop('title', orgContent[0].data);
			}
		},
		height: 35
	});

	var form = $('#photo-upload, #addQueue');
	var form_new = $('#photo-upload-new');

	//form

	form.find('#container_image, #container_image').cropit({
		exportZoom: 1.15,
		imageBackground: true,
		imageBackgroundBorderWidth: 50,
		rejectSmallImage: false,
		onImageError: function (event) {
			form.find('#cropitImageError').html(event.message);
		}
	});

	form.find('.cropit-image-input').change(function () {
		form.find('#cropitImageError').html('');
	});

	form.on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var uploadId = button.data('upload-id');
		var modal = $(this);
		modal.find('#upload-id').val(uploadId);
	});

	//form-new
	form_new.find('.cropit-image-input').change(function () {
		form_new.find('#cropitImageError').html('');
	});

	var jcrop_api,
		crop_x = 15, crop_y = 15, crop_width = 355, crop_height = 355,
		zoom, image_src,
		image = form_new.find('#upload-image-img');
	var initJcrop = function(){
		if (typeof jcrop_api != 'undefined') {
			jcrop_api.destroy();
		}
		image.Jcrop({
			minSize: [320, 320],
			maxSize: [1080, 1080],
			bgColor: 'FF0000',
			drawBorders: false,
			bgOpacity: 0.4,
			setSelect: [crop_x, crop_y, crop_width, crop_height],
			selection: true,
			touchSupport: true,
			keySupport: false,
			theme: 'light',
			aspectRatio: 1,
			onSelect: function(coordinates){
				crop_x = coordinates.x;
				crop_y = coordinates.y;
				crop_width = coordinates.w;
				crop_height = coordinates.h;
			},
			onChange: function(coordinates){
				crop_x = coordinates.x;
				crop_y = coordinates.y;
				crop_width = coordinates.w;
				crop_height = coordinates.h;
			}
		}, function() {
			jcrop_api = this;
		});
	};
	form_new.find('#fileupload').fileupload({
		done: function (e, data)
		{
			form_new.find('.progress').hide();
			if (data.result.result){
				image.attr('src', data.result.src);
				image_src = data.result.src;
				initJcrop();
			} else {
				alert(data.result.error);
			}
		},
		disableImageResize: true,
		add: function (e, data)
		{
			if (data.autoUpload || (data.autoUpload !== false &&
				$(this).fileupload('option', 'autoUpload'))) {
				form_new.find('.progress').show();
				data.process().done(function ()
				{
					data.submit();
				});
			}
		},
		acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
		progressall: function (e, data)
		{
			var progress = parseInt(data.loaded / data.total * 100, 10);
			form_new.find('.progress .progress-bar').css(
				'width',
				progress + '%'
			).text(progress + '%');
		}
	}).bind('fileuploadprocessfail', function (e, data) {
		form_new.find('#imageError'.html(data.files[data.index].error));
	});
	form_new.find("#image-zoom-slider").slider({
		value: 50,
		max: 100,
		min: 1,
		slide: function(event, ui) {
			if (typeof  jcrop_api === 'undefined'){
				event.stopPropagation();
				return false;
			}
			var value = form_new.find('#image-zoom-slider').slider('option', 'value');
			zoom = (value + 50)/100;
			var width = image.width() * zoom;
			var height = image.height() * zoom;
			var result_slide = jcrop_api.resizeImage(width, height);
			if (!result_slide){
				event.stopPropagation();
				return false;
			}
		},
		stop: function(){
			if (typeof jcrop_api === 'undefined'){
				event.stopPropagation();
				return false;
			}
			jcrop_api.updateSelection();
		}
	});

	form_new.find('#send-image').on('click', function(){
		if (typeof jcrop_api === 'undefined'){
			return false;
		}
		if (crop_width < 1 || crop_height < 1){
			jcrop_api.setSelect([15, 15, 355, 355]);
			return false;
		}
		var input = form_new.find('#image-upload-input');
		var id = form_new.find('#upload-id').val();
		$.ajax({
			url: "/content/crop/" + id,
			data: {
				image_src: image_src,
				crop_x: crop_x,
				crop_y: crop_y,
				crop_width: crop_width,
				crop_height: crop_height,
				zoom: zoom,
				description: input.val()
			},
			method: 'post',
			dataType: 'json',
			beforeSend: function () {
				form_new.find('#container_image button').hide();
				form_new.find('#container_image .loading-send').show();
				form_new.find('#upload-image-alert').hide().removeClass('alert-danger alert-success');
			},
			success: function (data) {
				if (data.error == undefined) {
					form_new.find('#upload-image-alert').show().addClass('alert-success').html(window.Zengram.t('Image uploaded'));
					input.val('');
					if (typeof jcrop_api != 'undefined') {
						jcrop_api.destroy();
					}
					image.attr('src', '/img/en/no_image_available.png');
					image.attr('style', '');
					image_src = '/img/en/no_image_available.png';
					form_new.find("#image-zoom-slider").slider("value", 50);
				} else {
					form_new.modal('hide');
					updateAccountError(id, data.error, true);
				}
			},
			complete: function () {
				form_new.find('#container_image button').show();
				form_new.find('#container_image .loading-send').hide();
			}
		});
	});

	form_new.on('show.bs.modal', function (event) {
		form_new.find('#photo-upload').find('#upload-image-alert').hide();
		form_new.find('#upload-id').val($(event.relatedTarget).data('upload-id'));
	});

	//media-list
	navButtons();

	var $mediaList = $('#media-list');
	$mediaList.on('click', '.instagram-modal-comment > .delete', function () {
//		if (!confirm(window.Zengram.t('Are you sure want to remove comment?'))) {
//			return;
//		}

		var $this = $(this);
		var $comment = $this.parents('.instagram-modal-comment');
		var media_id = $comment.parents('.modal-media-item').data('media-id');
		var photo_title = $mediaList.find('.instagram-photo-title').html();

		$.ajax({
			url: "/content/comment-remove/" + $mediaList.data('user-id'),
			data: {'media_id': media_id, comment_id: $comment.data('comment-id')},
			method: 'post',
			success: function (data) {
				if (data.indexOf(window.Zengram.t('Error')) < 0) {
					$this.parents('.modal-content-comments').html(photo_title + data).minEmoji();
				}
			},
			beforeSend: function () {
			},
			complete: function () {
			}
		});
	});
});

function navButtons()
{
	$('.instagram-media-modal').on('show.bs.modal', function (event) {
		var modal = $(this);

		setTimeout(function () {
			$.post("/content/commentscounter/", {
				mediaId: modal.attr('data-mediaId'),
				comments: modal.attr('data-comments')
			});
			$('#newCommentCounter' + modal.attr('data-mediaId')).html('+0');
			modal.find('.modal-content-comments').scrollTop(modal.find('.modal-content-comments').prop("scrollHeight"));
			if ($('#' + modal.attr('id') + '.modal').next().next().attr('id')) {
				modal.find('.next').show();
			}
			if ($('#' + modal.attr('id') + '.modal').prev().prev().attr('id')) {
				modal.find('.prev').show();
			}
			modal.find('.modal-content-comments').find('.commenter-login').dotdotdot();
		}, 200);
	});

	$(document).on('click', '.prev', function () {
		var current = $(this).data('current');
		$('#showMedia' + current).modal('hide');
		$('#' + $('#showMedia' + current + '.modal').prev().prev().attr('id')).modal('show');
		return false;
	});

	$(document).on('click', '.next', function () {
		var current = $(this).data('current');
		$('#showMedia' + current).modal('hide');
		$('#' + $('#showMedia' + current + '.modal').next().next().attr('id')).modal('show');
		return false;
	});
}
