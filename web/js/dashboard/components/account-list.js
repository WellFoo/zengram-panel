var AccountList = can.Control.extend({
    init: function(element, options) {
        this.element.html(can.view('accounts-list-view', options));
    }
});