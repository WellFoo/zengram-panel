<?php
namespace app\components;

use app\components\UHelper;
use app\models\Account;
use app\models\Device;
use app\models\Proxy;
use Core;
use core\instagram\exceptions\CheckpointReqiredException;
use core\storage\MemoryStorage;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class InstagramComponent extends Component
{
	private $handledAccount;
	private $useNewApi = false;

	public function account($account_id)
	{
		if ($account_id instanceof Account) {
			$account = $account_id;
		} else {
			$account = $this->getAccountById($account_id);
		}
		
		$this->setHandlerAccount($account);

		return Core::$app->api;
	}

	public function instagram($account_id, $password = null)
	{
		if ($password === null) {
			$account = $this->getAccountByInstagramId($account_id);
			$this->setHandlerAccount($account);
		} else {
			
		}

		return $this->api;
	}

	private function setHandlerAccount(Account $account)
	{
		$this->handledAccount = $account;

		\Core::init([
			'app' => [
				'storage' => [
					//'class' => '\app\components\MongoStorage',
					'class' => '\core\storage\MemoryStorage',
					'instagram_id' => $account->instagram_id
				],
				'api' => [
					'class'              => '\app\components\InstagramApi',
					'cookiesStorageName' => 'storage',
					'verbose'            => 0,
				],
				'captcha_apikey' => 'e963a8aab389d293b9982e42ef4cacba',
				'server_type'    => 'statistic',
			]
		]);

		\Core::setLogCallback([$account, 'log']);

		\Core::$app->api->checkpointCallback = function () {
			throw new CheckpointReqiredException('checkpoint');
		};

		$device = $account->getDeviceData();
		$proxy = $account->getProxyDataString();

		\Core::$app->api->load([
			'proxy'     => $proxy,
			'ip'        => $account->user->reg_ip,
			'username'  => $account->login,
			'password'  => UHelper::unpack($account->password),
			'device' => [
				'os' => [
					'class'     => '\core\instagram\device\OS',
					'api_level' => $device['android_api'],
					'version'   => $device['android_version'],
					'language'  => 'en_US',
				],
				'screen' => [
					'class'     => '\core\instagram\device\Screen',
					'width'     => $device['screen_x'],
					'height'    => $device['screen_y'],
					'dpi'       => $device['dpi'],
				],
				'vendor'        => $device['vendor'],
				'model'         => $device['model'],
				'platform'      => $device['platform'],
				'cpu'           => $device['cpu'],
			]
		]);
	}

	private function getAccountById($account_id)
	{
		if (!empty($this->handledAccount) && $this->handledAccount->id == $account_id) {
			return $this->handledAccount;
		}
		return Account::findOne($account_id);
	}

	private function getAccountByInstagramId($instagram_id)
	{
		if (!empty($this->handledAccount) && $this->handledAccount->instagram_id == $instagram_id) {
			return $this->handledAccount;
		}
		return Account::findOne(['instagram_id' => $instagram_id]);
	}

	public function __get($name)
	{
		if ($name == 'api') {
			$this->setHandlerAccount($this->handledAccount);

			return Core::$app->api;
		}

		return parent::__get($name);
	}
}