<?php
/**
 * @var \yii\web\View $this
 */

use app\modules\partnership\Module;

$this->title = Module::t('module', 'Admin panel');
?>

<h1><?= $this->title ?></h1>

<?php
echo \yii\bootstrap\Nav::widget([
	'items' => [
		[
			'label' => Module::t('module', 'Banners'),
			'url' => ['banners'],
		], [
			'label' => Module::t('module', 'Settings'),
			'url' => ['settings'],
		]
	]
]);
?>

