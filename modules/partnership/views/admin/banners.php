<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $data_provider
 */

use app\modules\partnership\Module;
use yii\helpers\Html;

$this->title = Module::t('module', 'Banners');
?>

<h1><?= $this->title ?></h1>

<?= \yii\grid\GridView::widget([
	'dataProvider' => $data_provider,
	'columns' => [
		'id',
		'code:html',
		[
			'format' => 'raw',
			'value' => function ($item) {
				/** @var \app\modules\partnership\models\Banner $item */
				return
					Html::a(
						Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']),
						['banner-edit', 'id' => $item->id],
						['title' => Module::t('module', 'Edit')]
					)
					.'&nbsp;'
					.Html::a(
						Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash']),
						['banner-delete', 'id' => $item->id],
						[
							'data-method' => 'post',
							'title' => Module::t('module', 'Delete')
						]
					);
			},
			'headerOptions' => [
				'style' => 'width: 70px;'
			]
		]
	],
]) ?>

<?= Html::a(Module::t('module', 'Create new'), ['banner-create'], ['class' => 'btn btn-success']) ?>
