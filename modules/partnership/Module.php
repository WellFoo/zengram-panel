<?php

namespace app\modules\partnership;

use app\modules\partnership\components\Settings;
use Yii;

class Module extends \yii\base\Module
{
	public $controllerNamespace = 'app\modules\partnership\controllers';

	/** @var Settings */
	public static $settings;

	public function init()
	{
		parent::init();

		// custom initialization code goes here
		$this->registerTranslations();

		self::$settings = new Settings();
		self::$settings->restore();
	}

	public function registerTranslations()
	{
		Yii::$app->i18n->translations['modules/partnership/*'] = [
			'class' => 'yii\i18n\PhpMessageSource',
			'sourceLanguage' => 'en-US',
			'basePath' => '@app/modules/partnership/messages',
			'fileMap' => [
				'modules/partnership/module' => 'module.php',
			]
		];
	}

	public static function t($category, $message, $params = [], $language = null)
	{
		return Yii::t('modules/partnership/' . $category, $message, $params, $language);
	}
}
