<?php namespace app\modules\partnership\models;

/**
 * Interface ModuleUserInterface
 * @package app\modules\partnership\models
 * @property boolean $isAdmin
 * @property Partner $partner
 * @property boolean $isPartner
 * @property Referal $referral
 * @property boolean $isReferral
 * @property Manager $manager
 * @property boolean $isManager
 * @property string  $emailAddress
 */
interface ModuleUserInterface
{
	/**
	 * @return boolean
	 */
	public function getIsAdmin();

	/**
	 * @return \yii\db\ActiveRecord
	 */
	public function getPartner();

	/**
	 * @return boolean
	 */
	public function getIsPartner();

	/**
	 * @return \yii\db\ActiveRecord
	 */
	public function getReferral();

	/**
	 * @return boolean
	 */
	public function getIsReferral();

	/**
	 * @return \yii\db\ActiveRecord
	 */
	public function getManager();

	/**
	 * @return boolean
	 */
	public function getIsManager();

	/**
	 * @param $amount double
	 */
	public function addBalance($amount);

	/**
	 * @return string
	 */
	public function getEmailAddress();
}