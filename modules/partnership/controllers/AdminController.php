<?php namespace app\modules\partnership\controllers;

use app\modules\partnership\components\Controller;
use app\modules\partnership\models\Banner;
use app\modules\partnership\models\User;
use app\modules\partnership\Module;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class AdminController extends Controller
{
	public function init()
	{
		parent::init();

		/** @var User $user */
		$user = \Yii::$app->user->identity;
		if (!$user->isAdmin) {
			throw new ForbiddenHttpException();
		}
	}

	public function actionIndex()
	{
		return $this->render('index');
	}

	public function actionBanners()
	{
		$data_provider = new ActiveDataProvider([
			'query' => Banner::find()
		]);

		return $this->render('banners', [
			'data_provider' => $data_provider
		]);
	}

	public function actionBannerCreate()
	{
		return $this->bannerForm(new Banner());
	}

	public function actionBannerEdit($id)
	{
		/** @var Banner $banner */
		$banner = Banner::findOne($id);
		if (is_null($banner)) {
			throw new NotFoundHttpException(Module::t('module', 'Banner not found'));
		}

		return $this->bannerForm($banner);
	}

	/**
	 * @param Banner $banner
	 * @return string|\yii\web\Response
	 */
	private function bannerForm($banner)
	{
		if ($banner->load(\Yii::$app->request->post()) && $banner->save()) {
			return $this->redirect(['banners']);
		}

		return $this->render('banner_edit', [
			'model' => $banner
		]);
	}

	public function actionBannerDelete($id)
	{
		/** @var Banner $banner */
		$banner = Banner::findOne($id);
		if (is_null($banner)) {
			throw new NotFoundHttpException(Module::t('module', 'Banner not found'));
		}

		$banner->delete();

		return $this->redirect(['banners']);
	}

	public function actionSettings()
	{
		if (Module::$settings->load(\Yii::$app->request->post()) && Module::$settings->save()) {
			$this->redirect(['index']);
		}

		return $this->render('settings');
	}
}