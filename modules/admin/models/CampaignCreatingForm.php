<?php

namespace app\modules\admin\models;

use yii\base\Model;

class CampaignCreatingForm extends Model
{
    public $name;
    public $location;
    public $profilesOfDonors;
    public $accountIdBeforeTrig;
    public $frequencyOfPosting;
    public $limitFollow;
    public $limitLike;
    public $tags;
    public $fpopularFrom;
    public $fpopularTo;
    public $frecomendedFrom;
    public $frecomendedTo;
    public $followersTrigger;
    public $daysTrigger;
    public $hourPassTrigger;
    public $hourBanTrigger;
    public $languages;
    public $accounts;
    public $wait;


    public function rules()
    {
        return [
            //[['name', 'location', 'tags', 'profilesOfDonors', 'accountIdBeforeTrig', 'frequencyOfPosting', 'limitFollow', 'limitLike', 'fpopularFrom', 'fpopularTo', 'frecomendedFrom', 'frecomendedTo', 'followersTrigger', 'daysTrigger', 'hourPassTrigger', 'hourBanTrigger'], 'required', 'message' => false],
            [['profilesOfDonors', 'accountIdBeforeTrig', 'limitFollow', 'limitLike', 'fpopularFrom', 'fpopularTo', 'frecomendedFrom', 'frecomendedTo', 'followersTrigger', 'daysTrigger', 'hourPassTrigger', 'hourBanTrigger', 'wait'], 'integer'],
            [['name', 'location', 'tags', 'frequencyOfPosting'], 'string'],
            ['languages', 'each', 'rule' => ['in', 'range' => [Donor::LANG_EN, Donor::LANG_ES, Donor::LANG_RU]]],
        ];
    }
    public function attributeLabels()
    {
        return [
            'languages' => 'Языки',
            'accounts' => 'Доноры'
        ];
    }
    public static function donorsAccounts($lang = ['ru','en','es'])
    {
        $donors = Donor::find()->asArray()->all();

        $result = [];
        foreach($donors as $val){

            if($val['sex'] == 'male'){
                $russex = '<span class="'.str_replace(',',' ',$val['languages']).'" style="color:#0070a3; font-weight: bold;">муж</span>';
            }
            else{
                $russex = '<span class="'.str_replace(',',' ',$val['languages']).'" style="color:#ff0084; font-weight: bold;">жен</span>';
            }
            $result[$val['id']] = "($russex) ".$val['login'];
        }
        return $result;
        /*
        return [
            $donors[0]['id'] => $donors[0]['login'],
            $donors[1]['id']  => $donors[1]['login'],
            $donors[2]['id'] => $donors[2]['login'],
        ];
        */
    }
}
