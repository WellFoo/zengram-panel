<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\models\PopularAccountsCommentsLog;

/**
 * PopularAccountsCommentsLogSearch represents the model behind the search form about `app\modules\admin\models\PopularAccountsCommentsLog`.
 */
class PopularAccountsCommentsLogSearch extends PopularAccountsCommentsLog
{
	public $user_id;
	public $media;
	public $query;
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['code', 'in', 'range' => [
				self::CODE_TRUNCATE,
				self::CODE_ADDED_ROWS,
				self::CODE_STATISTIC,
				self::CODE_ERROR,
				self::CODE_COMMENTED,
				self::CODE_DELETED,
				self::CODE_EMPTY_COMMENT,
				self::CODE_COMMENT_FAILED,
				self::CODE_COMMENT_SUCCESS,
				self::CODE_DELETE_FAILED,
				self::CODE_DELETE_SUCCESS,
			]],
			[['user_id', 'media'], 'safe']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		Yii::info('Попадаем в модель Serach');
		if (!empty($this->query)){
			$query = $this->query;
		} else {
			$query = PopularAccountsCommentsLog::find();
		}
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['date' => SORT_DESC]],
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		if ($this->code){
			$query->andFilterWhere([
				'code' => $this->code,
			]);
		}

		if ($this->user_id){
			$query->andFilterWhere(['like', 'description', '"user_id":"'.trim($this->user_id).'"']);
		}

		if ($this->media){
			$query->andFilterWhere(['like', 'description', '"media_id":"'.trim($this->media).'"']);
		}

		return $dataProvider;
	}
}