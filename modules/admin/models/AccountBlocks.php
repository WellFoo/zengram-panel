<?php namespace app\modules\admin\models;

use app\models\Account;
use app\models\Users;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "account_blocks".
 *
 * @property integer $id
 * @property integer $account_id
 * @property integer $user_id
 * @property integer $instagram_id
 * @property string  $action
 * @property string  $message
 * @property string  $block_date
 * @property string  $block_expire
 * @property integer $account_media
 * @property integer $account_follows
 * @property integer $account_followers
 * @property integer $actions_today
 * @property integer $num_actions
 *
 * @property Account $account
 * @property Users   $user
 */
class AccountBlocks extends ActiveRecord
{
	const ACTION_COMMENTS = 'comments';
	const ACTION_FOLLOWS  = 'follows';
	const ACTION_UNFOLLOWS  = 'unfollow';
	const ACTION_FOLLOWS_TIMEOUT  = 'follows_timeout';
	const ACTION_SPAM     = 'spam';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'account_blocks';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['account_id', 'user_id', 'instagram_id', 'action', 'account_media', 'account_follows', 'account_followers', 'actions_today', 'num_actions'], 'required'],
			[['account_id', 'user_id', 'instagram_id', 'account_media', 'account_follows', 'account_followers', 'actions_today', 'num_actions'], 'integer'],
			[['block_date', 'block_expire'], 'date'],
			[['action'], 'string', 'max' => 15],
			[['message'], 'string']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'                => 'ID',
			'account_id'        => 'Account ID',
			'user_id'           => 'User ID',
			'instagram_id'      => 'Instagram ID',
			'action'            => 'Действие',
			'block_date'        => 'Дата блокировки',
			'block_expire'      => 'Дата истечения',
			'account_media'     => 'Кол-во медиа',
			'account_follows'   => 'Кол-во подписок',
			'account_followers' => 'Кол-во подписчиков',
			'actions_today'     => 'Десйствий за день',
			'num_actions'       => 'Действий с предыдущего блока',
			'message'           => 'Сообщение',
		];
	}

	public function getAccount()
	{
		return self::hasOne(Account::className(), ['id' => 'account_id']);
	}

	public function getUser()
	{
		return self::hasOne(Users::className(), ['id' => 'user_id']);
	}

	public static function actionsLabels()
	{
		return [
			self::ACTION_COMMENTS => 'Комментарии',
			self::ACTION_FOLLOWS  => 'Фоллоу',
			self::ACTION_UNFOLLOWS  => 'Анфоллоу',
			self::ACTION_FOLLOWS_TIMEOUT  => 'Некорректный фоллоу',
			self::ACTION_SPAM     => 'Спам',
		];
	}
}
