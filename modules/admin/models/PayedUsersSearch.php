<?php namespace app\modules\admin\models;

use app\models\Account;
use app\models\Invoice;
use app\models\Users;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

class PayedUsersSearch extends Model
{
	public $id;
	public $mail;
	public $accounts;

	public function rules()
	{
		return [
			[['mail', 'accounts', 'id'], 'safe'],
			['id', 'integer'],
		];
	}

	public function search($params = null)
	{
		$accsSubQuery = Account::find()
			->select('user_id, COUNT(id) AS accounts_count')
			->groupBy('user_id');

		$invSubQueryAll = Invoice::find()
			->select('user_id, COUNT(id) AS invoice_count, SUM(sum) AS invoice_sum, MAX(date) AS last_payment')
			->groupBy('user_id')
			->where(['status' => Invoice::STATUS_SUCCESS]);

		$invSubQueryMonth = Invoice::find()
			->select('user_id, COUNT(id) AS month_invoice_count, SUM(sum) AS month_invoice_sum')
			->groupBy('user_id')
			->where(['status' => Invoice::STATUS_SUCCESS])
			->andWhere("date >= (NOW() - '1 month'::interval)");

		$query = Users::find()
					->where(['is_payed' => 1])
					->select([
						Users::tableName().'.*', 'a.accounts_count', 'i.invoice_count', 'i.invoice_sum','i.last_payment', 'im.month_invoice_count', 'im.month_invoice_sum'
					])
					->joinWith(['invoices' => function($q){
						/** @var ActiveQuery $q */
						$q->joinWith('price');
					}])
			//->groupBy(Users::tableName().'.id')
				
//			->joinWith(['monthInvoices' => function ($q) {
//				/** @var ActiveQuery $q */
//				$q->joinWith('price');
//			}])
			->leftJoin(['a' => $accsSubQuery], 'a.user_id = ' . Users::tableName() . '.id')
			->leftJoin(['i' => $invSubQueryAll], 'i.user_id = ' . Users::tableName() . '.id')
			->leftJoin(['im' => $invSubQueryMonth], 'im.user_id = ' . Users::tableName() . '.id')->asArray();

		$dataProvider = new ActiveDataProvider([
			'query'      => $query,
			'sort'       => [
				'defaultOrder' => ['id' => SORT_DESC],
				'attributes'   => [
					'id',
					'mail',
					'balance',
					'accounts'          => [
						'asc'  => ['a.accounts_count' => SORT_ASC],
						'desc' => ['a.accounts_count' => SORT_DESC]
					],
					'invoiceCount'      => [
						'asc'  => ['i.invoice_count' => SORT_ASC],
						'desc' => ['i.invoice_count' => SORT_DESC]
					],
					'monthInvoiceCount' => [
						'asc'  => ['im.month_invoice_count' => SORT_ASC],
						'desc' => ['im.month_invoice_count' => SORT_DESC]
					],
					'invoiceSum'      => [
						'asc'  => ['i.invoice_sum' => SORT_ASC],
						'desc' => ['i.invoice_sum' => SORT_DESC]
					],
					'monthInvoiceSum' => [
						'asc'  => ['im.month_invoice_sum' => SORT_ASC],
						'desc' => ['im.month_invoice_sum' => SORT_DESC]
					],
					'lastPayment' => [
							'asc'  => ['i.last_payment' => SORT_ASC],
							'desc' => ['i.last_payment' => SORT_DESC]
					],
					'demo'
				]
			],
			'pagination' => [
				'pageSize' => 50
			],
		]);

		if ($params === null) {
			$params = \Yii::$app->request->queryParams;
		}

		if (!$this->load($params) || !$this->validate()) {
			return $dataProvider;
		}

		/** @var ActiveQuery $query */
		if ($this->mail) {
			$query->andWhere(['ILIKE', 'mail', trim($this->mail)]);
		}

		if ($this->id) {
			$query->andWhere([Users::tableName() . '.id' => trim($this->id)]);
		}

		if ($this->accounts) {
			$query->innerJoin(Account::tableName() . ' AS acc', 'acc.user_id = ' . Users::tableName() . '.id');
			$query->andWhere(['a.accounts_count' => intval($this->accounts)]);
		}

		if (!empty(\Yii::$app->request->queryParams['num_users'])) {
			$dataProvider->pagination->pageSize = \Yii::$app->request->queryParams['num_users'];
		}

		return $dataProvider;
	}

	public function searchNoDimensions($params = null)
	{
		$accsSubQuery = Account::find()
			->select('user_id, COUNT(id) AS accounts_count')
			->groupBy(['user_id']);

		$invSubQueryAll = Invoice::find()
			->select('user_id, COUNT(id) AS invoice_count, SUM(sum) AS invoice_sum, MAX(date) AS last_payment')
			->groupBy('user_id')
			->where(['status' => Invoice::STATUS_SUCCESS]);
		
		$invSubQueryMonth = Invoice::find()
			->select('user_id, COUNT(id) AS month_invoice_count, SUM(sum) AS month_invoice_sum')
			->groupBy('user_id')->where(['status' => Invoice::STATUS_SUCCESS])
			->andWhere("date >= (NOW() - '30 day'::interval)");
			
		$query = Users::find()
			->where(['is_payed' => 1])
			->andWhere('(gender = \''.Users::UNDEFINED.'\' OR person=\''.Users::UNDEFINED.'\')')
			->select([Users::tableName().'.*', 'a.accounts_count', 'i.invoice_count','i.invoice_sum','i.last_payment',
				'im.month_invoice_count', 'im.month_invoice_sum'])
			->joinWith(['invoices' => function($q){
				/** @var ActiveQuery $q */
				$q->joinWith('price');
			}])->groupBy([Users::tableName().'.id', 'a.accounts_count', 'i.invoice_count', 'i.invoice_sum', 'i.last_payment', 'im.month_invoice_count', 'im.month_invoice_sum'])
//			->joinWith(['monthInvoices' => function ($q) {
//				/** @var ActiveQuery $q */
//				$q->joinWith('price');
//			}])
			->leftJoin(['a' => $accsSubQuery], 'a.user_id = ' . Users::tableName() . '.id')
			->leftJoin(['i' => $invSubQueryAll], 'i.user_id = ' . Users::tableName() . '.id')
			->leftJoin(['im' => $invSubQueryMonth], 'im.user_id = ' . Users::tableName() . '.id')->asArray();

		$dataProvider = new ActiveDataProvider([
			'query'      => $query,
			'sort'       => [
				'defaultOrder' => ['id' => SORT_DESC],
				'attributes'   => [
					'id',
					'mail',
					'balance',
					'accounts'          => [
						'asc'  => ['a.accounts_count' => SORT_ASC],
						'desc' => ['a.accounts_count' => SORT_DESC]
					],
					'invoiceCount'      => [
						'asc'  => ['i.invoice_count' => SORT_ASC],
						'desc' => ['i.invoice_count' => SORT_DESC]
					],
					'monthInvoiceCount' => [
						'asc'  => ['im.month_invoice_count' => SORT_ASC],
						'desc' => ['im.month_invoice_count' => SORT_DESC]
					],
					'invoiceSum'      => [
						'asc'  => ['i.invoice_sum' => SORT_ASC],
						'desc' => ['i.invoice_sum' => SORT_DESC]
					],
					'monthInvoiceSum' => [
						'asc'  => ['im.month_invoice_sum' => SORT_ASC],
						'desc' => ['im.month_invoice_sum' => SORT_DESC]
					],
					'lastPayment' => [
						'asc'  => ['i.last_payment' => SORT_ASC],
						'desc' => ['i.last_payment' => SORT_DESC]
					],
					'demo'
				]
			],
			'pagination' => [
				'pageSize' => 50
			],
		]);

		if ($params === null) {
			$params = \Yii::$app->request->queryParams;
		}

		if (!$this->load($params) || !$this->validate()) {
			return $dataProvider;
		}

		/** @var ActiveQuery $query */
		if ($this->mail) {
			$query->andWhere(['like', 'mail', trim($this->mail)]);
		}


		if ($this->id) {
			$query->andWhere([Users::tableName() . '.id' => trim($this->id)]);
		}

		if ($this->accounts) {
			$query->innerJoin(Account::tableName() . ' AS acc', 'acc.user_id = ' . Users::tableName() . '.id');
			$query->andWhere(['like', 'acc.login', trim($this->accounts)]);
		}

		if (!empty(\Yii::$app->request->queryParams['num_users'])) {
			$dataProvider->pagination->pageSize = \Yii::$app->request->queryParams['num_users'];
		}

		return $dataProvider;
	}

}