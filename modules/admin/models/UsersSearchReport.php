<?php namespace app\modules\admin\models;

use app\models\Account;
use app\models\Invoice;
use app\models\Users;
use app\models\UsersLog;
use Faker\Provider\lv_LV\Payment;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class UsersSearchReport extends Model
{
	public $id;
	public $mail;
	public $accounts;

	public function rules()
	{
		return [
			[['mail', 'accounts', 'id'], 'safe'],
			['id', 'integer'],
		];
	}

	public function search($params = null)
	{

		$subQuery = Account::find()->select('user_id, COUNT(id) AS accounts_count')->groupBy('user_id');
		$subQuery2 = Invoice::find()->select('user_id, COUNT(*) AS payment_count')->where(['status' => 3])->groupBy('user_id');

		$query = Users::find()->leftJoin(['a' => $subQuery], 'a.user_id = ' . Users::tableName() . '.id');
		$query->leftJoin(['b' => $subQuery2], 'b.user_id = ' . Users::tableName() . '.id');

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['id' => SORT_DESC],
				'attributes' => [
					'id',
					'mail',
					'balance',
					'accounts' => [
						'asc' => ['a.accounts_count' => SORT_ASC],
						'desc' => ['a.accounts_count' => SORT_DESC]
					],
					'demo'
				]
			],
			'pagination' => [
				'pageSize' => 50
			],
		]);


		if ($params === null) {
			$params = \Yii::$app->request->queryParams;
		}


		if (empty($params['type-report'])) {
			$params['type-report'] = 'registered';
			$params['from'] = date('Y-m-d', time() - 84000);

			$params['to'] =  date('Y-m-d', time());
		}

		switch ($params['type-report']) {
			case 'payed':
				// Получаем все успещные платежи за выбранный период
				if (isset($params['from']) && isset($params['to'])) {
					$payments = Invoice::find()
						->where(['>=', 'date', date('Y-m-d', strtotime($params['from']))])
						->andWhere(['<', 'date', date('Y-m-d', strtotime($params['to']))])
						->andWhere(['status' => 3])
						->asArray()
						->all();
					$usersPayers = [];
					foreach ($payments as $payment) {
						$usersPayers[] = $payment['user_id'];
					}
					if (!count($usersPayers)) {
						$usersPayers = [0];
					}
					$query->andWhere(['id' => $usersPayers]);
				}

				break;
			case 'registered':
				if (isset($params['from']) && isset($params['to'])) {
					$query->andWhere(['>=', 'regdate', date('Y-m-d', strtotime($params['from']))]);
					$query->andWhere(['<', 'regdate', date('Y-m-d', strtotime($params['to']))]);
				}
				break;
		}

		if (isset($params['ufilter'])) {
			switch ($params['ufilter']) {
				case 0:
					break;
				case 1:
					$logs = UsersLog::find()->select('user_id')->distinct('user_id')->where(['>', 'date', date('Y-m-d', time() - 60 * 60 * 24 * 30)])->asArray()->all();;
					$excludeUsers = [];
					foreach ($logs as $log) {
						$excludeUsers[] = $log['user_id'];
					}
					$usl = '';
					foreach ($excludeUsers as $key => $exc) {
						$usl .= 'id <> ' . $exc;
						if ($key + 1 != count($excludeUsers)) {
							$usl .= ' AND ';
						}
					}
					//echo $usl;
					$query->andWhere($usl);
					$query->andWhere(['is_payed' => 1]);

					break;
				case 2:
					$logs = UsersLog::find()->select('user_id')->distinct('user_id')->where(['>', 'date', date('Y-m-d', time() - 60 * 60 * 24 * 30)])->asArray()->all();
					$excludeUsers = [];
					foreach ($logs as $log) {
						$excludeUsers[] = $log['user_id'];
					}
					$usl = '';
					foreach ($excludeUsers as $key => $exc) {
						$usl .= 'id <> ' . $exc;
						if ($key + 1 != count($excludeUsers)) {
							$usl .= ' AND ';
						}
					}
					//echo $usl;
					$query->andWhere($usl);
					$query->andWhere(['is_payed' => 1]);
					$query->andWhere(['<=', 'balance', 0]);
					break;
				case 3:
					$query->andWhere(['is_payed' => '1']);
					$query->andWhere('b.payment_count = 1');
					break;
				case 4:
					$query->andWhere(['is_reg_from_rf' => '1']);
			}
		}


		if (!$this->load($params) || !$this->validate()) {
			return $dataProvider;
		}

		if ($this->mail) {
			$query->andWhere(['ILIKE', 'mail', trim($this->mail)]);
		}

		if ($this->id) {
			$query->andWhere([Users::tableName() . '.id' => trim($this->id)]);
		}

		if ($this->accounts) {
			$query->innerJoin(Account::tableName() . ' AS acc', 'acc.user_id = ' . Users::tableName() . '.id');
			$query->andWhere(['a.accounts_count' => intval($this->accounts)]);
		}

		if (!empty(\Yii::$app->request->queryParams['num_users'])) {
			$dataProvider->pagination->pageSize = \Yii::$app->request->queryParams['num_users'];
		}

		return $dataProvider;
	}
}