<?php

namespace app\modules\admin\controllers;

use app\components\Controller;
use app\components\RPCHelper;
use app\models\Account;
use app\models\AccountProcessLog;
use app\models\AccountSettings;
use app\models\AccountsLog;
use app\models\Log;
use app\models\Settings;
use app\models\Users;
use app\models\Whitelist;
use core\logger\Logger;
use Yii;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\data\Pagination;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\grid\GridView;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ProjectsController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'except' => [
					'index',
					'logs'
				],
				'rules' => [
					[
						'allow' => true,
						'matchCallback' => function ($rule, $action) {
							return Yii::$app->user->identity->isAdmin;
						}
					]
				]
			]
		];
	}

	public function actionIndex($user_id = null)
	{
		$projects = Account::find();
		if (!empty($user_id)) {
			$projects = $projects->where(['user_id' => (int)$user_id]);
		}

		/** @var ActiveQuery $projects */
		if (!empty($_GET['login'])){
			$projects->andWhere(['ILIKE', 'login', trim($_GET['login'])]);
		}

		if (!empty($_GET['instagram_id'])){
			$projects->andWhere(['instagram_id' => (int)trim($_GET['instagram_id'])]);
		}

		$countQuery = clone $projects;
		$pages = new Pagination(['totalCount' => $countQuery->count()]);
		$models = $projects
			->orderBy(['id' => SORT_DESC])
			->offset($pages->offset)
			->limit($pages->limit)
			->all();

		return $this->render('index', [
			'projects' => $models,
			'pages'    => $pages,
			'user_id'  => $user_id
		]);
	}

	public function actionSettings($id)
	{
		$settings = Settings::find()->where('pause IS NULL')->asArray()->all();
		$settings_pause_0 = Settings::find()->where(['pause' => 0])->asArray()->all();
		$settings_pause_1 = Settings::find()->where(['pause' => 1])->asArray()->all();
		$settings_pause_2 = Settings::find()->where(['pause' => 2])->asArray()->all();

		$account = Account::findOne($id);

		return $this->render('settings', [
			'account'          => $account,
			'settings'         => $settings,
			'settings_pause_0' => $settings_pause_0,
			'settings_pause_1' => $settings_pause_1,
			'settings_pause_2' => $settings_pause_2,
		]);
	}

	public function actionSetting($id = null, $setting = null, $pause = null)
	{
		/** @var Settings $value */
		$value = Settings::find()->where(['setting' => $setting, 'pause' => $pause])->one();

		/** @var Account $account */
		$account = Account::find()->where(['instagram_id' => $id])->one();

		/** @var AccountSettings $model */
		$model = AccountSettings::find()
			->where([
				'account_id' => $id,
				'setting' => $setting,
				'pause' => $pause
			])->one();

		if (is_null($model)) {
			$model = new AccountSettings();
			$model->value = $value->value;
			$model->account_id = $id;
			$model->setting = $setting;
			$model->pause = $pause;
		}

		if (isset($_POST['AccountSettings'])) {
			$model->load($_POST);
			if ($model->save()) {
				Yii::$app->session->setFlash('success', 'Model has been saved');
				Yii::$app->response->redirect(['/admin/projects/settings/', 'id' => $account->id]);
			} else {
				Yii::$app->session->setFlash('error', 'Model could not be saved');
			}
		}

		return $this->render('setting_edit',
			[
				'account_id' => $id,
				'value'      => $value,
				'model'      => $model,
			]);
	}

	public function actionSearchUser()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$request = Yii::$app->request->post('request', false);

		if ($request === false) {
			throw new NotFoundHttpException();
		}

		$users = Users::find()
			->select([
				Users::tableName().'.id',
				Users::tableName().'.mail',
				Users::tableName().'.balance'
			])
			->where([
				'id' => (int) $request
			])
			->orWhere(['ILIKE', 'mail', trim($request)])
			->orderBy(['id' => SORT_ASC])
			->asArray();

		$users->addSelect(['accounts' => sprintf(
			'(SELECT COUNT(*) FROM %s WHERE %s = %s)',
			Account::tableName(),
			Account::tableName().'.user_id',
			Users::tableName().'.id'
		)]);

		$dataProvider = new ArrayDataProvider([
			'allModels' => $users->all(),
		]);

		$data = GridView::widget([
			'dataProvider' => $dataProvider,
			'layout' => '
				{items}',
			'columns'      => [
				[
					'attribute' => 'id',
					'label' => 'ID'
				],
				[
					'attribute' => 'mail',
					'label' => 'Эл. почта'
				],
				[
					'attribute' => 'balance',
					'label' => 'Баланс'
				],
				[
					'attribute' => 'accounts',
					'label' => 'Кол-во аккаунтов'
				],
				[
					'label' => '',
					'format' => 'raw',
					'value' => function($user) {
						return Html::tag('span', 'Привязать',
							[
								'class' => 'btn btn-success set-user',
								'data' => [
									'id' => $user['id']
								]
							]
						);
					}
				]
			],
		]);

		return [
			'status' => 'ok',
			'data' => $data
		];
	}

	public function actionChangeUser()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$user_id = Yii::$app->request->post('user_id', false);
		$account_id = Yii::$app->request->post('account_id', false);

		$user = Users::findOne($user_id);
		$account = Account::findOne($account_id);

		if (is_null($user) || is_null($account)) {
			throw new NotFoundHttpException();
		}

		$account->user_id = $user_id;
		$account->save();

		AccountProcessLog::log([
			'worker_ip' => ip2long($account->server),
			'instagram_id' => $account->instagram_id,
			'user_id' => $account->user_id,
			'code' => Logger::Z010_ACCOUNT_OWNER_CHANGE,
		]);

		return [
			'status' => 'ok',
			'user_id' => $user_id,
			'account_id' => $account->id,
			'account_login' => $account->login,
			'user_mail' => $user->mail,
		];
	}

	public function actionOptions($id)
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$optionName = Yii::$app->request->post('option', null);
		if ($optionName === null) {
			throw new BadRequestHttpException('Отсутствует обязательный параметр option');
		}

		$state = Yii::$app->request->post('state', null);
		if ($state === null) {
			throw new BadRequestHttpException('Отсутствует обязательный параметр state');
		}

		/* @var $account Account */
		$account = Account::findOne($id);
		if ($account === null) {
			throw new NotFoundHttpException('Аккаунт не найден');
		}

		$account->options->setAttribute($optionName, $state);
		$account->options->skipRelations = true;
		if ($account->options->save()) {
			return ['status' => 'ok'];
		} else {
			return [
				'status' => 'fail',
				'errors' => $account->options->getErrors()
			];
		}
	}

	public function actionDelete($id = null)
	{
		if ($id == null) {
			throw new HttpException(404, 'User not found.');
		} else {
			/** @var Account $account */
			$account = Account::find()->where('id = ' . $id)->one();
			$account->delete();
			return $this->actionIndex(Yii::$app->request->getQueryParam('user_id', null));
		}

	}

	public function actionStart($id)
	{
		if (empty($id)) {
			return 'error';
		}

		/** @var Account $account */
		$account = Account::find()->where('id = ' . $id)->one();
		//$result = $account->accountStart(true);
		$result = RPCHelper::startAccount($account, 'admin');
		if ($result) {
			Yii::$app->session->setFlash('Project action', 'Project successful start');
		}
		return Yii::$app->response->redirect(Yii::$app->request->referrer);
	}

	public function actionStop($id)
	{
		if (empty($id)) {
			return 'error';
		}
		/** @var Account $account */
		$account = Account::find()->where('id = ' . $id)->one();
		//$result = $account->accountStop(true);
		$result = RPCHelper::stopAccount($account, 'admin');
		if ($result) {
			Yii::$app->session->setFlash('Project action', 'Project successful stop');
		}
		return Yii::$app->response->redirect(Yii::$app->request->referrer);
	}

	public function actionRestart($id)
	{
		if (empty($id)) {
			return 'error';
		}
		/** @var Account $account */
		$account = Account::find()->where('id = ' . $id)->one();
		//$result = $account->accountStop(true);
		//$result &= $account->accountStart(true);

		$result = RPCHelper::stopAccount($account, 'admin');
		$result &= RPCHelper::startAccount($account, 'admin');

		if ($result) {
			Yii::$app->session->setFlash('Project action', 'Project successful restart');
//			return $this->actionIndex(Yii::$app->request->getQueryParam('user_id', null));
		}
		return Yii::$app->response->redirect(Yii::$app->request->referrer);
	}

	public function actionLog($id = null)
	{
		// TODO похоже этот экшн не используется
		$log = Log::find()
			->where('account_id = ' . $id)
			->orderBy([
				'date' => SORT_DESC,
			])
			->all();

		return $this->render('log', [
			'log' => $log,
		]);
	}

	public function actionClearLog($id)
	{
		/** @var Account $account */
		$account = Account::find()->where(['instagram_id' => $id])->one();
		if (empty($account->server)){
			Yii::$app->getResponse()->redirect(Yii::$app->request->referrer);
		}
		$curl_handle = curl_init();
		curl_setopt($curl_handle, CURLOPT_URL, 'http://'.$account->server.'/userLogClear.php');
		curl_setopt($curl_handle, CURLOPT_POSTFIELDS, 'id=' . $id);
		curl_setopt($curl_handle, CURLOPT_POST, true);
		curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
		curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
		curl_exec($curl_handle);
		curl_close($curl_handle);

		Yii::$app->session->setFlash('LogClear');
		Yii::$app->getResponse()->redirect(Yii::$app->request->referrer);
	}

	public function actionRestartStop()
	{
		$result = false;
		$accounts = Account::find()->all();
		/* @var $accounts \app\models\Account[] */
		$_SESSION['restartProjects'] = array();
		if ($accounts) {
			foreach ($accounts as $account) {
				if ($account->getStatus()) {
					$_SESSION['restartProjects'][] = $account->id;
					//$result = $account->accountStop(true);
					$result = RPCHelper::stopAccount($account, 'admin');
				}
			}
		}
		if ($result) {
			Yii::$app->session->setFlash('Project successful started');
			Yii::$app->getResponse()->redirect(['admin/projects']);
		}
		return '';
	}

	public function actionRestartStart()
	{
		$result = false;
		if (count($_SESSION['restartProjects'])) {
			foreach ($_SESSION['restartProjects'] as $id) {
				/** @var Account $account */
				$account = Account::findOne($id);
				//$result = $account->accountStart(true);
				$result = RPCHelper::startAccount($account, 'admin');
			}
			unset($_SESSION['restartProjects']);
		}
		if ($result) {
			Yii::$app->session->setFlash('Project successful started');
			Yii::$app->getResponse()->redirect(['admin/projects']);
		}
		return '';
	}

	public function actionRestartRestart()
	{
		$result = false;
		$started = Account::getActiveAccounts();
		/** @var Account[] $accounts */
		$accounts  = Account::find()->where(['instagram_id' => $started])->all();
		foreach ($accounts as $account) {
			//$account->accountStop(true);
			//$account->accountStart(true)
			RPCHelper::stopAccount($account, 'admin');
			RPCHelper::startAccount($account, 'admin');;
		}
		if ($result) {
			Yii::$app->session->setFlash('Project successful started');
			Yii::$app->getResponse()->redirect(['admin/projects']);
		}
		return '';
	}

	public function actionLogs($id = null, $email = null, $account = null)
	{
		if ($id) {
			$project = AccountsLog::findOne(['id' => $id]);

			if ($project == null || !$project->id) {
				throw new ForbiddenHttpException(Yii::t('app', 'You cannot view that resource'));
			}

			return $this->render('log', [
				'project' => $project,
			]);
		}

		$query = AccountsLog::find();

		if (!empty($email)) {
			$query->andWhere(['ILIKE', 'clientmail', trim($email)]);
		}

		if (!empty($account)) {
			$query->andWhere(['ILIKE', 'data', '"username":"'.trim($account)]);
		}

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => ['defaultOrder' => ['datetime' => SORT_DESC]],
		]);

		return $this->render('logs', [
			'dataProvider' => $dataProvider,
			'email' => $email,
			'account' => $account,
		]);
	}

	public function actionWhitelist($instagram_id = null)
	{
		
		if (!$instagram_id) {
			return false;
		}

		$whitelist = Whitelist::find()->where([
			'account_id' => $instagram_id
		]);

		$dataProvider = new ActiveDataProvider([
			'query' => $whitelist,
		]);

		return $this->render('whitelist', [
			'dataProvider' => $dataProvider,
		]);
	}
}