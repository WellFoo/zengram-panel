<?php

namespace app\modules\admin\controllers;

use app\components\Controller;
use app\models\Account;
use app\models\AccountStartPlaceStats;
use app\models\AccountStats;
use app\models\ActionsLog;
use app\models\BalanceFlow;
use app\models\Options;
use app\models\SupportRequest;
use app\models\UsageStats;
use app\models\Users;
use app\modules\admin\models\AccountBlocks;
use app\modules\admin\models\AccountStatsSearch;
use app\models\Actions;
use app\models\Invoice;
use app\models\Place;
use app\models\Prices;
use app\models\Proxy;
use app\models\Regions;
use app\models\Statistics;
use app\modules\admin\models\Balance;
use app\modules\admin\models\OverallStatistics;
use PDO;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use Yii;

class StatsController extends Controller
{
	public function actionIndex()
	{
		return $this->actionOverall();
	}

	public function actionOverall()
	{
		$statsAll = new OverallStatistics(OverallStatistics::ALLTIME_INTERVAL);
		$statsMonth = new OverallStatistics(OverallStatistics::MONTH_INTERVAL);
		$statsWeek = new OverallStatistics(OverallStatistics::WEEK_INTERVAL);
		return $this->render('overall', [
			'stats' => [$statsWeek, $statsMonth, $statsAll]
		]);
	}

	public function actionFunctions()
	{
		$activeQuery = Options::find()->innerJoin(
			Account::tableName(),
			'options.account_id = accounts.id'
		)->where([
			'monitoring_status' => Account::STATUS_WORK
		]);

		$active = [
			'users'        => Users::countActive(),
			'accounts'     => Account::find()->where(['monitoring_status' => Account::STATUS_WORK])->count(),
		];
		$query = (clone $activeQuery);
		$active['geo_place'] = $query->andWhere(['options.search_by' => 1])->count();
		$query = (clone $activeQuery);
		$active['geo_regions'] = $query->andWhere(['options.search_by' => 3])->count();
		$query = (clone $activeQuery);
		$active['hashtags'] = $query->andWhere(['options.search_by' => 4])->count();
		$query = (clone $activeQuery);
		$active['competitors'] = $query->andWhere(['options.search_by' => 8])->count();
		$query = (clone $activeQuery);
		$active['geotags'] = $query->andWhere(['options.use_geotags' => true])->count();

		return $this->render('functions', [
			'data_all' => [
				'users'       => Users::find()->count(),
				'accounts'    => Account::find()->count(),
				'geo_place'   => Options::find()->where(['search_by' => 1])->count(),
				'geo_regions' => Options::find()->where(['search_by' => 3])->count(),
				'hashtags'    => Options::find()->where(['search_by' => 4])->count(),
				'competitors' => Options::find()->where(['search_by' => 8])->count(),
				'geotags'     => Options::find()->where(['use_geotags' => true])->count(),
			],
			'data_active' => $active,
		]);
	}

	public function actionProjects($id = null)
	{
		if ($id === null) {
			return $this->render('projects_list', [
				'accounts' => Account::find()->asArray()->all()
			]);
		}

		$selections = [];

		$data = Statistics::find()->where('datetime > \'' . date('Y-m-d H:i:s', strtotime(' -1 day')) . '\' AND account_id = \'' . $id . '\'')->groupBy('likes')->orderBy('datetime')->all();
		if (count($data) > 1 && $data['0']['likes'] !== 0) {
			$selections[] = 'likes';
		}

		$data = Statistics::find()->where('datetime > \'' . date('Y-m-d H:i:s', strtotime(' -1 day')) . '\' AND account_id = \'' . $id . '\'')->groupBy('comments')->orderBy('datetime')->all();

		if (count($data) > 1 && $data['0']['comments'] !== 0) {
			$selections[] = 'comments';
		}

		$data = Statistics::find()->where('datetime > \'' . date('Y-m-d H:i:s', strtotime(' -1 day')) . '\' AND account_id = \'' . $id . '\'')->groupBy('follow')->orderBy('datetime')->all();
		if (count($data) > 1 && $data['0']['follow'] !== 0) {
			$selections[] = 'follow';
		}

		$data = Statistics::find()->where('datetime > \'' . date('Y-m-d H:i:s', strtotime(' -1 day')) . '\' AND account_id = \'' . $id . '\'')->groupBy('unfollow')->orderBy('datetime')->all();
		if (count($data) > 1 && $data['0']['unfollow'] !== 0) {
			$selections[] = 'unfollow';
		}

		return $this->render('project', [
			'selections' => $selections,
			'id'         => $id,
		]);
	}

	public function actionLikes($id)
	{
		if (!$id) {
			throw new BadRequestHttpException();
		}

		$statistic = [];
		/** @var Statistics[] $data */
		$data = Statistics::find()->where(sprintf(
			"datetime > '%s' AND account_id = '%d'",
			date('Y-m-d H:i:s', strtotime(' -1 day')),
			intval($id)
		))->orderBy('datetime')->all();

		foreach ($data as $stat) {
			$statistic[] = [
				strtotime($stat->datetime) * 1000,
				$stat->likes,
			];
		}

		return json_encode($statistic);
	}

	public function actionComments($id)
	{
		if (!$id) {
			throw new BadRequestHttpException();
		}
		$statistic = [];
		/** @var Statistics[] $data */
		$data = Statistics::find()->where(sprintf(
			"datetime > '%s' AND account_id = '%d'",
			date('Y-m-d H:i:s', strtotime(' -1 day')),
			intval($id)
		))->orderBy('datetime')->all();

		foreach ($data as $stat) {
			$statistic[] = [
				strtotime($stat->datetime) * 1000,
				$stat->comments,
			];
		}

		return json_encode($statistic);
	}

	public function actionFollow($id)
	{
		if (!$id) {
			throw new BadRequestHttpException();
		}
		$statistic = [];
		/** @var Statistics[] $data */
		$data = Statistics::find()->where(sprintf(
			"datetime > '%s' AND account_id = '%d'",
			date('Y-m-d H:i:s', strtotime(' -1 day')),
			intval($id)
		))->orderBy('datetime')->all();

		foreach ($data as $stat) {
			$statistic[] = [
				strtotime($stat->datetime) * 1000,
				$stat->follow,
			];
		}

		return json_encode($statistic);
	}

	public function actionUnfollow($id)
	{
		if (!$id) {
			throw new BadRequestHttpException();
		}
		$statistic = [];
		/** @var Statistics[] $data */
		$data = Statistics::find()->where(sprintf(
			"datetime > '%s' AND account_id = '%d'",
			date('Y-m-d H:i:s', strtotime(' -1 day')),
			intval($id)
		))->orderBy('datetime')->all();

		foreach ($data as $stat) {
			$statistic[] = [
				strtotime($stat->datetime) * 10000,
				$stat->unfollow,
			];
		}

		return json_encode($statistic);
	}

	public function actionActions($type = null)
	{
		if ($type) {
			switch ($type) {
				case 'vkontakte':
					break;
				case 'facebook':
					break;
				case 'mailru':
					break;
			}
			return $this->render('actionsShare');
		}

		return $this->render('actions', [
			'actions' => Actions::find()->asArray()->all()
		]);
	}

	public function actionAccountsSummary()
	{
		$searchProvider = new AccountStatsSearch();
		$dataProvider = $searchProvider->search();
		/** @var ActiveQuery $summaryQuery */
		$summaryQuery = (clone $dataProvider->query);

		$simple_columns = [
			'ast.account_id',
			'ast.user_id',
			'ast.instagram_id',
			'ast.date',
			'speed',
			'trial',
			'u.is_service',
		];
		$select = $summaryQuery->select;
		foreach ($select as $key => $value) {
			if (in_array($value, $simple_columns)) {
				unset($select[$key]);
			}
		}

		$summaryQuery
			->select($select)
			->groupBy('trial')
			->andWhere(['is_service' => 0])
			->orderBy(['trial' => SORT_ASC])
			->asArray();

		/** @var ActiveQuery $query */
		$query = (clone $dataProvider->query);
		$query->select([
			'COUNT(DISTINCT ast.account_id) as count',
			'SUM(ast.likes) as likes',
			'SUM(ast.comments) as comments',
			'SUM(ast.follows) as follows',
			'SUM(ast.unfollows) as unfollows',
			'SUM(photos) as photos',
			'SUM(ast.manual_comments) as manual_comments',
			'SUM(ast.password_resets) as password_resets',
			'AVG(ast.speed) as speeds',
			'SUM(ast.came) as came',
			'SUM(ast.gone) as gone',
			'SUM(ast.likes_time) as likes_time',
			'SUM(ast.comments_time) as comment_time',
			'SUM(ast.follows_time) as follow_time',
			'SUM(ast.unfollows_time) as unfollow_time',
		]);

		$serviceQuery = clone $query;
		$serviceQuery
			->groupBy(['u.is_service'])
			->andWhere(['is_service' => 1]);

		/** @var ActiveQuery $overallQuery */
		$overallQuery = clone $query;
		$overallQuery->groupBy(null);

		/** @var ActiveQuery $instagram_ids */
		$instagram_ids = clone $query;
		$instagram_ids->select('a.instagram_id');
		$instagram_ids->groupBy(['a.instagram_id']);

		$searchProvider->instagram_id = Account::getStatusesByInstagramIdsList($instagram_ids->column());

		/** @var ActiveQuery $proxyQuery */
		$proxyQuery = clone $dataProvider->query;
		$proxyQuery
			->select([
				new Expression('COUNT(*) AS count'),
				'ast.proxy_id',
				'date'
			])
			->innerJoin(['p' => 'proxy'], 'p.id = a.proxy_id')
			->groupBy(['ast.proxy_id', 'date']);

		/** @var Query $minQuery */
		$minProxies = Proxy::find()
			->select([new Expression('MIN(a.count) as min'), 'proxy', 'id'])
			->innerJoin(['a' => $proxyQuery], 'a.proxy_id = proxy.id')
			->groupBy(['a.proxy_id', 'proxy', 'id'])
			->orderBy(['min' => SORT_ASC])
			->limit(1)
			->asArray();

		/** @var Query $minQuery */
		$maxProxies = Proxy::find()
			->select([new Expression('MAX(a.count) as max'), 'proxy', 'id'])
			->innerJoin(['a' => $proxyQuery], 'a.proxy_id = '.Proxy::tableName().'.id')
			->groupBy(['a.proxy_id', 'proxy', 'id'])
			->orderBy(['max' => SORT_DESC])
			->limit(1)
			->asArray();

		/** @var Query $avgQuery */
		$avgProxies = Proxy::find()
			->select([new Expression('AVG(a.count) as avg'), 'proxy', 'id'])
			->innerJoin(['a' => $proxyQuery], 'a.proxy_id = proxy.id')
			->groupBy(['proxy', 'id'])
			->asArray();

		$moscowAccounts = (new Query)
			->select(['id' => 'account_id', 'acc_cnt' => 'COUNT(*)'])
			->from('account_places')
			->where(['in', 'place_id', Place::find()->select(['id'])->where(['name' => 'Москва'])])
			->groupBy('account_id');

		$moscowAccountsIDs = (new Query)
			->select('id')
			->from(['asq' => $moscowAccounts])
			->where(['acc_cnt' => 1]);
		$moscowQuery = clone $overallQuery;
		$moscowQuery->andWhere(['in', 'a.id', $moscowAccountsIDs]);

		return $this->render('summary', [
			'date'       => $searchProvider->date,
			'dateFrom'   => $searchProvider->dateFrom,
			'dateTo'     => $searchProvider->dateTo,
			'summary'    => $summaryQuery->all(),
			'service'    => $serviceQuery->asArray()->one(),
			'overall'    => $overallQuery->asArray()->one(),
			'minProxies' => $minProxies->all(),
			'maxProxies' => $maxProxies->all(),
			'avgProxies' => $avgProxies->all(),
			'moscow'     => $moscowQuery->asArray()->one(),
		]);
	}
	

	public function actionAccountsActions($id = null)
	{
		$searchProvider = new AccountStatsSearch();
		$dataProvider = $searchProvider->search();

		$simple_columns = [
			'account_id',
			'ast.user_id',
			'ast.instagram_id',
			'speed',
			'trial',
			'u.is_service',
		];

		$dataProviderSummary = null;

		if ($id !== null) {
			$dataProvider->query->andWhere(['account_id' => $id]);

			$dataProviderSummary = clone $dataProvider;
			$dataProviderSummary->query = clone $dataProvider->query;
			$dataProviderSummary->query->groupBy($simple_columns);

			$dataProvider->query->addSelect('ast.date');
			array_unshift($simple_columns, 'ast.date');
			$idx = 0;
			$dataProvider->query->indexBy(function() use(&$idx) {
				return $idx++;
			});
		} else {
			$dataProvider->query->with(['account', 'user']);
		}

		$dataProvider->query->groupBy($simple_columns);

		/** @var ActiveQuery $instagram_id */
		$instagram_id = clone $dataProvider->query;
		$instagram_id->select('a.instagram_id')->groupBy('a.instagram_id');

		$searchProvider->instagram_id = Account::getStatusesByInstagramIdsList($instagram_id->column());

		$template = $id === null ? 'accounts' : 'account';
		return $this->render($template, [
			'dataProvider' => $dataProvider,
			'dataProviderSummary' => $dataProviderSummary,
			'filterModel'  => $searchProvider,
			'date'         => $searchProvider->date,
			'dateFrom'     => $searchProvider->dateFrom,
			'dateTo'       => $searchProvider->dateTo,
			'account_id'   => $id
		]);
	}

	public function actionPayments()
	{
		$mainQuery = Invoice::find()->where(['status' => Invoice::STATUS_SUCCESS]);
		$dateTo = strtotime(date('Y-m-d'));
		$date = Yii::$app->request->get('date', 'day');
		switch ($date) {
			case 'day':
			default:
				$dateFrom = $dateTo - 86400;
				break;

			case 'week':
				$dateFrom = $dateTo - 86400 * 7;
				break;

			case 'month':
				$dateFrom = $dateTo - 86400 * 30;
				break;

			case 'range':
				$dateFrom = strtotime(Yii::$app->request->get('from'));
				$dateTo = strtotime(Yii::$app->request->get('to'));
				break;
		}

		$mainQuery
			->andWhere(['>=', 'date', date('Y-m-d', $dateFrom)])
			->andWhere(['<', 'date', date('Y-m-d', $dateTo)])

			// Ислючаем инвойсы из instashpion
			->andWhere(['!=', 'price_id', 9]);

		$countQuery = clone $mainQuery;
		$sumQuery = clone $mainQuery;
		$sumQueryPaypal = clone $sumQuery;
		$sumQueryPaypal->andWhere(['agent' => 'paypal']);
		$sumQueryRobokassa = clone $sumQuery;
		$sumQueryRobokassa->andWhere(['agent' => 'interkassa']);
		$sumQuery2co = clone $sumQuery;
		$sumQuery2co->andWhere(['agent' => '2co']);
		$sumQueryYad = clone $sumQuery;
		$sumQueryYad->andWhere(['agent' => 'yandex_card']);
		$invoiceSubQuery = clone $mainQuery;
		$invoiceSubQuery
			->select('price_id, COUNT(id) AS count_price, SUM(sum) AS sum_price')
			->groupBy('price_id');

		$countPacketQuery = Prices::find()
			->leftJoin(['i' => $invoiceSubQuery], 'i.price_id = ' . Prices::tableName() . '.id')
			->select(['i.count_price', 'i.sum_price', 'price', Prices::tableName() . '.id'])
			->limit(null)
			->asArray();

		$countPacketDataProvider = new ActiveDataProvider([
			'query' => $countPacketQuery
		]);
		$minIdsQuery = clone $mainQuery;
		$minIdsQuery->groupBy('user_id');
		$minIds = $minIdsQuery->where(['status' => Invoice::STATUS_SUCCESS])->select(new Expression('MIN(id)'))->column();
		$invoiceFirstUserQuery = clone $mainQuery;
		$invoiceFirstUserQuery->andWhere(['id' => $minIds]);
		$invoiceAfterUserQuery = clone $mainQuery;
		$invoiceAfterUserQuery->andWhere(['not in', 'id', $minIds]);

//		$dataProvider = new ActiveDataProvider([
//				'query' => $query->groupBy('account_id'),
//		]);

		return $this->render('payments', [
			'count'           => $countQuery->count(),
			'sum'             => $sumQuery->sum('sum'),
			'sumPaypal'       => $sumQueryPaypal->sum('sum'),
			'sumRobokassa'    => $sumQueryRobokassa->sum('sum'),
			'sum2co'          => $sumQuery2co->sum('sum'),
			'sumYad'          => $sumQueryYad->sum('sum'),
			'colPackets'      => $countPacketDataProvider->getModels(),
			'countUsersFirst' => $invoiceFirstUserQuery->count(),
			'countUsersAfter' => $invoiceAfterUserQuery->count(),
			'sumUsersFirst'   => $invoiceFirstUserQuery->sum('sum'),
			'sumUsersAfter'   => $invoiceAfterUserQuery->sum('sum'),
			'date'            => $date,
			'dateFrom'        => $dateFrom,
			'dateTo'          => $dateTo
		]);

	}

	public function actionSuggestPromo()
	{
		$mainQuery = SupportRequest::find()->where(['subject' => ['Promo action reply', Yii::t('app', 'Promo action reply')]]);
		$dateTo = strtotime(date('Y-m-d'));
		$date = Yii::$app->request->get('date', 'day');
		switch ($date) {
			case 'day':
			default:
				$dateFrom = $dateTo - 86400;
				break;

			case 'week':
				$dateFrom = $dateTo - 86400 * 7;
				break;

			case 'month':
				$dateFrom = $dateTo - 86400 * 30;
				break;

			case 'range':
				$dateFrom = strtotime(Yii::$app->request->get('from'));
				$dateTo = strtotime(Yii::$app->request->get('to'));
				break;
		}

		$mainQuery->andWhere(['>=', 'added', date('Y-m-d', $dateFrom)])
			->andWhere(['<', 'added', date('Y-m-d', $dateTo)]);

		$dataProvider = new ActiveDataProvider([
			'query' => $mainQuery,
			'sort'  => [
				'defaultOrder' => ['added' => SORT_DESC]
			]
		]);

		return $this->render('suggest', [
			'count'           => $mainQuery->count(),
			'dataprovider'    => $dataProvider,
			'date'            => $date,
			'dateFrom'        => $dateFrom,
			'dateTo'          => $dateTo
		]);

	}

	/*
	public function actionRegions()
	{
		$mainQuery = AccountStartPlaceStats::find();
		$dateTo = strtotime(date('Y-m-d'));
		$date = Yii::$app->request->get('date', 'day');
		switch ($date) {
			case 'day':
			default:
				$dateFrom = $dateTo - 86400;
				break;

			case 'week':
				$dateFrom = $dateTo - 86400 * 7;
				break;

			case 'month':
				$dateFrom = $dateTo - 86400 * 30;
				break;

			case 'range':
				$dateFrom = strtotime(Yii::$app->request->get('from'));
				$dateTo = strtotime(Yii::$app->request->get('to'));
				break;
		}

		$mainQuery
			->select([new Expresion('COUNT(*) AS count'), 'type_id'])
			->andWhere(['>=', 'date', date('Y-m-d', $dateFrom)])
			->andWhere(['<', 'date', date('Y-m-d', $dateTo)])
			->groupBy('type_id');

		$countryLevel = clone $mainQuery;

		$countryLevel
			->innerJoinWith('place')
			->andWhere(['type' => 'place'])
			->groupBy(Place::tableName() . '.country')
			->select([
				new Expression('COUNT(*) AS count'),
				'country AS name',
				'type_id'
			])
			->andWhere(['!=', 'country', ''])
			->asArray();

		$regionLevel = clone $mainQuery;

		$regionLevel
			->innerJoinWith('place')
			->andWhere(['type' => 'place'])
			->groupBy(Place::tableName() . '.region')
			->select([
			new Expression('COUNT(*) AS count'),
				'region AS name',
				'type_id'
			])
			->andWhere(['!=', 'region', ''])
			->asArray();

		$cityLevel = clone $mainQuery;

		$cityLevel
			->innerJoinWith('place')
			->andWhere(['type' => 'place'])
			->groupBy(Place::tableName() . '.name')
			->select([
				new Expression('COUNT(*) AS count'),
				'name',
				'type_id'
			])->asArray();

		$regionsQuery = clone $mainQuery;

		$regionsQuery
			->innerJoinWith('region')
			->andWhere(['type' => 'region'])
			->groupBy('type_id')
			->select([
				new Expression('COUNT(*) AS count'),
				'type_id',
				'parent_id',
				'name'
			])->asArray();

		$regions = $regionsQuery->all();
		$countedRegions = [];
		$parents = [];
		$i = 0;

		while ($i < count($regions)) {
			$region = $regions[$i++];
			if ($region['parent_id'] !== null) {
				if (!array_key_exists($region['parent_id'], $parents)){
					$parent_region = Regions::find()->andWhere(['id' => $region['parent_id']])->asArray()->one();
					$parent_region['count'] = $region['count'];
					$parents[$region['parent_id']] = $parent_region;
					$regions[] = $parent_region;
				}
			}
			if (array_key_exists($region['name'], $countedRegions)) {
				$countedRegions[$region['name']]['count'] += $region['count'];
			} else {
				$countedRegions[$region['name']] = $region;
			}
		}
		$countries = $countryLevel->all();
		foreach ($countries as &$country) {
			if (array_key_exists($country['name'], $countedRegions)) {
				$country['count'] += $countedRegions[$country['name']]['count'];
				unset($countedRegions[$country['name']]);
			}
		}
		$regionsArray = $regionLevel->all();
		foreach ($regionsArray as &$regionItem) {
			if (array_key_exists($regionItem['name'], $countedRegions)) {
				$regionItem['count'] += $countedRegions[$regionItem['name']]['count'];
				unset($countedRegions[$regionItem['name']]);
			}
		}
		$regionsArray = array_merge($regionsArray, $countedRegions);
		unset($countedRegions);
		$cities = $cityLevel->all();
		usort($countries, [$this, 'compare']);
		usort($regionsArray, [$this, 'compare']);
		usort($cities, [$this, 'compare']);
		return $this->render('regions', [
			'countries'      => $countries,
			'regions'        => $regionsArray,
			'cities'         => $cities,
			'date'          => $date,
			'dateFrom'      => $dateFrom,
			'dateTo'        => $dateTo
		]);
	} */

	public function actionRegions()
	{
		$dateTo = strtotime(date('Y-m-d'));
		$date = Yii::$app->request->get('date', 'day');
		switch ($date) {
			case 'day':
			default:
				$dateFrom = $dateTo - 86400;
				break;

			case 'week':
				$dateFrom = $dateTo - 86400 * 7;
				break;

			case 'month':
				$dateFrom = $dateTo - 86400 * 30;
				break;

			case 'range':
				$dateFrom = strtotime(Yii::$app->request->get('from'));
				$dateTo = strtotime(Yii::$app->request->get('to'));
				break;
		}

		$mainQuery = AccountStartPlaceStats::find()
			->select([new Expression('COUNT(*) AS count'), 'type_id'])
			->andWhere(['>=', 'date', date('Y-m-d', $dateFrom)])
			->andWhere(['<', 'date', date('Y-m-d', $dateTo)])
			->groupBy('type_id');

		$countryLevel = clone $mainQuery;
		$countryLevel = (new Query())
			->select(['SUM(sq.count) AS count', 't.country AS name'])
			->from(['sq' => $countryLevel->andWhere(['type' => 'place'])])
			->innerJoin(['t' => Place::tableName()], 'sq.type_id = t.id AND t.country != \'\'')
			->groupBy('t.country');

		$regionLevel = clone $mainQuery;
		$regionLevel = (new Query())
			->select(['SUM(sq.count) AS count', 't.region AS name'])
			->from(['sq' => $regionLevel->andWhere(['type' => 'place'])])
			->innerJoin(['t' => Place::tableName()], 'sq.type_id = t.id AND t.region != \'\'')
			->groupBy('t.region');

		$cityLevel = clone $mainQuery;
		$cityLevel = (new Query())
			->select(['sq.count', 't.name'])
			->from(['sq' => $cityLevel->andWhere(['type' => 'place'])])
			->innerJoin(['t' => Place::tableName()], 'sq.type_id = t.id');

		$regionsQuery = clone $mainQuery;
		$regionsQuery = (new Query())
			->select(['sq.count', 'parent_id', 't.name'])
			->from(['sq' => $regionsQuery->andWhere(['type' => 'region'])])
			->innerJoin(['t' => Regions::tableName()], 'sq.type_id = t.id');

		$regions = $regionsQuery->all();
		$countedRegions = [];
		$parents = [];
		$i = 0;
		while ($i < count($regions)) {
			$region = $regions[$i++];
			if ($region['parent_id'] !== null) {
				if (!array_key_exists($region['parent_id'], $parents)){
					$parent_region = Regions::find()->andWhere(['id' => $region['parent_id']])->asArray()->one();
					$parent_region['count'] = $region['count'];
					$parents[$region['parent_id']] = $parent_region;
					$regions[] = $parent_region;
				}
			}
			if (array_key_exists($region['name'], $countedRegions)) {
				$countedRegions[$region['name']]['count'] += $region['count'];
			} else {
				$countedRegions[$region['name']] = $region;
			}
		}
		$countries = $countryLevel->all();
		foreach ($countries as &$country) {
			if (array_key_exists($country['name'], $countedRegions)) {
				$country['count'] += $countedRegions[$country['name']]['count'];
				unset($countedRegions[$country['name']]);
			}
		}
		$regionsArray = $regionLevel->all();
		foreach ($regionsArray as &$regionItem) {
			if (array_key_exists($regionItem['name'], $countedRegions)) {
				$regionItem['count'] += $countedRegions[$regionItem['name']]['count'];
				unset($countedRegions[$regionItem['name']]);
			}
		}
		$regionsArray = array_merge($regionsArray, $countedRegions);
		unset($countedRegions);
		$cities = $cityLevel->all();
		usort($countries, [$this, 'compare']);
		usort($regionsArray, [$this, 'compare']);
		usort($cities, [$this, 'compare']);

		return $this->render('regions', [
			'countries'      => $countries,
			'regions'        => $regionsArray,
			'cities'         => $cities,
			'date'          => $date,
			'dateFrom'      => $dateFrom,
			'dateTo'        => $dateTo
		]);
	}

	public function actionBlocks()
	{
		$dateTo = strtotime(date('Y-m-d'));
		$date = Yii::$app->request->get('date', 'day');
		switch ($date) {
			case 'day':
			default:
				$dateFrom = $dateTo - 86400;
				break;

			case 'week':
				$dateFrom = $dateTo - 86400 * 7;
				break;

			case 'month':
				$dateFrom = $dateTo - 86400 * 30;
				break;

			case 'range':
				$dateFrom = strtotime(Yii::$app->request->get('from'));
				$dateTo = strtotime(Yii::$app->request->get('to'));
				break;
		}

		$query = AccountBlocks::find()
			->where(['>=', 'block_date', date('Y-m-d', $dateFrom)])
			->andWhere(['<', 'block_date', date('Y-m-d', $dateTo)]);

		$commentsQuery = clone $query;
		$comments = (int) $commentsQuery
			->andWhere(['action' => 'comments'])
			->count();

		$followsQuery = clone $query;
		$follows = (int)$followsQuery
			->andWhere(['action' => 'follows'])
			->count();

		$followsTimeoutQuery = clone $query;
		$follows_timeout = (int)$followsTimeoutQuery
			->andWhere(['action' => 'follows_timeout'])
			->count();

		$commentsSpamQuery = clone $query;
		$comments_spam = (int)$commentsSpamQuery
			->andWhere(['action' => 'spam'])
			->count();

		$unfollowQuery = clone $query;
		$unfollow = (int) $unfollowQuery
			->andWhere(['action' => AccountBlocks::ACTION_UNFOLLOWS])
			->count();

		return $this->render('blocks', [
			'comments'  => $comments,
			'follows'   => $follows,
			'follows_timeout'      => $follows_timeout,
			'spam'      => $comments_spam,
			'unfollow'  => $unfollow,
			'date'      => $date,
			'date_form' => $dateFrom,
			'date_to'   => $dateTo
		]);
	}

	public function actionDiscountActions()
	{
		$dateTo = strtotime(date('Y-m-d'));
		$date = Yii::$app->request->get('date', 'day');
		switch ($date) {
			case 'day':
			default:
				$dateFrom = $dateTo - 86400;
				break;

			case 'week':
				$dateFrom = $dateTo - 86400 * 7;
				break;

			case 'month':
				$dateFrom = $dateTo - 86400 * 30;
				break;

			case 'range':
				$dateFrom = strtotime(Yii::$app->request->get('from'));
				$dateTo = strtotime(Yii::$app->request->get('to'));
				break;
		}

		$query = ActionsLog::find()
			->where(['>=', 'added', date('Y-m-d', $dateFrom)])
			->andWhere(['<', 'added', date('Y-m-d', $dateTo)]);

		return $this->render('discount_actions', [
			'count'  => $query->count(),
			'cost'   => $query->sum('cost'),
			'date'      => $date,
			'dateFrom' => $dateFrom,
			'dateTo'   => $dateTo,
			'dataProvider'  =>  new ActiveDataProvider([
				'query' => $query->groupBy(['instagram_id', 'id']),
			]),
		]);
	}

	public function actionBlocksDetails()
	{
		$action = Yii::$app->request->get('action');
		if (!in_array($action, array_keys(AccountBlocks::actionsLabels()))) {
			throw new BadRequestHttpException('Неизвестное действие');
		}

		$dateTo = strtotime(date('Y-m-d'));
		$date = Yii::$app->request->get('date', 'day');
		switch ($date) {
			case 'day':
			default:
				$dateFrom = $dateTo - 86400;
				break;

			case 'week':
				$dateFrom = $dateTo - 86400 * 7;
				break;

			case 'month':
				$dateFrom = $dateTo - 86400 * 30;
				break;

			case 'range':
				$dateFrom = strtotime(Yii::$app->request->get('from'));
				$dateTo = strtotime(Yii::$app->request->get('to'));
				break;
		}

		$query = AccountBlocks::find()
			->with(['user', 'account'])
			->where(['>=', 'block_date', date('Y-m-d', $dateFrom)])
			->andWhere(['<', 'block_date', date('Y-m-d', $dateTo)])
			->leftJoin(Users::tableName(), Users::tableName().'.id = '.AccountBlocks::tableName().'.user_id')
			->leftJoin(Account::tableName(), Account::tableName().'.id = '.AccountBlocks::tableName().'.account_id')
			->andWhere(['action' => $action]);

		$email = Yii::$app->request->get('email');
		$account = Yii::$app->request->get('account');

		if (!empty($email)) {
			$query->andWhere(['ILIKE', Users::tableName().'.mail', trim($email)]);
		}

		if (!empty($account)) {
			$query->andWhere(['ILIKE', 'login', trim($account)]);
		}


		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => [
				'defaultOrder' => ['block_date' => SORT_DESC]
			]
		]);

		return $this->render('blocks_details', [
			'data_provider' => $dataProvider,
			'action'        => $action,
			'account'       => $account,
			'email'         => $email,
			'date'          => $date,
			'date_form'     => $dateFrom,
			'date_to'       => $dateTo
		]);
	}

	function compare($element_1, $element_2)
	{
		$direction = Yii::$app->request->get('sort', 'ASC');
		$sort = Yii::$app->request->get('by', 'count');
		if ($sort === 'count'){
			if ($element_1['count'] === $element_2['count']){
				return 0;
			}
			if (strtoupper($direction) === 'ASC'){
				return ($element_1['count'] < $element_2['count'] ? -1 : 1);
			} else {
				return ($element_1['count'] < $element_2['count'] ? 1 : -1);
			}
		} elseif ($sort === 'name'){
			if ($element_1['name'] === $element_2['name']){
				return 0;
			}
			$comparer = new \Collator('ru_RU');
			if (strtoupper($direction) === 'ASC'){
				return ($comparer->compare($element_1['name'], $element_2['name']) < 0 ? -1 : 1);
			} else {
				return ($comparer->compare($element_1['name'], $element_2['name']) < 0  ? 1 : -1);
			}
		}
		return 0;
	}

	public function actionUserTime()
	{

		$user_id = 3459;

		$user = Users::findOne($user_id);
		/*$records = BalanceFlow::findAll([
			'user_id' => $user_id,
			'type' => BalanceFlow::ADD_BALANCE
		]);*/

		$result = Yii::$app->db->createCommand(sprintf('
			SELECT SUM(a.value)
 			FROM balance_flow a
 			WHERE user_id = %s AND type = \'%s\'',
			$user->id,
			BalanceFlow::ADD_BALANCE
		))->queryAll();

		echo('Время работы: ' . Balance::formatBalance($result[0]['sum'] - $user->balance, '%r%D'));

		/** @var BalanceFlow $record */
		/*foreach ($records as $record) {
			echo($record->description . ' ' . Balance::formatBalance($record->value) . '<br>');
		}*/
		//return $this->render('user-time');
	}

	public function actionUsage()
	{
		return $this->render('usage', [
			'data' => UsageStats::find()->orderBy(['date' => SORT_ASC])->all()
		]);
	}
}