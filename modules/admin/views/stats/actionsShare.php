<div class="site-admin">
	<?php if(!empty($share)): ?>
		<ul class="share-links">
			<?php foreach($share as $link): ?>
				<li><a href="<?=$link?>"><?=$link?></a></li>
			<?php endforeach; ?>
		</ul>
	<?php else: ?>
		<p class="bg-warning" style="padding: 25px; text-align: center;">Ссылки не найдены</p>
	<?php endif; ?>
</div>