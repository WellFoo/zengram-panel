<?php
use yii\helpers\Url;
use yii\web\View;

/** @var array $selections */

$this->title = 'Статистика ID:'.$id;
$this->registerJsFile('@web/js/highstock/highstock.js', ['position' => View::POS_BEGIN]);
?>
<div class="site-admin">
	<div class="form-inline text-center">
		<form method="post">
			<div class="form-group">
				<label for="datefrom">С:</label>
				<input class="form-control date-picker" id="datefrom" name="datefrom" type="text" value="<?=date('Y-m-d', strtotime(' -1 day'))?>" data-date="<?=date('Y-m-d', strtotime(' -1 day'))?>" data-date-format="yyyy-mm-dd">
				<label for="dateto">По:</label>
				<input class="form-control date-picker" id="dateto" name="dateto" type="text" value="<?=date("Y-m-d")?>" data-date="<?=date("Y-m-d")?>" data-date-format="yyyy-mm-dd">
			</div>
			<button type="submit" class="btn btn-default">Применить</button>
		</form>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-12 col-sm-12">

			<?php if(count($selections)): ?>
				<div id="container" style="height: 400px; min-width: 600px;"></div>
			<?php else: ?>
				<p>Пока нет данных по этому аккаунту</p>
			<?php endif; ?>

		</div>
	</div>

</div>
<script>
	$(function () {

		var seriesOptions = [],
			seriesCounter = 0,
		//names = ['likes', 'comments', 'follow', 'unfollow'],
			names = <?=json_encode($selections); ?>,
		// create the chart when all data is loaded
			createChart = function () {

				$('#container').highcharts({

					chart: {
						type: 'line'
					},
					credits : {
						enabled : false
					},
					title: {
						text: false
					},

					rangeSelector: {
						selected: 2
					},

					xAxis: {
						title:{
							text: 'Day',
							style: {
								color: '#666666',
								fontSize: '12px',
								fontWeight: 'normal'
							}
						},
						type: 'datetime',
						dateTimeLabelFormats: {
							day: '%e.%b'
						},
						showFirstLabel: false
					},

					series: seriesOptions
				});
			};

		$.each(names, function (i, name) {

			$.getJSON(
				'<?=Url::to(['/admin/stats'])?>/' + name.toLowerCase() +'?id=' + '<?=$id; ?>',
				function (data) {
					seriesOptions[i] = {
						name: name,
						data: data
					};

					// As we're loading the data asynchronously, we don't know what order it will arrive. So
					// we keep a counter and create the chart when all the data is loaded.
					seriesCounter += 1;

					if (seriesCounter === names.length) {
						createChart();
					}
				}
			);
		});
	});
</script>