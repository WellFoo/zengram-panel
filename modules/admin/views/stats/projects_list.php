<?php

use yii\helpers\Html;
use yii\helpers\Url;

/** @var \app\models\Account[] $accounts */

$this->title = 'Stats';
?>
<div class="site-admin">

	<table class="table">
		<thead>
		<tr>
			<th>id</th>
			<th>Логин</th>
			<th><i class="fa fa-heart"></i> Лайки</th>
			<th><i class="fa fa-comment"></i> Комментарии</th>
			<th><i class="fa fa-check"></i> Подписываться</th>
			<th><i class="fa fa-close"></i> Отписываться</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach($accounts as $account): ?>
			<tr>
				<td><?= Html::a($account['instagram_id'], Url::to(['/admin/stats/projects/', 'id' => $account['instagram_id']])); ?></td>
				<td><?= Html::a($account['login'], Url::to(['/admin/stats/projects/', 'id' => $account['instagram_id']])); ?></td>
				<td class="text-center"><div id="likes"><?=$account['likes'];?></div></td>
				<td class="text-center"><div id="likes"><?=$account['comments'];?></div></td>
				<td class="text-center"><div id="likes"><?=$account['follow'];?></div></td>
				<td class="text-center"><div id="likes"><?=$account['unfollow'];?></div></td>
				<td><div id=""></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

</div>
<script>

</script>