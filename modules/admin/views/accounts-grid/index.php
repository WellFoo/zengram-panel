<?php

use app\models\Account;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var $this yii\web\View
 * @var $dataProvider yii\data\ActiveDataProvider
 */
$this->title = 'Сетка аккаунтов';

$account_statuses = Account::getStatusesByInstagramIdsList(ArrayHelper::getColumn($dataProvider->models, 'account.instagram_id'));
if ($account_statuses === null) {
	$account_statuses = [];
}

$dataProvider->sort = false;
?>
<style>
.read-message {
	display: block;
	position: inherit;
}
</style>
<div id="account-grid-container" class="site-admin">
	<h1><?=$this->title?></h1>

	<p>
		<?= Html::a('Добавить аккаунт', ['/admin/accounts-grid/create'], ['class' => 'btn btn-success']) ?>
		<?= Html::a('Импортировать аккаунты', ['/admin/accounts-grid/import'], ['class' => 'btn btn-primary']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			//['class' => 'yii\grid\SerialColumn'],

			[
				'label'  => '',
				'format' => 'raw',
				'value'  => function($item) use ($account_statuses) {
					/** @var \app\modules\admin\models\AccountsGrid $item */
					$result = '';
					if ($item->account->message) {
						$result .= Html::tag('span', '', [
							'class' => 'read-message glyphicon glyphicon-info-sign',
							'data'  => [
								'content' => $item->account->getErrorMessage(),
								'title'   => 'Проект остановлен',
								'toggle'  => 'popover',
							],
							'aria-hidden' => 'true'
						]);
					}

					$status = ArrayHelper::getValue($account_statuses, intval($item->account->instagram_id), false);
					$result .= Html::a(
						Html::tag('span', '', [
							'class' => 'glyphicon glyphicon-'.($status ? 'play' : 'stop'),
							'aria-hidden' => 'true',
							'style' => 'color: '.($status ? '#009900' : '#990000')
						]),
						['/admin/projects/'.($status ? 'stop' : 'start').'/'.$item->account->id]
					);
					return $result;
				}
			],

			[
				'label'  => 'Время запуска',
				'format' => 'raw',
				'value'  => function($item) {
					/** @var \app\modules\admin\models\AccountsGrid $item */
					return Html::a(
						date('d/m/Y H:i:s', $item->create_date),
						'http://'.($item->account->server ? $item->account->server : Yii::$app->params['serverIP'])
						.'/userLog.php?id='.$item->account->instagram_id
					);
				}
			],

//			[
//				'label' => 'Пользователь',
//				'value' => 'user.mail'
//			],

			[
				'label'  => 'Instagram',
				'format' => 'raw',
				'headerOptions' => [
					'colSpan' => 2
				],
				'value'  => function ($item) {
					$password = $item->account->password;
					if (strlen($password) > Account::MAX_PASSWORD_LENGTH) {
						$password = call_user_func(MCRYPT_DECODE, $password);
					}
					/** @var \app\modules\admin\models\AccountsGrid $item */
					return
						Html::a('@'.$item->account->login, 'https://instagram.com/'.$item->account->login, [
							'rel'    => 'noreferrer',
							'target' => '_new'
						]).
						Html::endTag('td').
						Html::beginTag('td').
						Html::a($password, ['/options/index', 'id' => $item->account->id]);
				}//'account.login'
			],

			[
				'label'  => 'E-Mail',
				'format' => 'raw',
				'headerOptions' => [
					'colSpan' => 2
				],
				'value'  => function ($item) {
					return $item->email . '</td><td>' . $item->email_pwd;
				}
			],

			'donor_url:url',

			'sign:text',
			'sign_url:url',

			'mark_number',

			[
				'label'  => 'Город',
				'format' => 'raw',
				'value'  => function ($item) {
					/** @var \app\modules\admin\models\AccountsGrid $item */
					$result = [];
					foreach ($item->account->places as $place) {
						$result[] = $place->name;
					}
					return implode(', ', $result);
				}
			],

			'description:text',

			[
				'format' => 'raw',
				'value'  => function ($item) {
					return Html::a(
						Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']),
						['/admin/accounts-grid/edit', 'id' => $item->id]
					);
				}
			],
		],
	]); ?>
</div>

<script>
jQuery(function($){
	$('.read-message').popover();
});
</script>