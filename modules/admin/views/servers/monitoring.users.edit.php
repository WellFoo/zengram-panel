<?php
/**
 * @var $this \yii\web\View
 * @var $user \app\models\SnifferGroups
 */

use app\models\SnifferMonitoringParameters;
use app\models\SnifferServers;
use yii\bootstrap\Html;

$this->title = 'Редактирование пользователя';

echo '<h2>' . $this->title . '</h2>';

echo $this->render('monitoring.user.form.php', [
	'user' => $user
]);

$query = SnifferMonitoringParameters::find()
	->where([
		'user_id' => $user->id
	])
	//->all()
;

$dataProvider = new \yii\data\ActiveDataProvider([
	'query' => $query,
	'pagination' => [
		'pageSize' => 1000,
	],
]);

echo '<h3>Отслеживание</h3>';

echo Html::a(
	'Добавить отслеживание',
	['/admin/servers/add-monitoring/' . $user->id],
	[
		'class' => 'btn btn-success',
	]);

echo '<br><br>';

echo \yii\grid\GridView::widget([
	'dataProvider' => $dataProvider,
	'layout' => '
		{items}'
	,
	'columns'      => [
		[
			'attribute' => 'server_id',
			'value' => function($monitoring){
				/** @var $monitoring SnifferMonitoringParameters */

				$server = $monitoring->server;

				return $server->group->name . ' - ' . $server->name . ' (' . long2ip($server->ip) . ')';
			}
		],
		[
			'attribute' => 'parameter',
			'value' => function($monitoring){
				/** @var $monitoring SnifferMonitoringParameters */
				return $monitoring->getParameterDescription();
			}
		],
		[
			'attribute' => 'operator',
			'value' => function($monitoring){
				/** @var $monitoring SnifferMonitoringParameters */
				return $monitoring->getOperatorDescription();
			}
		],
		'value',
		[
			'attribute' => 'notification',
			'value' => function($monitoring){
				/** @var $monitoring SnifferMonitoringParameters */

				$notifications = SnifferMonitoringParameters::getNotificationsList();

				$result = '';

				foreach ($notifications as $notification => $description) {
					if (!empty($monitoring->$notification)) {
						$result .= ($result == '' ? '' : ', ') . $description;
					}
				}

				return $result;
			}
		],
		[
			'label' => '',
			'format' => 'raw',
			'value' => function($monitoring){
				/** @var $monitoring SnifferMonitoringParameters */
				return Html::a(
					'<span class="glyphicon glyphicon-pencil"></span>',
					['/admin/servers/edit-monitoring/' . $monitoring->id]
				) . '&emsp;' . Html::a(
					'<span class="glyphicon glyphicon-remove"></span>',
					['/admin/servers/delete-monitoring/' . $monitoring->id],
					['class' => 'action-delete']
				);
			}
		],
	],
]);
