<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $account string */
/* @var $email string */

$this->title = Yii::t('app', 'Стартовые логи проектов');
?>
<div class="site-admin">
	<h1><?= Html::encode($this->title) ?></h1>

	<br>

	<form method="get">


		<div class="form-inline clearfix">

			E-mail&nbsp;
			<div class="input-group">
				<input type="text" class="form-control" name="email" value="<?=$email?>">
			</div>
			Логин
			<div class="input-group">
				<input type="text" class="form-control" name="account" value="<?=$account?>">
			</div>

			<button type="submit" class="btn btn-default">Вперед</button>

		</div>

	</form>

	<br><br>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			[
				'attribute' => 'Аккаунт',
				'format' => 'html',
				'value' => function($data) {
					$projectData = json_decode($data->data, true);
					if (!empty($projectData['user']) && !empty($projectData['user']['username'])) {
						return '@'. $projectData['user']['username'];
					}
					return '';
				},
				'contentOptions' => ['class' => 'come-class']
			],
			'clientmail',
			'datetime',
			[
				'attribute' => 'Подпись',
				'format' => 'html',
				'value' => function($data) {
					$projectData = json_decode($data->data, true);
					if (!empty($projectData['user']) && !empty($projectData['user']['biography'])) {
						return $projectData['user']['biography'];
					}
					return '';
				},
				'contentOptions' => ['class' => 'come-class']
			],
			[
				'attribute' => 'Медиа',
				'format' => 'html',
				'value' => function($data) {
					$projectData = json_decode($data->data, true);
					if (!empty($projectData['user']) && !empty($projectData['user']['media_count'])) {
						return $projectData['user']['media_count'];
					}
					else return '';
				},
				'contentOptions' => ['class' => 'come-class text-center']
			],
			[
				'attribute' => 'Подписки',
				'format' => 'html',
				'value' => function($data) {
					$projectData = json_decode($data->data, true);
					if (!empty($projectData['user']) && !empty($projectData['user']['following_count'])) {
						return $projectData['user']['following_count'];
					}
					else return '';
				},
				'contentOptions' => ['class' => 'come-class text-center']
			],
			[
				'attribute' => 'Подписчики',
				'format' => 'html',
				'value' => function($data) {
					$projectData = json_decode($data->data, true);
					if (!empty($projectData['user']) && !empty($projectData['user']['follower_count'])) {
						return $projectData['user']['follower_count'];
					}
					else return '';
				},
				'contentOptions' => ['class' => 'come-class text-center']
			],
		],
	]); ?>
</div>