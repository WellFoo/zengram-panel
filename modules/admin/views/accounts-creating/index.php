<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Options;
use yii\grid\GridView;

?>

<a href="/admin/accounts-creating/campaign-create/"><button type="button" class="btn btn-primary">Создать новую кампанию</button></a>
<a href="/admin/accounts-creating/donors/"><button type="button" class="btn btn-primary">Доноры</button></a>

<?= GridView::widget([
'dataProvider' => $dataProvider,
]);
?>