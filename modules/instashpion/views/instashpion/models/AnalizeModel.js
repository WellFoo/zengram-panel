var AnalizeModel = Backbone.Model.extend({
    UPDATING_INTERVAL: 3 * 1000,

    _interval: null,

    defaults: {
        process:        false,
        id:             null,
        complete:       false,
        reanalize:      null,
        result:         new UsersCollection(),
        new_follows:    new UsersCollection(),
        isReanalize:    false,
        progress:       {
            follows:    0,
            likes:      0,
            relikes:    0
        },
        updated: false,
        pending: false,
        updatePending: false,
        showBalanceModal: false,
        showPayModal: false,
        tick: false,
        toggleHistry: false
    },

    flush: function() {
        var id = this.get('id');

        this.set(this.defaults);

        app.set({'isBrowser': false, id: id});

        this.get('result').reset();
        this.get('new_follows').reset();

        setTimeout(function () {
            if (isGuest) {
                localStorage.setItem('store', JSON.stringify(app));
            }
        }, 300);

    },

    isReanalize: function () {
        return this.get('isReanalize');
    },

    getAnalizeUrl: function() {
        if (this.get('isReanalize') === true) {
            return '/instashpion/ajax/reanalize';
        }

        return '/instashpion/ajax/analize';
    },

    getStatusUpdateUrl: function() {
        return '/instashpion/ajax/check-analize-progress';
    },

    start: function()
    {
        var user    = app.get('user');
        var handler = app.get('handlerUser');
        var self    = this;

        if (this.get('process') === true || user === null || app.get('user').get('loadedInfo')) {
            return false;
        }

        app.set('isBrowser', false);

        if (app.get('balance') < 1 && !this.isReanalize()) {
            this.set('showBalanceModal', true);
            return null;
        }

        if (this.get('id') === null) {
            this.set('id', 0);
        }

        this.set({
            process: true,
            complete: false,
            progress: this.defaults.progress
        });

        app.set({tick: !app.get('tick')});

        if (this.get('id') !== null && parseInt(this.get('id')) > 0) {
            this.set('isReanalize', true);
        }

        var request = {
            instagram_id: user.get('instagram_id')
        };

        if (this.isReanalize()) {
            request.analize_id = this.get('id');
        }

        if (handler) {
            request.handlerUser = {
                login:      handler.get('login'),
                password:   handler.get('password')
            }
        }

        this.set({
            'pending': true
        });

        if (this.update_request) {
            this.update_request.abort();
        }

        if (this.analize_request) {
            this.analize_request.abort();
        }

        this.analize_request = $.ajax({
            type: 'post',
            url: this.getAnalizeUrl(),
            data: request,
            success: function (response) {
                self.set({
                    pending: false
                });

                if (response.status == 'ok') {
                    if (!self.isReanalize()) {
                        store.set('balance', parseInt(store.get('balance') - 1));
                    }

                    self.set({
                        'id': response.analize_id
                    });
                    self.startUpdater();
                    self.set({
                        pending:  false,
                        updated: false
                    });

                    if (!self.isReanalize()) {
                        var existed = false;

                        app.get('history').each(function (item) {
                            if (item.get('user').get('instagram_id') == app.get('user').get('instagram_id')) {
                                existed = true;
                            }
                        });

                        if (!existed) {
                            app.get('history').add(
                                new HistoryItem({
                                    user: app.get('user').clone(),
                                    analize: app.get('analize').clone(),
                                    hidden: false
                                })
                            );

                            if (app.get('history').size() >= 3) {
                            //    app.set('history', new HistoryCollection(app.get('history')));
                            }
                        }
                    }

                    setTimeout(function () {
                        if (isGuest) {
                            localStorage.setItem('store', JSON.stringify(app));

                            console.log('Обновляем куки', app.get('history').map(function (item) { return item.get('user').get('id'); }));
                            Cookies.set('history', app.get('history').map(function (item) { return item.get('analize').get('id'); }));
                        }
                    }, 300);

                } else {
                    console.error(response.message);
                }

                self.set('pending', false);
            },
            jsonp: true
        });
    },

    isComplete: function ()
    {
        // если статус известен возвращаем его
        if (this.get('complete') === true) {
            return true;
        }

        var progress = this.get('progress');

        // Проверяем статус по показателям прогресса
        return !!(progress.follows >= 100 && progress.likes >= 100 && progress.relikes >= 100);
    },

    updateHandler: function () {
        var self        = this;
        var progress    = this.get('progress');



        if (this.get('updatePending') === true) {
            return true;
        }

        if (this.get('id') < 1) {
            return null;
        }

        var request     = {
            analize_id: this.get('id')
        };

        this.set({
            'updatePending': true
        });

        if (this.update_request) {
            this.update_request.abort();
        }

        this.update_request = $.ajax({
            type: 'post',
            url: '/instashpion/ajax/check-analize-progress',
            data: request,
            success: function (response) {
                self.set({
                    updatePending:  false
                });

                app.set({tick: !app.get('tick')});

                self.set({toggleHistory: false});

                if (response.status == 'ok') {
                    var result = self.get('result');

                    if (self.isReanalize()) {


                        if (_.size(response.result) > 0) {
                            _.forEach(response.result, function (user) {
                                var model = result.findWhere({id: user.id});

                                if (model) {
                                    // Обновляем пользователя если он есть в коллекции
                                    model.set(user);
                                } else {
                                    // Или добовляем нового
                                    result.add(user);
                                }
                            });
                        }

                        if (_.size(response.new_follows) > 0) {
                            self.get('new_follows').reset(response.new_follows);
                        }
                    } else {
                        result.reset(_.toArray(response.result));
                    }

                    self.set({
                        progress: response.progress,
                        updated: true
                    });

                    if (response.progress.likes < 100 || response.progress.follows < 100 || response.progress.relikes < 100) {
                        if (self.get('complete') === true) {
                            self.set('complete', false);
                        }

                        if (self.get('process') === false) {
                            self.set('process', true);
                        }
                    }

                    if (self.isComplete()) {
                        self.set({
                            complete:   true,
                            process:    false
                        });

                        self.stopUpdater();
                    }
                } else {
                    console.error(response.message);
                }
            },
            jsonp: true
        });
    },

    stop: function() {
        var request = {
            id: this.get('id')
        };

        var self = this;

        if (this.update_request) {
            this.update_request.abort();
        }

        if (this.analize_request) {
            this.analize_request.abort();
        }

        $.post('/instashpion/ajax/stop', request, function (response) {
            if (response.status == 'ok') {
                console.log('Успешно остановлено');
                self.stopUpdater();
                self.flush();

                self.set({
                    process: false,
                    pending:  false,
                    updated: false,
                    isReanalize: true
                });

                setTimeout(function () {
                    if (isGuest) {
                        localStorage.setItem('store', JSON.stringify(app));
                    }
                }, 200);

            }
        });

        this.stopUpdater();
        this.flush();
    },

    startUpdater: function ()
    {
        var self = this;
        this.stopUpdater();

        this._interval = setInterval(function () { self.updateHandler() }, this.UPDATING_INTERVAL);
    },

    stopUpdater: function () {

        if (this.update_request) {
            this.update_request.abort();
        }

        if (this.analize_request) {
            this.analize_request.abort();
        }

        clearInterval(this._interval);
    }
});

var HistoryItem = Backbone.Model.extend({
    restore: function () {
        this.set('hidden', false);

        app.set({tick: !app.get('tick')});
        app.set({balance: (app.get('balance') - 1)});

        var request = {
            instagram_id: this.get('user').get('instagram_id')
        };

        this.restore = $.ajax({
            type: 'get',
            url: '/instashpion/ajax/restore-victim',
            data: request,

            success: function (response) {
                if (response.status == 'ok') {
                    console.log('восстановлено');
                } else {
                    console.error(response.message);
                }
            },
            jsonp: true
        });
    },

    open: function() {
        if (app.get('analize').get('process')) {
        //    return true;
        }

        app.get('analize').stopUpdater();
        app.get('analize').stop();

        var user = this.get('user').clone();
        var analize = this.get('analize').clone();

        analize.set({
            process: false,
            updatePending: false,
            updated: false,
            isReanalize: true,
            complete: true,
            toggleHistory: true
        });

        analize.get('result').reset();

        app.set({
            'user': user,
            'analize': analize
        });
        app.set({tick: !app.get('tick')});

        app.get('analize').startUpdater();

    }
});

var HistoryCollection = Backbone.Collection.extend({
    model: HistoryItem
});