var Spin = React.createClass({
    getDefaultStyle: {
        display: 'inline-block'
    },

    getContainerStyle() {
        var style = this.getDefaultStyle;

        if (this.props.size) {
            style.width = this.props.size;
            style.height = this.props.size;
        }

        return style;
    },

    getWheelStyle() {
        var style = this.getDefaultStyle;

        if (this.props.size) {
            style.width = this.props.size;
            style.height = this.props.size;
        }

        if (this.props.color) {
            style.borderTopColor = this.props.color;
            style.borderBottomColor = this.props.color;
        }

        return style;
    },

    render() {
        if (this.props.loading) {
            return (
                <div className="spin-container" style = {this.getContainerStyle()}>
                    <div className="spin-wheel" style = {this.getWheelStyle()}></div>
                </div>
            );
        } else {
            return <span>{(this.props.label || 'Отправить')}</span>;
        }
    }
});
