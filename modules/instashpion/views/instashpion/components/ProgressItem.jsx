var ProgressItem = React.createClass({
    getInitialState() {
        return {
            complete: false
        }
    },

    renderIcon() {
        if (this.state.complete || (this.props.loop && this.props.completed)) {
            return (<i className="glyphicon glyphicon-ok"></i>);
        }

        return (<Spin label = "Анализировать" loading = {true} size = "20px" color = "#999" />);
    },

    completeHandler() {
        this.setState({
            complete: true
        });
    },

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.progress > 99) {
            this.completeHandler();
        }

        return true;
    },

    render() {
        var self = this;
        return (
            <div className = "progress-item">
                <div className="row">
                    <div className="col-md-9 col-xs-8 progress-label" style = {{color: ((this.state.complete || (this.props.loop === true && this.props.completed === true)) ? 'rgb(58, 148, 58)' : '#666')}}>
                        {this.renderIcon()}
                        <span style = {{marginLeft: '15px'}}>{this.props.label}</span>
                    </div>
                    <div className="col-md-3 col-xs-4 percents-progress">
                        {(() => {
                            if (this.props.start && !this.props.loop) {
                                return (
                                    <span>
                                        <DinamicNumber sync = {self.props.progress} right = {self.props.right} fps = {10} onComplete = {this.completeHandler} />%
                                    </span>
                                );
                            } else {
                                if (this.props.loop) { return null; }

                                return <span>{self.props.progress}%</span>;
                            }
                        })()}

                    </div>
                </div>
            </div>
        );
    }
});