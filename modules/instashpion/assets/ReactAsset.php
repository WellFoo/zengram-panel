<?php  namespace app\modules\instashpion\assets;

use yii\web\AssetBundle;
use yii\web\View;

class ReactAsset extends AssetBundle
{
	public $sourcePath = '@bower';
	public $js = [
		YII_ENV_DEV ? 'react/react.js' : 'react/react.min.js',
		YII_ENV_DEV ? 'react/react-dom.js' : 'react/react-dom.min.js',
		YII_ENV_DEV ? 'babel/browser.js' : 'babel/browser.min.js',
	];
	public $jsOptions = [
		'position' => View::POS_HEAD,
	];
}