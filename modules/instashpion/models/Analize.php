<?php
namespace app\modules\instashpion\models;

use app\models\Users;
use Yii;
use yii\db\ActiveRecord;
/**
 * Модель состояния анализа
 *
 * @property integer $id
 *
 * @property integer $victim_id
 * @property integer $user_id
 * @property integer $analize_list_status
 * @property integer $analize_likes_status
 * @property string $list_id
 * @property string $task_id
 */
class Analize extends ActiveRecord
{
	const STATUS_LIST_STOP = 0;
	const STATUS_LIST_WORK = 1;
	const STATUS_LIST_COMPLETE = 2;

	/**
	* @inheritdoc
	*/
	public static function tableName()
	{
		return '{{%analize}}';
	}

	/**
	* @inheritdoc
	*/
	public function rules()
	{
		return [
			[['victim_id', 'user_id', 'analize_list_status', 'analize_likes_status'], 'integer'],
			[['list_id', 'task_id'], 'string'],
			[['updated_at'], 'safe']
		];
	}

	public function getVictim()
	{
		return self::hasOne(Victim::className(), ['id' => 'victim_id']);
	}

	public function getUser()
	{
		return self::hasOne(Users::className(), ['id' => 'user_id']);
	}
}
