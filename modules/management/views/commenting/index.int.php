<?php
/**
 * @var \yii\web\View $this
 * @var string $date_from
 * @var string $date_to
 * @var integer $user
 */
use app\modules\admin\models\PopularAccountsCommentsLog;

$query = PopularAccountsCommentsLog::find()
	->where([
		'code' => [PopularAccountsCommentsLog::CODE_COMMENT_SUCCESS, PopularAccountsCommentsLog::CODE_COMMENT_FAILED]
	])
	->andWhere(['>=', 'date', $date_from])
	->andWhere(['<', 'date', $date_to])
	->orderBy('date ASC');

if (isset($user) && $user != 0) {
	$query->andWhere([
		'project_id' => $user
	]);
}

$comment_data = [];
$last_instagram_id = 0;
$last_project_id = 0;

/* @var $log PopularAccountsCommentsLog */
foreach ($query->each() as $log) {

	$date = date('Y-m-d', $log->date);

	if ($last_instagram_id === $log->instagram_id && $last_project_id === $log->project_id && $log->code == PopularAccountsCommentsLog::CODE_COMMENT_FAILED) {
		continue;
	} else {
		$last_instagram_id = $log->instagram_id;
		$last_project_id = $log->project_id;
	}

	if (!isset($comment_data[$date])) {
		$comment_data[$date] = [
			'day' => $date,
			'commented' => $log->code == PopularAccountsCommentsLog::CODE_COMMENT_SUCCESS ? 1 : 0,
			'uncommented' => $log->code == PopularAccountsCommentsLog::CODE_COMMENT_FAILED ? 1 : 0
		];
	} else {
		if ($log->code == PopularAccountsCommentsLog::CODE_COMMENT_SUCCESS) {
			$comment_data[$date]['commented']++;
		} elseif ($log->code == PopularAccountsCommentsLog::CODE_COMMENT_FAILED) {
			$comment_data[$date]['uncommented']++;
		}
	}

}

$data = new \yii\data\ArrayDataProvider([
	'allModels' => $comment_data
]);

?>

<?= \yii\grid\GridView::widget([
	'dataProvider' => $data,
	'columns' => [
		[
			'label' => 'День',
			'attribute' => 'day',
		],
		[
			'label' => 'Прокомментировано',
			'attribute' => 'commented',
		],
		[
			'label' => 'Не прокомментировано',
			'attribute' => 'uncommented',
		],
	]
]) ?>

<script>
	$('.user-filter').change(function(){
		$(this).closest('form').submit();
		return false;
	});
</script>