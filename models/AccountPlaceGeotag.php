<?php namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "account_place_geotags".
 *
 * @property integer $account_id
 * @property integer $place_id
 * @property integer $fb_geotag_id
 * @property boolean $enabled
 *
 * @property Account  $account
 * @property Place    $place
 * @property FbGeotag $tag
 */
class AccountPlaceGeotag extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'account_place_geotags';
	}

	/**
	 * @inheritdoc
	 */
	public static function primaryKey()
	{
		return ['account_id', 'place_id', 'fb_geotag_id'];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['account_id', 'place_id', 'fb_geotag_id'], 'integer'],
			[['enabled'], 'boolean'],
			[
				['account_id', 'place_id', 'fb_geotag_id'],
				'unique',
				'targetAttribute' => ['account_id', 'place_id', 'fb_geotag_id'],
				'message' => 'The combination of Account ID, Place ID and Fb Geotag ID has already been taken.'
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'account_id'   => Yii::t('app', 'Account ID'),
			'place_id'     => Yii::t('app', 'Place ID'),
			'fb_geotag_id' => Yii::t('app', 'Fb Geotag ID'),
			'enabled'      => Yii::t('app', 'Enabled'),
		];
	}

	public function getAccount()
	{
		return $this->hasOne(Account::className(), ['id' => 'account_id']);
	}

	public function getPlace()
	{
		return $this->hasOne(Place::className(), ['id' => 'place_id']);
	}

	public function getTag()
	{
		return $this->hasOne(FbGeotag::className(), ['id' => 'fb_geotag_id']);
	}

	/**
	 * @param $model AccountPlaceGeotag
	 * @return array
	 */
	public static function toOptionsArray($model)
	{
		return [
			'id'         => $model->fb_geotag_id,
			'title'      => $model->tag->name,
			'address'    => $model->tag->address,
			'coords'     => $model->tag->getCoordsArray(),
			'enabled'    => $model->enabled,
		];
	}
}
