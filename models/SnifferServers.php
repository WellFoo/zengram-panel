<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sniffer_servers".
 *
 * @property SnifferGroups $group
 * @property SnifferUsers $user
 * @property integer $id
 * @property integer $user_id
 * @property string  $name
 * @property integer $status
 * @property integer $type
 * @property integer $time
 * @property integer $ip
 * @property integer $last_check
 * @property integer $parent
 * @property integer $ping
 * @property string  $disk_drive
 * @property float   $memory
 * @property float   $processor
 * @property string  $parameters
 * @property string  $configuration
 * @property string  $sniffer
 * @property string  $message
 * @property string  $description
 * @property boolean $secure
 */
class SnifferServers extends ActiveRecord
{
	const STATUS_OFF        = 0;
	const STATUS_ON         = 1;
	const STATUS_UNEXPECTED = 2;

	const SERVER_TYPE_WEB    = 1;
	const SERVER_TYPE_WORKER = 2;

	const SNIFFER_KEY = '2p8923pn2p3p23v9822';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'sniffer_servers';
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Наименование',
		];
	}

	public function log()
	{
		SnifferLog::log([
			'server_id'  => $this->id,
			'ping'       => $this->ping,
			'memory'     => $this->memory,
			'disk_drive' => $this->disk_drive,
			'processor'  => $this->processor,
		]);
	}

	public function isOnline()
	{
		return $this->status === self::STATUS_ON;
	}

	public function getGroup()
	{
		return SnifferGroups::findOne($this->parent);
	}

	public function getUser()
	{
		return SnifferUsers::findOne($this->user_id);
	}

	public function sniff()
	{
		$data = [
			'action' => 'sniff',
			'config' => $this->configuration,
		];

		return $this->request($data);
	}

	public function storeData($data)
	{
		$this->memory = round(100 - $data['mem'], 2);
		$this->disk_drive = round($data['disk'], 2);
		$this->processor = round(100 - $data['cpu'], 2);

		$config = $this->getConfiguration();

		$parameters = [];

		if (!empty($config['add'])) {
			foreach ($config['add'] as $command => $info) {
				$parameters[$command] = empty($data[$command]) ?
					false : $this->parseRequest($command, $data[$command]);
			}
		}

		$this->parameters = json_encode($parameters);
	}

	public function parseRequest($command, $request)
	{
		switch ($command) {
			/*case 'rabbit':
				$result = 'заипцом';
				break;*/
			default:
				return $request;
				break;
		}
	}

	public function getConfiguration()
	{
		return empty($this->configuration) ? [] : json_decode($this->configuration, true);
	}

	public function getParameters()
	{
		return empty($this->parameters) ? [] : json_decode($this->parameters, true);
	}

	public function upload($data)
	{
		$data['action'] = 'update';
		return $this->request($data);
	}

	public function getParams()
	{
		return self::getCommonParams();
		// TODO: доп. параметры из конфига.
	}

	public function getLastParameter($parameter)
	{
		$last_log = SnifferLog::find()
			->where([
				'server_id' => $this->id
			])
			->orderBy(['id' => SORT_DESC])
			->one();

		if (is_null($last_log) || empty($last_log->$parameter)) {
			return null;
		}

		return $last_log->$parameter;
	}

	public function getValue($name)
	{
		if (!empty($this->$name)) {
			return $this->$name;
		}

		return false;
	}

	public static function getCommonParams()
	{
		return [
			'processor',
			'disk_drive',
			'memory',
			'ping',
		];
	}

	public function request($data)
	{
		$url = $this->getSniffUrl();

		$curl = curl_init();

		$data['key'] = self::SNIFFER_KEY;

		//var_dump($data);

		$options = [
			CURLOPT_URL              => $url,
			CURLOPT_POST             => true,
			CURLOPT_HEADER           => 0,
			CURLOPT_RETURNTRANSFER   => true,
			CURLOPT_POSTFIELDS       => $data,
			CURLOPT_TIMEOUT          => 60,
			//CURLOPT_TIMEOUT_MS       => 60 * 1000,
		];

		if ($this->secure) {
			$options[CURLOPT_SSL_VERIFYPEER] = 0;
			$options[CURLOPT_SSL_VERIFYHOST] = 0;
		}

		curl_setopt_array($curl, $options);

		try {
			curl_setopt($curl, CURLOPT_SAFE_UPLOAD, false);
		} catch (\Exception $e) {}

		$result = curl_exec($curl);

		//var_dump($result);

		preg_match("#{\".*}#", $result, $matches);
		if ($matches) {
			$result = $matches[0];
		}

		//var_dump($result);

		$result = json_decode($result, true);

		//var_dump($result);

		if (is_null($result)) {
			$this->status(SnifferServers::STATUS_UNEXPECTED);
			$this->message = 'Не удалось получить данные с сервера';
			$this->save();

			return false;
		}

		if (empty($result['status']) || $result['status'] !== 'ok') {
			$this->status(self::STATUS_UNEXPECTED);
			if (!empty($result['data']) && is_string($result['data'])) {
				$this->message = $result['data'];
			}
			$this->save();
			return false;
		}

		return $result['data'];
	}

	public function getUrl()
	{
		return ($this->secure ? 'https://' : 'http://') . long2ip($this->ip) . '/';
	}

	public function getSniffUrl()
	{
		$server = $this->getUrl();

		if (empty($this->sniffer)) {
			return $server . 'monitoring-sniffer.php';
		}

		return $server . $this->sniffer;
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {

			$this->last_check = time();

			if ($this->time == 0 && $this->status !== self::STATUS_ON && !empty($this->message)) {

				$user = $this->user;

				if (is_null($user)) {
					return true;
				}

				$user->allNotify([
					$this->message
				], $this);
			}
			return true;
		}
		return false;
	}

	public function status($status)
	{
		if ($this->status !== $status) {
			$this->time = 0;
		} else {
			$this->time += time() - $this->getOldAttribute('last_check');
		}

		$this->status = $status;
	}

	public function getDescriptuionData()
	{
		return json_decode($this->description, true);
	}

	public function getDescriptions()
	{
		$data = $this->getDescriptuionData();

		if (is_null($data)) return [];

		return [
			'Хостер: ' . $data['hoster'],
			'Ядер: '   . $data['core'],
			'Память: ' . $data['memory'] . ' GB',
			'Диск: '   . $data['disk'] . ' GB',
		];
	}

	public function getDescription($parameter)
	{
		$data = $this->getDescriptuionData();

		if (is_null($data) || empty($data[$parameter])) return '';

		return $data[$parameter];
	}

	public function getSpecialParameters()
	{
		$config = $this->getConfiguration();
		$result = [];
		if (!empty($config['add'])) {
			$parameters = $this->getParameters();
			foreach ($config['add'] as $command => $info) {
				$result[$command] = empty($parameters[$command]) ? false : $parameters[$command];
			}
		}
		return $result;
	}

	public function getDecodeParameters()
	{
		$parameters = $this->getSpecialParameters();
		$result = [];
		foreach ($parameters as $command => $data) {

			switch ($command) {
				case 'rabbit':
					$result[$command] = json_decode($data, true);
					break;
				default:
					$result[$command] = $data;
					break;
			}
		}
		return $result;
	}
}
