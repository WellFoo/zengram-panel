<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "options".
 *
 * @property integer $id
 * @property integer $account_id
 * @property integer $likes
 * @property integer $follow
 * @property integer $comment
 * @property integer $unfollow
 * @property integer $mutual
 * @property integer $commercial
 * @property integer $private
 * @property integer $sameComment
 * @property integer $skipCommercial
 * @property integer $speed
 * @property integer $firmcomments
 * @property integer $autounfollow
 * @property integer $prev_mask
 * @property string  $search_by
 * @property integer $filter_by_follows
 * @property integer $experimental
 * @property integer $likeMutuals
 * @property integer $likeFollowers
 * @property integer $auto_fill_freq
 * @property integer $auto_fill_mode
 * @property integer $auto_fill_follows_min_during_period
 * @property integer $auto_fill_follows_max_during_period
 * @property integer $auto_fill_likes_after_follow_min
 * @property integer $auto_fill_likes_after_follow_max
 * @property integer $auto_fill_follow_percent_clients
 * @property integer $follows_limit_to_unfollow
 * @property boolean $skip_popular_update
 * @property boolean $comment_popular
 * @property boolean $account_antispam
 * @property boolean $auto_fill
 * @property boolean $use_whitelist
 * @property boolean $parallel_unfollows
 * @property string  $auto_fill_start_date
 * @property Account $account
 * @property boolean $use_geotags
 */
class Options extends ActiveRecord
{
	const OPT_LIKES    = 0b0001;
	const OPT_COMMENT  = 0b0010;
	const OPT_FOLLOW   = 0b0100;
	const OPT_UNFOLLOW = 0b1000;

	const SEARCH_MASK       = 0b1111;
	const SEARCH_GEO_MASK   = 0b0011;
	const SEARCH_PLACE      = 0b0001;
	const SEARCH_REGION     = 0b0011;
	const SEARCH_HASHTAG    = 0b0100;
	const SEARCH_COMPETITOR = 0b1000;

	public $skipRelations = false;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'options';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['account_id'], 'required'],
			[
				[
					'account_id', 'likes', 'follow', 'comment', 'unfollow', 'autounfollow', 'mutual', 'commercial', 'private',
					'sameComment', 'type', 'skipCommercial', 'speed', 'firmcomments', 'prev_mask', 'experimental', 'search_by',
					'filter_by_follows', 'likeFollowers', 'likeMutuals', 'auto_fill_freq', 'auto_fill_mode',
					'auto_fill_follows_min_during_period', 'auto_fill_follows_max_during_period', 'use_whitelist',
					'auto_fill_likes_after_follow_min', 'auto_fill_likes_after_follow_max', 'auto_fill_follow_percent_clients',
					'follows_limit_to_unfollow', 'parallel_unfollows',
				],
				'integer',
			],
			[[
				'commercial', 'private', 'sameComment', 'type', 'skipCommercial', 'speed', 'firmcomments', 'prev_mask',
				'experimental', 'filter_by_follows', 'likeFollowers', 'likeMutuals', 'auto_fill_freq',
				'auto_fill_mode', 'auto_fill_follows_min_during_period', 'auto_fill_follows_max_during_period',
				'use_whitelist', 'auto_fill_likes_after_follow_min', 'auto_fill_likes_after_follow_max',
				'auto_fill_follow_percent_clients'
			], 'default', 'value' => 0],
			['search_by', 'default', 'value' => self::SEARCH_PLACE],
			[['auto_fill_start_date'], 'string'],
			[['skip_popular_update', 'comment_popular', 'account_antispam', 'auto_fill', 'use_geotags'], 'boolean'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'likes'          => Yii::t('app', 'To put likes'),
			'follow'         => Yii::t('app', 'To follow'),
			'comment'        => Yii::t('app', 'To comment'),
			'unfollow'       => Yii::t('app', 'To unfollow'),
			'mutual'         => Yii::t('app', 'Unfollow who don\'t follow me'),
			'autounfollow'   => Yii::t('app', 'Automatically activate unfollows, on follow limit'),
			'commercial'     => Yii::t('app', 'Skip non-commercial accounts'),
			'skipCommercial' => Yii::t('app', 'Skip commercial check'),
			'speed'          => Yii::t('app', 'Speed of work'),
			'firmcomments'   => Yii::t('app', 'Use recommended, branded Zengram comments'),
			'filter_by_follows' => Yii::t('app', 'Фильтровать отлайковших по подпискам запостившего'),
//			'private'     => Yii::t('app', 'Private'),
//			'sameComment' => Yii::t('app', 'Same Comment'),
			'skip_popular_update' => 'Пропускать обновление популярных аккаунтов',
			'comment_popular' => 'Комментировать популярные аккаунты',
			'account_antispam' => 'Антиспам для аккаунта',
			'use_whitelist' => 'Whitelist для аккаунта',
			'auto_fill' => 'Прокачка аккаунта',
			'auto_fill_freq' => 'Частота постинга фотографий',
			'auto_fill_mode' => 'Режим работы Автопрокачки',

			'parallel_unfollows' => 'Параллельная работа отписок и подписок',
			'follows_limit_to_unfollow' => 'Предел подписок, после которых начинают работать отписки',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAccount()
	{
		return $this->hasOne(Account::className(), ['id' => 'account_id']);
	}

	/**
	 * @param bool $insert
	 *
	 * @return bool
	 */
	public function beforeSave($insert)
	{
		if (!$insert) {
			$this->checkSearchBy();
		}

		if (empty($this->prev_mask)) {
			$this->prev_mask = self::OPT_LIKES | self::OPT_COMMENT | self::OPT_FOLLOW;
		}

		if ($insert || $this->skipRelations) {
			return parent::beforeSave($insert);
		}

		if ($this->account->message === Account::EMPTY_COMMENTS ||
			$this->account->message === Account::EMPTY_GEO ||
			$this->account->message === Account::EMPTY_HASHTAGS
		) {
			$this->account->message = '';
			$this->account->save();
		}
		return parent::beforeSave($insert);
	}

	public function getGeneratedArray($postField)
	{
		$itemPost = $this->getPostField($postField);
		return $itemPost ? array_map('yii\helpers\Html::encode', array_unique(
			array_filter(array_map('trim', $itemPost)))) : [];
	}

	/**
	 * @param $field
	 *
	 * @return bool|mixed|array
	 */
	private function getPostField($field)
	{
		return isset(Yii::$app->request->post()[$field]) ? Yii::$app->request->post()[$field] : false;
	}

	/**
	 * @param $account_id
	 * @param bool $check_user
	 * @return Options
	 */
	public static function findByAccountId($account_id, $check_user = true)
	{
		$join_options = [Account::tableName().'.id' => $account_id];
		if ($check_user) {
			$join_options[Account::tableName().'.user_id'] = Yii::$app->user->id;
		}
		$options = (new static(
			Options::find()->innerJoin(Account::tableName(), $join_options)
				->where(['account_id' => $account_id])
				->one()
		));
		$options->setIsNewRecord(false);
		return $options;
	}

	public function setPrevOptions()
	{
		if (!$this->unfollow) {
			return;
		}
		$this->prev_mask = 0;
		if (!empty($this->oldAttributes['likes'])) {
			$this->prev_mask |= self::OPT_LIKES;
		}
		if (!empty($this->oldAttributes['comment'])) {
			$this->prev_mask |= self::OPT_COMMENT;
		}
		if (!empty($this->oldAttributes['follow'])) {
			$this->prev_mask |= self::OPT_FOLLOW;
		}
		if (!empty($this->oldAttributes['unfollow'])) {
			$this->prev_mask |= self::OPT_UNFOLLOW;
		}
	}

	public function getSearchBy()
	{
		return intval($this->search_by) & self::SEARCH_MASK;
	}

	public function setSearchBy($val)
	{
		$this->search_by = intval($val) & self::SEARCH_MASK;
	}

	public function isSearchByGeo()
	{
		return $this->search_by & self::SEARCH_GEO_MASK;
	}

	public function isSearchByPlaces()
	{
		return ($this->search_by & self::SEARCH_GEO_MASK) === self::SEARCH_PLACE;
	}

	public function setSearchByPlaces()
	{
		$this->search_by &= self::SEARCH_MASK ^ self::SEARCH_GEO_MASK;
		$this->search_by |= self::SEARCH_PLACE;
	}

	public function isSearchByRegions()
	{
		return ($this->search_by & self::SEARCH_GEO_MASK) === self::SEARCH_REGION;
	}

	public function setSearchByRegions()
	{
		$this->search_by &= self::SEARCH_MASK ^ self::SEARCH_GEO_MASK;
		$this->search_by |= self::SEARCH_REGION;
	}

	public function isSearchByHashtags()
	{
		return $this->search_by & self::SEARCH_HASHTAG;
	}

	public function isSearchByCompetitors()
	{
		return $this->search_by & self::SEARCH_COMPETITOR;
	}

	private function checkSearchBy()
	{
		if (empty($this->search_by)) {
			$this->search_by = self::SEARCH_PLACE;
		}
	}
}
