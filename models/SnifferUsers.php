<?php

namespace app\models;

use app\components\SmsCenterApi;
use Exception;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sniffer_users".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $email
 * @property string  $phone
 * @property string  $skype
 */
class SnifferUsers extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'sniffer_users';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id'], 'integer'],
			[['name', 'email', 'phone', 'skype'], 'string'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Имя',
			'email' => 'E-mail',
			'phone' => 'Телефон',
			'skype' => 'Скайп',
		];
	}

	public static function getUsers()
	{
		$users = self::find()
			->all();

		$result = [];

		/** @var SnifferUsers $user */
		foreach ($users as $user) {
			$result[$user->id] = $user->name;
		}

		return $result;
	}

	public function allNotify($message, $server)
	{
		$this->notify([
			'email' => $message,
			'phone' => $message,
			'skype' => $message,
			'push'  => $message,
		], $server);
	}

	public function notify($data, $server)
	{
		if (!Yii::$app->params['send-monitoring-notify']) {
			return true;
		}

		foreach ($data as $notification => $message) {
			switch ($notification) {
				case 'email':
					$this->sendToEmail($message, $server);
					break;
				case 'phone':
					$this->sendSms($message, $server);
					break;
				case 'skype':

					break;
				case 'push':

					break;
				default:
					break;
			}
		}
	}

	public function sendSms($message, $server)
	{
		$sms = new SmsCenterApi(
			Yii::$app->params['sms_center_login'],
			Yii::$app->params['sms_center_password'],
			Yii::$app->params['adminEmail'],
			'utf-8',
			1
		);

		$text = long2ip($server->ip) . ' (' . $server->group->name . ' - ' . $server->name . ')';
		$text .= implode("\r\n", $message);

		$sms->send_sms($this->phone, $text);
	}
	
	public function sendToEmail($message, $server)
	{
		try {
			Yii::$app->mailer->compose()
				->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['company']])
				->setTo($this->email)
				->setSubject('Мониторинг: ' . long2ip($server->ip) . ' (' . $server->group->name . ' - ' . $server->name . ')')
				->setHtmlBody(implode('<br>', $message))
				->send();
		} catch (Exception $e) {
			var_dump($e->getMessage());
			return false;
		}
		
		return true;
	}
}
