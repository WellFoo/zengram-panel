<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shpion_balance".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $date
 * @property string $victims
 * @property integer $is_bonus
 * @property integer $is_guest
 */
class ShpionBalance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shpion_balance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'date', 'is_bonus', 'is_guest'], 'integer'],
            ['is_guest', 'default', 'value' => 0],
            [['victims'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'date' => 'Date',
            'victims' => 'Victims',
        ];
    }
}
