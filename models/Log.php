<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "log".
 * @property integer $id
 * @property integer $user_id
 * @property integer $account_id Instagram ID
 * @property string  $date
 * @property string  $message
 */
class Log extends ActiveRecord
{
	public static function tableName()
	{
		return 'log';
	}
}