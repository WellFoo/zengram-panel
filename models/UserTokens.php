<?php  namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * Class UserTokens
 *
 * @property integer $id
 * @property integer $user_id
 * @property string  $token
 * @property integer $time
 * @property Users   $user
 *
 * @package app\models
 */

class UserTokens extends ActiveRecord
{
	public static function tableName()
	{
		return 'user_tokens';
	}

	public function rules()
	{
		return [
			[['user_id', 'token', 'time'], 'required'],
			['token', 'safe', 'on' => 'search']
		];
	}

	public function getUser()
	{
		return $this->hasOne(Users::className(), ['id' => 'user_id']);
	}

	public function generate($user_id)
	{
		$this->user_id = $user_id;
		do {
			$this->token = Yii::$app->security->generateRandomString(50);
			$model = self::findOne(['token' => $this->token]);
		} while ($model !== null);
		$this->time = time();
	}

	/**
	 * @param integer $user_id
	 * @return string
	 */
	public static function getToken($user_id)
	{
		$instance = new self();
		$instance->generate($user_id);
		$instance->save();
		return $instance->token;
	}
}