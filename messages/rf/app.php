<?php return [
	'My accounts' => 'Мои аккаунты',
	'Blog' => 'Блог',
	'Contacts' => 'Контакты',
	'Support' => 'Тех.поддержка',
	'Settings' => 'Настройки',
	'Sign out' => 'Выход',
	'Sign up' => 'Регистрация',
	'Log in' => 'Вход',
	'Log out' => 'Выход',
	'Send' => 'Отправить',
	'Ok' => 'Ок',
	'Close' => 'Закрыть',
	'Done' => 'Готово',
	'Choose one' => 'Выберите',
	'Add' => 'Добавить',
	'Remove' => 'Удалить',

	'Problems of accounts authorization. Please try again later.' => 'Проблема при авторизации. Повторите попытку чуть позже.',

	'Continue' => 'Продолжить',

	'Login' => 'Логин',
	'Instagram login' => 'Логин Instagram',
	'Password' => 'Пароль',

	'Old password' => 'Старый пароль',
	'New password' => 'Новый пароль',

	'Admin panel' => 'Панель управления',
	'Manager panel' => 'Панель менеджера',
	'Settings of the account' => 'Настройки аккаунта',

	'Save' => 'Сохранить',
	'Cancel' => 'Отмена',

	'You cannot view that resource' => 'У вас нет прав для просмотра этой страницы',
	'Account not found' => 'Аккаунт не найден',
	'Enter correct E-Mail.' => 'Введите правильный E-Mail',
	'E-Mail already used.' => 'E-mail уже используется.',
	'Password incorrect' => 'Неверный пароль.',

	'Record not found' => 'Запись не найдена',

	// Models
	'It is necessary to pass verification.' => 'Необходимо пройти верификацию.',
	'Wrong username or password.' => 'Неправильный логин или пароль.',
	'The limit of followings is exceeded. It is necessary to unfollow.' => 'Превышен лимит подписок. Необходимо включить отписки.',
	'Work of unfollowings is complete' => 'Работа по отпискам завершена',
	'Work of unfollowings is complete. Follows will start automatically after {time} hours.' => 'Работа по отпискам завершена. Подписки включатся автоматически через {time} часов.',
	'Reached daily limit unfollows that was installed by instagramom since {date}. Works will continued automatically after {time} hours' => 'Достигнут суточный лимит отписок, установленный инстаграмом с {date} Отписки продолжатся автоматически через {time} часов',
	'At the moment on the action imposed limit of 24 hours by instagram. You can run other actions. This message will disappear when the lock expiring. When activated, they begin unfollows automatically after {time} hours' => 'В настоящий момент на данное действие наложено ограничение инстаграмом на 24 часа. Вы можете пока запустить другие действия. При снятии блокировки данное сообщение пропадет. При включенной функции отписок они начнутся автоматически через {time} часов',
	'At the moment on the action imposed limit of 24 hours by instagram. You can run other actions. This message will disappear when the lock expiring. When activated, they begin follows automatically after {time} hours' => 'В настоящий момент на данное действие наложено ограничение инстаграмом на 24 часа. Вы можете пока запустить другие действия. При снятии блокировки данное сообщение пропадет. При включенной функции подписок они начнутся автоматически через {time} часов',
	'The limit of followings is exceeded. Unfollow will start automatically after {time} hours.' => 'Достигнут лимит подписок. Отписки включатся автоматически через {time} часов.',
	'The limit of unfollowings is exceeded. Follow will start automatically after {time} hours.' => 'Достигнут лимит отписок. Подписки включатся автоматически через {time} часов.',
	'There are no points for work. It is necessary to adjust geography of spread.' => 'Нет точек для работы. Необходимо настроить географию распрострения.',
	'The chosen by you points contain few publications. It is necessary to choose other geopoint, or to expand the promoting area.' => 'Выбранные вами точки содержат очень мало публикаций. Просим выбрать другую геоточку, или расширить область продвижения.',
	'There are no hashtags for work. It is necessary to add hashtags.' => 'Нет хештегов для работы. Необходимо добавить хештеги.',
	'File upload failed, please try again later' => 'Ошибка отправки фото. Пожалуйста, попробуйте повторить позже',
	'Image have incorrect size. Width and height should be between 320 and 1080 pixels' => 'Изображение имеет неверный размер. Ширина и высота должны быть в диапазаоне между 320 и 1080 пикселей',
	'There are no accounts for work. It is necessary to add accounts.' => 'Нет аккаунтов для поиска пользователей. Необходимо добавить аккаунты.',
	'The chosen by you hashtags contain few publications. It is necessary to choose other hashtags.' => 'Выбранные вами хештеги содержат очень мало публикаций. Просим выбрать другие хештеги.',
	'There is no list of comments for additing. It is necessary to add comments, or to disconnect commenting.' => 'Нет списка комментариев для добавления. Необходимо добавить комментарии, или отключить комментирование.',
	'All accounts have been processed. Please add new accounts or select another type of targeting.' => 'Все аккаунты были обработаны. Пожалуйста, добавьте новые аккаунты или выберите другой тип таргетинга',
	'In these cities there is no media. Please change the target or add new geo tags.' => 'По указанным городам нет медиа. Просим изменить таргетинг или добавить новые гео теги.',


	'Free works for account {login} already done' => 'Бесплатные работы у аккаунта {login} уже использовались',
	'You need to pass verification' => 'Необходимо пройти верификацию',
	'Incorrect login or password' => 'Ошибка в имени пользователя или пароле',
	'Unable to login to Instagram <label>{login}</label>. Please, try again later.' => 'Неудалось войти в инстаграм <label>{login}</label>. Пожалуйста, повторите попытку позже.',
	'Unable to login to Instagram. Please, try again later.' => 'Неудалось войти в инстаграм. Пожалуйста, повторите попытку позже.',
	'Unable to make account <label>{login}</label> public.' => 'Неудалось сделать аккаунт <label>{login}</label> публичным.',
	'Account with same Instagram ID already exists' => 'Аккаунт с таким Instagram ID уже был добавлен ранее',
	'{n, plural, =0{ # hours} one{ # hour} few{ # hours} many{ # hours} other{ # hours}}' => '{n, plural, =0{ # часов} one{ # час} few{ # часа} many{ # часов} other{ # часов}}',
	'{n, plural, =0{ # days} one{ # day} few{ # days} many{ # days} other{ # days}}' => '{n, plural, =0{ # дней} one{ # день} few{ # дня} many{ # дней} other{ # дней}}',
	'{days}d. {hours}h.' => '{days}д. {hours}ч.',

	'I am promoting my account in Instagram using zengram.net. I was glad for 3 free days, which is presenting to everyone' => 'Раскручиваю свой аккаунт с помощью @zengram_russia. Порадовали 3 бесплатных дня, которые дарят всем!',
	'Next submition will be avaible on {next_date}' => 'Следующее размещение будет доступно {next_date}',
	'One or more of your accounts has removed post with recommendation of zengram. Bonus balance of {cost} was subbed. We are recommending to <a href="{url}">make another submition</a> for balance renewal' => 'На одном или нескольких ваших аккаунтов был удален пост с рекомендацией zengram. Бонусный баланс в размере {cost} списан. Рекомендуем <a href="{url}">повторить размещение</a> для возобновления баланса',
	'Action description' => 'Описание акции',
	'Thanks for your recommendation! You have {cost} free' => 'Спасибо за рекомендацию! Вам начислено бесплатно {cost}',

	'Unable to check account, please try again later' => 'Не удалось проверить пользователя, повторите попытку позже',
	'Account @{login} not found' => 'Пользователь @{login} не найден',
	'Account @{login} is private' => 'Пользователь @{login} - приватный',
	'Account @{login} already exists' => 'Пользователь @{login} уже добавлен',

	'Can&#39;t add account' => 'Не добавляется аккаунт',
	'Can&#39;t start account' => 'Не запускается аккаунт',
	'Problems with counter actions' => 'Проблемы со счётчиком действий',
	'Problems with payments' => 'Проблема при оплате',
	'Problems with password change' => 'Проблемы при смене пароля',
	'Problems with making comments' => 'Проблемы при комментировании',
	'Problems with balance cancellation' => 'Проблемы со списанием баланса',
	'Problem with setup project' => 'Проблема с настройками проекта',
	'Other' => 'Иное',

	'Your account <span id="login"></span> have beed started.' => 'Мы запустили проект <span id="login"></span>',
	'Works automatically started in <span id="city"></span>' => 'Работа началась по городу - <span id="city"></span>',
	'Follows and likes are on by default.' => 'По умолчанию используются - подписка и лайки',
	'You make changes in project by clicking "Settings" button.' => 'Чтобы внести изменения в проект, воспользуйтесь кнопкой “Настройки”',

	'Enter correct e-mail.' => 'Введите действительный e-mail.',
	'Incorrect username or password.' => 'Неправильные логин или пароль',

	'To put likes' => 'Ставить лайки',
	'To follow' => 'Подписываться',
	'To comment' => 'Комментировать',
	'To unfollow' => 'Отписываться',
	'Unfollow who don\'t follow me' => 'отписаться только от тех, кто на вас не подписан',
	'Automatically activate unfollows, on follow limit' => 'автоматически включать отписки при достижении лимита, установленного Инстаграм в 7500 подписчиков',
	'Speed of work' => 'Скорость работы',
	'Use recommended, branded Zengram comments' => 'Использовать рекомендованные, фирменные комментарии Zengram',

	'Summ ID' => 'ID суммы',
	'Quantity of days' => 'количество дней',
	'Price' => 'стоимость',
	'Your bonus' => 'ваш бонус',

	'Entered e-mail already in use.' => 'Введенный адрес уже используется.',
	'User already exists' => 'Пользователь уже существует',

	'Answer' => 'Ответ',
	'On top' => 'На главной',
	'Rating' => 'Рейтинг',
	'Cat ID' => 'Раздел',
	'Question' => 'Вопрос',
	'Catalog name' => 'Название раздела',
	'Short answer' => 'Короткий ответ',

	'Parent ID' => 'Родительская категория',
	'Account ID' => 'ID аккаунта',
	'Action' => 'Действие',

	// Controllers
	'User not found' => 'Пользователь не найден',
	'Already used' => 'Уже использовано',
	'There is no accounts for upload photo' => 'Нет аккаунтов для добавления фото',
	'Upload photo error. Please try again later.' => 'Ошибка во время загрузки фото. Пожалуйста, попробуйте повторить попытку позже.',
	'Photo were uploaded earlier.' => 'Фото уже было добавлено ранее.',

	'Account id #{id} not found' => 'Аккаунт с ID #{id} не существует',
	'Media #{media_id} not found' => 'Медиа c ID #{media_id} не найдено',
	'Not enough balance' => 'Ошибка. Недостаточно средств на счету',
	'Instagram error.' => 'Ошибка instagram.',

	'Error request params' => 'Ошибка параметров запроса',
	'You do not have permission to watch this' => 'У вас нет прав для редактирования этого аккаунта.',
	'Unable to save comment' => 'Не удолось сохранить комментарий',
	'Comment #{id} not found.' => 'Коментарий #{id} не найден.',
	'{city} is already in your list' => '{city} уже добавлен в ваш список',
	'Add city error' => 'Не удалось добавить город',
	'City ID #{id} not found' => 'Город с ID #{id} не найден',
	'Region not found' => 'Регион не найден',

	'USD' => 'RUB',
	'Balance recharge on Zengram.net' => 'Пополнение счета Zengram.ru',
	'Balance recharge on Zengram.net for #{user}' => 'Пополнение счета Zengram.ru для #{user}',
	'{value} days on Zengram.net' => '{value} дней на Zengram.ru',
	'{value} days on Zengram.net for #{user}' => '{value} дней на Zengram.ru для #{user}',

	'There is no user with this email' => 'Пользователь с таким email не найден',
	'Password reset error' => 'Ошибка смены пароля',
	'desktop' => 'компьютер',
	'smartphone' => 'смартфон',
	'tablet' => 'планшет',
	'Promo action reply' => 'Отзыв по акции',
	'ideas' => 'идеи',
];