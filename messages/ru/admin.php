<?php
return [
	'Project successful start' => 'Проект успешно запущен',
	'Project successful stop' => 'Проект успешно остановлен',
	'Project successful restart' => 'Проект успешно перезапущен',
];