<?php return [
	// Registration
	'You’ve successfully signed up' => 'Вы успешно зарегистрировались на сайте Zengram.ru',

	'Hello. We are pleased that your project account @{account} has passed authentication and is now working to promote your account.' => 'Привет. Мы рады, что ваш проект @{account} прошел авторизацию и теперь работы по продвижению вашего аккаунты начались.',

	'Congratulations!' => 'Поздравляем!',
	'You successfully sign up at Zengram.net.' => 'Вы успешно зарегистрировались на сайте Zengram.ru.',
	'For an authorization at the site use next data:' => 'Для авторизации на сайте используйте данные введёные при регистрации:',
	'E-mail: {mail}' => 'E-mail: {mail}',
	'Password: {password}' => 'Пароль: {password}',

	// Password reset
	'Password reset' => 'Сброс пароля на аккаунте',

	'Hello.' => 'Здравствуйте.',
	'Your account password was reseted' => 'По вашему запросу был сброшен пароль от вашего аккаунта.',
	'New password: {password}' => 'Новый пароль: {password}',
	'Use new password to log in. You can change the password in account settings.' => 'Используйте новый пароль для входа в аккаунт. Пароль можно сменить в настройках аккаунта.',

	// Errors subject
	'Zengram.net. Need your attention' => 'Zengram.ru. Требуется ваше участие',
	'Balance during idle time will not be charged.' => 'Баланс во время простоя работ списываться не будет.',
	// Checkpoint error
	'Hello, {login}' => 'Уважаемый {login}',
	'We inform you, that works on your account {login} are currently stopped.' => 'Сообщаем Вам, что в настоящий момент работы по вашему аккаунту {login} остановлены.',
	'For further work you should go to the Instagram site or Instagram app on your mobile device. You will see a form of account verification:' => 'Для дальнейшей работы Вам необходимо зайти на сайт инстаграма или в приложение инстаграм со своего мобильного устройства. Вы увидите такую форму подтверждения аккаунта:',
	'Confirm your account and run your project again.' => 'Подтвердите аккаунт и запустите Ваш проект повторно.',

	// Password error
	'Hello, {login}.' => 'Здравствуйте, {login}.',
	'We inform you, that works on your account ({login}) are currently stopped due to incorrect password.' => 'Сообщаем Вам, что в настоящий момент работы по вашему аккаунту ({login}) остановлены в связи с ошибкой пароля.',
	'For further work you should go to the account settings and re-enter the password to your account.' => 'Для дальнейшей работы Вам необходимо зайти в настройки аккаунта и заново ввести пароль к своему аккаунту.',

	// Bad geo
	'We inform you, that you have chosen Instagram cities with very few users media.' => 'Сообщаем Вам, что по выбранным Вами городам в инстаграме крайне мало фотографий пользователей.',
	'To speed up the promotion of your account, we recommend choosing additional cities.' => 'Для увеличения скорости раскрутки вашего аккаунта рекомендуем выбрать дополнительные города.',

	// Bad hashtags
	'We inform you, that you have chosen Instagram hashtags with very few users media.' => 'Сообщаем Вам, что по выбранным Вами хештегам в инстаграме крайне мало фотографий пользователей.',
	'To speed up the promotion of your account, we recommend choosing additional hashtags.' => 'Для увеличения скорости раскрутки вашего аккаунта рекомендуем выбрать дополнительные хештеги.',

	// Low balance
	'Zengram. Balance come to the end' => 'Zengram. Заканчиваются средства',

	'Hello!' => 'Здравствуйте!',
	'On your account at the zengram.net service means come to the end. We recommend you to %s the balance right now.' => 'На вашем аккаунте на сервисе zengram.ru заканчиваются средства. Рекомендуем пополнить баланс прямо сейчас.',

	// No balance
	'Zengram. Not enough funds' => 'Zengram. Закончились средства',

	'On your account for service zengram.net not enough funds. Your projects are temporary stopped.' => 'На вашем аккаунте на сервисе zengram.ru закончились средства. Ваши проекты временно остановлены.',
	'For futher use, please recharge balance.' => 'Для продолжения пополните счет.',
];