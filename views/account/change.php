<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\forms\AccountUpdateForm */

$this->title = Yii::t('views', 'Add account');
//$this->params['breadcrumbs'][] = $this->title;
?>
<div id="change-form-content">
	<?php $form = ActiveForm::begin([
		'id' => 'changeAccountForm',
		'options' => ['class' => 'form-horizontal'],
		'fieldConfig' => [
			'template' => "{label}\n<div class=\"col-lg-12\">{input}</div>\n<div class=\"col-lg-12\">{error}</div>",
			'labelOptions' => ['class' => 'col-lg-2 control-label'],
		],
		'validateOnBlur' => false,
		'validateOnType' => false,
		'validateOnChange' => false,
		'validateOnSubmit' => true,
		'enableAjaxValidation' => true,
		'validationUrl' => ['account/ajax-validate']
	]); ?>

	<?= Html::activeHiddenInput($model, 'id') ?>
	
	<h2 class="change-password-title text-center"><?= Yii::t('views', 'Login and password settings') ?></h2>

	<div class="alert alert-danger" style="display: none">
	</div>
	
	<?= $form->field($model, 'login') ?>

	<?= $form->field($model, 'password')->passwordInput(['placeholder' => Yii::t('views', 'Enter new password')]) ?>

	<div class="form-group">
		<div class="col-lg-12">
			<?= Html::submitButton(Yii::t('app', Yii::t('views', 'Save settings')), ['class' => 'btn btn-danger', 'name' => 'login-button']) ?>
		</div>
	</div>

	<?php ActiveForm::end(); ?>
</div>