<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<style>
h1 {
	font-size: 120%;
	font-weight: bold;
	margin-bottom: 1em;
}
p {
	margin-bottom: .5em;
}
</style>
<div class="site-error">

	<h1><?= Html::encode($this->title) ?></h1>

	<?php if (!empty($message)) : ?>
	<div class="alert alert-danger">
		<?= nl2br(Html::encode($message)) ?>
	</div>
	<?php endif; ?>

	<p><?= Yii::t('views', 'The above error occurred while the Web server was processing your request.') ?></p>
	<p><?= Yii::t('views', 'Please contact us if you think this is a server error. Thank you.') ?></p>

</div>
