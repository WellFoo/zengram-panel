<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = $title;
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page">
    <h1><?= Html::encode($title) ?></h1>

    <?=$text; ?>
</div>
