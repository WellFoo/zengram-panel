<?php
use yii\web\View;

$this->registerJsFile('@web/js/jquery.Jcrop.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.ui.widget.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.iframe-transport.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.fileupload.js', ['position' => View::POS_BEGIN]);
$this->registerCssFile('@web/css/jquery.Jcrop.min.css', ['position' => View::POS_BEGIN]);
?>
<input id="fileupload" type="file" name="file" data-url="/test/upload">
<div class="progress" style="display: none">
	<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
</div>
<div id="imageslider"></div>
<input id="tester" type="text">
<button id="submit">Test</button>
<br>
<div id="image-wrapper" style="height: 700px; width: 700px; overflow: auto; border:1px solid black;">
<img src="img/no_image_available.png" id="imageId"/>
</div>
<script>
	$(document).ready(function ()
	{
		var jcrop_api,
			crop_x, crop_y, crop_width, crop_height,
			image_width, image_height, image_src,
			image = $('#imageId');
		var initJcrop = function(){
			if (typeof jcrop_api != 'undefined') {
				jcrop_api.destroy();
			}
			image.Jcrop({
				minSize: [50, 50],
//				maxDrag: [700, 700],
				bgColor: 'FF0000',
				drawBorders: false,
				bgOpacity: 0.4,
				setSelect: [15, 15, 685, 685],
				selection: true,
				theme: 'light',
				aspectRatio: 1,
				onSelect: function(coordinates){
					crop_x = coordinates.x;
					crop_y = coordinates.y;
					crop_width = coordinates.w;
					crop_height = coordinates.h;
				},
				onChange: function(coordinates){
					crop_x = coordinates.x;
					crop_y = coordinates.y;
					crop_width = coordinates.w;
					crop_height = coordinates.h;
				}
			}, function() {
				jcrop_api = this;
			});
		};
		$('#fileupload').fileupload({
			done: function (e, data)
			{
				$('.progress').hide();
				if (data.result.result){
					image.attr('src', data.result.src);
					image_src = data.result.src;
					initJcrop();
				} else {
					alert(data.result.error);
				}
			},
			disableImageResize: true,
			add: function (e, data)
			{
				if (data.autoUpload || (data.autoUpload !== false &&
					$(this).fileupload('option', 'autoUpload'))) {
					$('.progress').show();
					data.process().done(function ()
					{
						data.submit();
					});
				}
			},
			acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
			progressall: function (e, data)
			{
				var progress = parseInt(data.loaded / data.total * 100, 10);
				$('.progress .progress-bar').css(
					'width',
					progress + '%'
				).text(progress + '%');
			}
		});
		$("#imageslider").slider({
			value: 100,
			max: 100,
			min: 1,
			slide: function(event, ui) {
				var value = $('#imageslider').slider('option', 'value');
				var width = image.width() * (value / 100);
				var height = image.height() * (value / 100);
				jcrop_api.resizeImage(width, height);
				image_width = width;
				image_height = height;
				$('#tester').val("org: "+image.width()+" now: "+width);
			},
			stop: function(){
				jcrop_api.updateSelection();
			}
		});
		$('#submit').on('click', function(){
			$.post('/test/crop', {
				image_src: image_src,
				crop_x: crop_x,
				crop_y: crop_y,
				crop_width: crop_width,
				crop_height: crop_height,
				image_width: image_width,
				image_height: image_height
			}, function(data){
				console.log(data);
			});
			console.log([crop_x, crop_y, crop_width, crop_height, image_width, image_height]);
		})
	})
</script>
<style>
	.jcrop-holder > div > div > .jcrop-tracker {
		background: linear-gradient(rgba(217, 217, 217, 0.7), transparent 2.5px), linear-gradient(90deg, rgba(217, 217, 217, 0.7), transparent 2.5px);
		background-size: 33.34% 33.34%;
		background-position: center center;
		border: 1px solid black;
	}
</style>
