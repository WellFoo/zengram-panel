<?php namespace app\widgets;

use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class Tree extends Widget
{
	private static $CSS = <<<CSS
.simple-tree-view {
	font-size: 12pt;
	list-style: none;
	padding: 0;
}
.simple-tree-view ul {
	list-style: none;
	padding-left: 30px;
}
.simple-tree-view li {
	padding: 4px 10px;
	border-radius: 3px;
}
.simple-tree-view li:hover {
	background: rgba(76, 175, 80, .15);
}
.simple-tree-view .add {
	display: none;
}
.simple-tree-view li:hover .add {
	display: inherit;
}
.simple-tree-view a[data-toggle] span {
	margin-right: 10px;
	transform: rotate(90deg);
	transition: all .2s;
}
.simple-tree-view a.collapsed span {
	transform: rotate(0deg);
}
.simple-tree-view .gag {
	margin-right: 26px;
}
.simple-tree-view a {
	cursor: pointer;
}
.simple-tree-view label {
	margin-bottom: 0;
}
CSS;

	private static $_id = 0;
	private static $_iterator = 0;
	private $renderItem;

	public $id;
	public $items = [];
	public $valueCallback;
	public $input;
	/* @var $model \yii\base\Model */
	public $model;
	public $attribute;

	public function init()
	{
		parent::init();

		$this->getView()->registerCss(self::$CSS);

		if ($this->id === null) {
			$this->id = self::$_id++;
		}

		if ($this->valueCallback) {
			$this->renderItem = $this->valueCallback;
		} elseif ($this->input !== null) {
			$type = ArrayHelper::getValue($this->input, 'type', 'checkbox');
			$name = $this->getInputName();
			$ids = $this->model->attributes[$this->attribute];
			if ($type === 'checkbox') {
				$name .= '[]';
			}
			$this->renderItem = function ($item) use ($type, $name, $ids) {
				$html = Html::beginTag('label');
				$html .= Html::input($type, $name, $item['id'], [
						'checked' => in_array($item['id'], $ids)
					]) . PHP_EOL;
				$html .= $item['label'];
				$html .= Html::endTag('label');
				return $html;
			};
		} else {
			$this->renderItem = function ($item) {
				return $item['label'];
			};
		}
	}

	public function run()
	{
		echo $this->renderTree($this->items);
	}

	private function renderTree($data, $root = true)
	{
		$callback = $this->renderItem;
		$list = '';
		foreach ($data as $item) {
			$list .= Html::beginTag('li');

			$index = 'tree-widget-branch-' . self::$_iterator++;
			if (isset($item['items'])) {
				$list .= Html::a(
					Html::tag('span', '', ['class' => 'glyphicon glyphicon-triangle-right']),
					'#' . $index,
					[
						'data-toggle' => 'collapse',
						'class' => 'collapsed text-success'
					]
				);
			} else {
				$list .= Html::tag('span', '', ['class' => 'gag']);
			}

			$list .= $callback($item);

			$list .= Html::endTag('li');

			if (isset($item['items'])) {
				$list .= Html::tag(
					'div',
					$this->renderTree($item['items'], false),
					[
						'id' => $index,
						'class' => 'collapse'
					]
				);
			}
		}
		return Html::tag('ul', $list, [
			'id' => $root ? $this->id : '',
			'class' => $root ? 'simple-tree-view' : ''
		]);
	}

	private function getInputName()
	{
		$name = ArrayHelper::getValue($this->input, 'name', null);
		if ($name !== null) {
			return $name;
		}
		if ($this->model && $this->attribute) {
			return Html::getInputName($this->model, $this->attribute);
		}
		return 'tree-view-' . $this->id;
	}
}