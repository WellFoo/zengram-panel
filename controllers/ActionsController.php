<?php

namespace app\controllers;

use app\components\RPCHelper;
use app\components\YiiImagick;
use app\models\Account;
use app\models\Actions;
use app\components\Controller;
use app\models\ActionsLog;
use app\models\ActionsSliders;
use app\models\BalanceFlow;
use app\models\Users;
use app\models\UsersLog;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\web\Response;

class ActionsController extends Controller
{
	public function beforeAction($action)
	{
		if (Yii::$app->user->isGuest) {
			Yii::$app->response->redirect(['/#loginModal']);
		}

		return parent::beforeAction($action);
	}

	public function actionIndex()
	{
		/** @var Users $user */
		$user = Yii::$app->user->identity;

		$accounts = Account::find()->where([
			'and',
			['user_id' => $user->id],
			['>', 'account_followers', 50],
			['not in', 'instagram_id', (new Query())
				->select(['instagram_id'])
				->from('actions_log')
				->where(['and', ['deleted' => 0], ['>=', 'added', new Expression("(NOW() - '" . Actions::CHECK_DAYS . " day'::interval)")]])
			]
		])->all();

		return $this->render('index', [
			'accounts' => $accounts,
			'count' => (int) Actions::find()->where(['data' => 'photo'])->count(),
			'images' => ActionsSliders::find()->all(),
			'price' => $this->countPrice($accounts)
		]);
	}

	/**
	 * @param Account[] $accounts
	 * @return int
	 */
	private function countPrice($accounts)
	{
		$price = 0;
		// сделал вывод по ходу массива, чтобы не было теоритического переполнения по значению int.
		foreach ($accounts as $account) {
			if ($account->account_followers > 9000){
				return 86400*Actions::MAX_DAYS;
			}
			//каждые 100 подписчиков = 8 часов
			$account_value = floor($account->account_followers/100)*28800;
			//если меньше, чем сутки - ставим сутки
			if ($account_value < 86400){
				$account_value = 86400;
			}
			//если набрали, больше, чем Actions::MAX_DAYS суток, то возвращаем Actions::MAX_DAYS суток
			if ($account_value > 86400*Actions::MAX_DAYS){
				return 86400*Actions::MAX_DAYS;
			}
			$price += $account_value;
			if ($price > 86400*Actions::MAX_DAYS){
				return 86400*Actions::MAX_DAYS;
			}
		}
		return $price;
	}

	public function actionCountPrice()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		return ['price' => Account::getStaticTimerText($this->countPrice(Account::find()->where(['id' => Yii::$app->request->get('accounts', [])])->all()), true)];
	}

	public function actionPost()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		$results = [];

		/** @var Account[] $accounts */
		$accounts = Account::find()->where(['id' => Yii::$app->request->post('accounts', [])])->all();

		$image_id = Yii::$app->request->post('image-id', false);
		if (!$image_id){
			return ['error' => Yii::t('content', 'No image found')];
		}

		/** @var ActionsSliders $actionImage */
		$actionImage = ActionsSliders::find()->where(['id' => $image_id])->one();
		if ($actionImage === null || !file_exists(Yii::$app->basePath.$actionImage->path)){
			return ['error' => Yii::t('content', 'No image found')];
		}

		$cost = 0;
		foreach ($accounts as $account) {
			if (!$account->actionAvaible){
				$nextDate = (new Query())
					->select(['date' => new Expression('DATE_ADD(added, INTERVAL ' . Actions::CHECK_DAYS . ' DAY)')])
					->from('actions_log')
					->where(['and', ['instagram_id' => $account->instagram_id], ['>=', 'added', new Expression("(NOW() - '" . Actions::CHECK_DAYS . " day'::interval)")]])
					->orderBy('id DESC')
					->one();
				if ($nextDate) {
					$results[$account->id]['error'] = Yii::t('app', 'Next submition will be avaible on {next_date}',
						['next_date' =>Yii::$app->formatter->asDatetime($nextDate['date'])]);
					continue;
				}
			}
			/** @var \app\components\YiiImagick $image */
			$image = new YiiImagick(Yii::$app->basePath.$actionImage->path);
			$originalSize = [$image->getImageWidth(), $image->getImageHeight()];
			if ($originalSize[0] != $originalSize[1]) {
				$min_side = $originalSize[0] < $originalSize[1] ? $originalSize[0] : $originalSize[1];
				$image->crop($min_side, $min_side, 0, 0);
			}
			if ($image->getImageWidth() > 1080){
				$image->resize(1080, 1080);
			}

			if ($image->getImageWidth() < 320){
				$image->resize(320, 320);
			}

			$image->placeRandomWatermark();

			$quality = mt_rand(90,99);
			$rpcResult = RPCHelper::sendImage(
				$account,
				base64_encode($image->render('jpeg', $quality)),
				Yii::t('app', 'I am promoting my account in Instagram using zengram.net. I was glad for 3 free days, which is presenting to everyone'),
				[
					'size' => $originalSize,
					'quality' => $quality,
					'crop' => [0, 0],
					'zoom' => number_format(1, 2, '.', '')
				]
			);
			$results[$account->id] = $rpcResult;

			if (empty($rpcResult['status']) || $rpcResult['status'] !== 'ok') {
				$results[$account->id]['error'] = Yii::t('app', 'Upload photo error. Please try again later.');
				continue;
			}

			$sendResult = $rpcResult['data'];
			if (empty($sendResult['result']) || $sendResult['result'] != 'success' || empty($sendResult['media_code'])){
				$results[$account->id]['error'] = Yii::t('app', 'Upload photo error. Please try again later.');
				continue;
			}

			$account_cost = $this->countPrice([$account]);
			$cost += $account_cost;

			$actionsLog = new ActionsLog([
				'instagram_id' => $account->instagram_id, 'user_id' => $account->user_id, 'image_id' => $image_id,
				'media_code' => strval($sendResult['media_code']), 'cost' => $account_cost
			]);
			$actionsLog->save();
//			$account->posted_photos++;
//			$account->save();
			$results[$account->id] = $sendResult;
		}

		//по условиям, минимум 1 день
		if ($cost < 86400){
			$cost = 86400;
		}

		/* @var $user Users */
		$user = Users::findOne(['id' => Yii::$app->user->id]);
		$user->balance += $cost;
		$user->save();
		$bf = new BalanceFlow([
			'user_id' => Yii::$app->user->id,
			'value'=> $cost,
			'description' => 'Акция зенграм время за пост',
			'type' => 'add'
		]);
		$bf->save();

		$results['text'] = Yii::t('app', 'Thanks for your recommendation! You have {cost} free', ['cost' => $cost ? Account::getStaticTimerText($cost) : '1 day']);
		return $results;
	}

	public function actionShare()
	{
		if(!isset($_POST['social'])) {
			return;
		}

		$social = $_POST['social'];
		$action = Actions::findOne(['user_id' => Yii::$app->user->id]);
		if ($action == null || !$action->id) {
			// Заносим клиента в список участников акции
			$action = new Actions();
			$action->user_id = Yii::$app->user->id;
			$action->mail = Yii::$app->user->identity->mail;
			$action->datetime = date("Y-m-d H:i:s");
			$action->data = $social;
			$action->save();

			echo 'ok';
			sleep(60);
			// Начисляем клиенту его бонус
			Yii::$app->db->createCommand("UPDATE users SET balance = balance + 86400 WHERE id = '".Yii::$app->user->id."'")->execute();
		}
	}

	public function actionClose()
	{
		$action = Actions::findOne(['user_id' => Yii::$app->user->id]);
		if ($action === null || !$action->id) {
			$action = new Actions();
			$action->user_id = Yii::$app->user->id;
			$action->mail = Yii::$app->user->identity->mail;
			$action->datetime = date("Y-m-d H:i:s");
			$action->data = 'close';
			$action->save();
			$logEntry = new UsersLog(['user_id' => Yii::$app->user->id, 'text' => 'Отказ от акции']);
			$logEntry->save();
			return 'ok';
		}
		return '';
	}
}