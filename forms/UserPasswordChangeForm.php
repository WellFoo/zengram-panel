<?php

namespace app\forms;

use app\models\Users;
use yii\base\Model;
use Yii;

class UserPasswordChangeForm extends Model
{
	public $email;
	public $old_password;
	public $new_password;

	public function init()
	{
		$this->email = Yii::$app->user->identity->mail;
		parent::init();
	}

	public function attributeLabels()
	{
		return [
			'email'        => Yii::t('app', 'E-mail'),
			'old_password' => Yii::t('app', 'Old password'),
			'new_password' => Yii::t('app', 'New password')
		];
	}

	public function rules()
	{
		return [
			[['email', 'old_password'], 'required'],
			['email', 'email', 'message' => Yii::t('app', 'Enter correct E-Mail.')],
			['email', 'validateMail'],
			['old_password', 'validatePassword'],
			['new_password', 'string', 'min' => 6]
		];
	}

	public function validateMail($attribute, $params)
	{
		if (Yii::$app->user->identity->mail != $this->email) {
			$owner = Users::find()->where(['mail' => $this->email])->one();
			if ($owner !== null) {
				$this->addError($attribute, Yii::t('app', 'E-Mail already used.'));
			}
		}
	}

	public function validatePassword($attribute, $params)
	{
		if (!Yii::$app->user->identity->validatePassword($this->old_password)) {
			$this->addError($attribute, Yii::t('app', 'Password incorrect'));
		}
	}

	public function save($runValidation = false)
	{
		/** @var Users $user */
		$user = Users::findOne(Yii::$app->user->id);
		$user->mail = $this->email;
		if (!empty($this->new_password)) {
			$user->password = md5($this->new_password);
		}
		return $user->save($runValidation);
	}
}
