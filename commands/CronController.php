<?php

namespace app\commands;

use app\components\Counters;
use app\components\RPCHelper;
use app\components\TaskController;
use app\models\Account;
use app\models\AccountActionLog;
use app\models\AccountProcessLog;
use app\models\AccountStats;
use app\models\Actions;
use app\models\ActionsLog;
use app\models\BalanceFix;
use app\models\BalanceFlow;
use app\models\Mail;
use app\models\Options;
use app\models\Proxy;
use app\models\UnfollowQueue;
use app\models\UsageStats;
use app\models\UsageStatsDetail;
use app\models\Users;
use app\models\UserTokens;
use app\models\Whitelist;
use core\logger\codes\ZengramCodes;
use core\logger\Logger;
use DateTime;
use PDO;
use ReflectionClass;
use Swift_RfcComplianceException;
use Swift_TransportException;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Console;
use yii\helpers\Url;
use Yii;

class CronController extends TaskController
{
	public function actionBalance()
	{
		$started = Account::getActiveAccounts();

		// Списание баланса
		$sql = sprintf('
			UPDATE "users" u
			SET balance = balance - s.minutes * 60, balance_flow = balance_flow + s.minutes
			FROM   (SELECT user_id, count(*) as minutes 
					FROM "accounts" 
					WHERE ("instagram_id" IN (%s) AND is_paused = 0)
					GROUP BY user_id) s
			WHERE u.id = s.user_id',
			implode(',',$started)
		);
		
		Yii::$app->db->createCommand($sql)->queryAll();
	}

	public function actionRevision() {

		$accounts = Account::find()
			->where(['auth_block' => 1])
			->all();

		var_dump(count($accounts));

		foreach ($accounts as $account) {
			RPCHelper::startAccount($account, 'cron/revision');
		}
	}

	public function actionBalanceFix()
	{
		self::log('Фиксирование баланса пользователей');

		$users = Users::find()
			->select([
				Users::tableName().'.id',
				Users::tableName().'.balance',
			])
			->addSelect(['accounts_count' => sprintf(
				'(SELECT COUNT(*) FROM %s WHERE %s = %s)',
				Account::tableName(),
				Account::tableName() . '.user_id',
				Users::tableName() . '.id'
			)])
			->asArray();

		$data = [];

		self::log('Выбрано пользователей: ' . $users->count());

		foreach ($users->each() as $user) {

			if ((int) $user['accounts_count'] === 0) {
				continue;
			}

			$data[] = [$user['id'], $user['balance'], date('Y-m-d')];
		}

		self::log('Пользователей, с кол-вом проектов больше 0: ' . count($data));

		if ((int) count($data) > 0) {
			Yii::$app->db->createCommand()->batchInsert(
				BalanceFix::tableName(), [
				'user_id',
				'balance',
				'date'
			], $data
			)->execute();
		}

		self::log('Batch-insert выполнен');
	}

	public function actionAccountsMonitor()
	{
		$started = Account::getActiveAccounts();
		$stopUsers = [];

		$accounts  = Account::find()
			->where(['instagram_id' => $started])
			->with(['user', 'options'])
			->orderBy([
				(rand(0, 1) ? 'id' : 'login') => (rand(0, 1) ? SORT_ASC : SORT_DESC)
			]);

		/** @var Account $account */
		foreach ($accounts->each() as $account) {

			if ($account->getTimer(300) <= 0) {
				$account->resetTimer();
				RPCHelper::stopAccount($account, 'cron/balance/timer');
				AccountProcessLog::log([
					'worker_ip' => ip2long($account->server),
					'instagram_id' => $account->instagram_id,
					'user_id' => $account->user_id,
					'code' => Logger::Z012_ACCOUNT_STOP_BY_TIMER,
				]);
			}

			/** @var Account $account */
			if ($account->is_paused) {
				continue;
			}

			if ($account->user->balance <= 86400) {
				/** @var Actions $action */
				$action = Actions::find()->where(['and', ['user_id' => $account->user->id], ['data' => 'close']])->one();
				if (!is_null($action)) {
					$action->delete();
				}
				$this->sendLowBalance($account->user);
			}

			if ($account->user->balance_flow >= 60) {
				$bf = new BalanceFlow([
					'user_id'     => $account->user->id,
					'value'       => $account->user->balance_flow * 60,
					'description' => 'Автоматическое снятие по планировщику. Пользователь ' . $account->login,
					'type'        => 'sub'
				]);
				$bf->save();
				$account->user->balance_flow = 0;
				$account->user->save();
			}

			if ($account->user->balance <= 0) {
				if (!array_key_exists($account->user->id, $stopUsers)) {
					if (!$account->user->is_payed){
						/** @var Actions $action */
						$action = Actions::find()->where(['and', ['user_id' => $account->user->id], ['data' => 'close']])->one();
						if (!is_null($action)) {
							$action->delete();
						}
					}
					$stopUsers[$account->user->id] = $account->user->getAttributes();
				}
			}

			if ($account->options->comment && $account->getCommentsCount() === 0) {
				$account->message = Account::EMPTY_COMMENTS;
				$account->save();
				RPCHelper::stopAccount($account, 'cron/balance/comments');
				AccountProcessLog::log([
					'worker_ip' => ip2long($account->server),
					'instagram_id' => $account->instagram_id,
					'user_id' => $account->user_id,
					'code' => Logger::Z012_ACCOUNT_STOP_BY_COMMENTS_ERROR,
				]);
			}

			if (!$account->options->unfollow) {
				if (($account->options->isSearchByPlaces() && $account->getPlacesCount() === 0) ||
					($account->options->isSearchByRegions() && $account->getRegionsCount() === 0)
				) {
					$account->message = Account::EMPTY_GEO;
					$account->save();
					RPCHelper::stopAccount($account, 'cron/balance/empty-geo');
					AccountProcessLog::log([
						'worker_ip' => ip2long($account->server),
						'instagram_id' => $account->instagram_id,
						'user_id' => $account->user_id,
						'code' => Logger::Z012_ACCOUNT_STOP_BY_GEO_ERROR,
					]);
				}

				if ($account->options->isSearchByHashtags() &&
					$account->getHashtagsCount() === 0
				) {
					$account->message = Account::EMPTY_HASHTAGS;
					$account->save();
				}
			}
		}

		foreach ($stopUsers as $stopUser) {
			$this->stopUser($stopUser);
		}
	}

	public function actionCheckMedia()
	{
		/** @var ActionsLog[] $actions */
		$actions = ActionsLog::find()->where([
			'and',
			['deleted' => 0],
			['>=', 'added', new Expression("(NOW() - '" . Actions::CHECK_DAYS . "' day'::interval)")]
		])->all();
		foreach ($actions as $action) {
			$ch = curl_init('https://instagram.com/p/'.$action->media_code);
			curl_setopt_array($ch, [
				CURLOPT_FOLLOWLOCATION => 1,
				CURLOPT_HEADER => 1,
				CURLOPT_NOBODY => 1,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_SSL_VERIFYHOST => 0,
				CURLOPT_SSL_VERIFYPEER => 0,
				CURLOPT_TIMEOUT        => 20,
			]);
			curl_exec($ch);
			$info = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			if ($info > 400 && $info < 500){
				$this->stdout('Media '.$action->media_code.': '.Console::ansiFormat('not found', [Console::FG_RED]).PHP_EOL);
				/* @var $user Users */
				$user = Users::find()->where(['id' => $action->user_id])->one();
				if (!$user){
					continue;
				}
				$user->balance -= $action->cost;
				$user->save();
				$bf = new BalanceFlow([
					'user_id' => $user->id,
					'value'=> $action->cost,
					'description' => 'Акция зенграм время за пост удалена',
					'type' => 'sub'
				]);
				$bf->save();
				$action->deleted = 1;
				$action->save();
			} else {
				$this->stdout('Media ' . $action->media_code . ': ' . Console::ansiFormat('exists', [Console::FG_GREEN]) . PHP_EOL);
			}
		}
	}

	public function actionUpdateStats()
	{
		$weekAccountsQuery = AccountStats::find()
			->select([
				new Expression('DISTINCT ' . AccountStats::tableName() . '.instagram_id as instagram_id'),
			])
			->orWhere(['!=', AccountStats::tableName() . '.likes', 0])
			->orWhere(['!=', AccountStats::tableName() . '.comments', 0])
			->orWhere(['!=', AccountStats::tableName() . '.follows', 0])
			->orWhere(['!=', AccountStats::tableName() . '.unfollows', 0])
			->andWhere(['>=', 'date', new Expression("(NOW() - '7 day'::interval)")])
			->asArray()->all();
		$weekAccounts = ArrayHelper::getColumn($weekAccountsQuery, 'instagram_id');
		$order = rand(0, 1);
		$query = Account::find()
			->andWhere(['instagram_id' => $weekAccounts])
			->orWhere(['>=', 'added', new Expression("(NOW() - '7 day'::interval)")])
			->orderBy(['id' => $order ? SORT_ASC : SORT_DESC]);
		$multicurlHandle = curl_multi_init();
		foreach ($query->batch(10) as $accounts) {
			/* @type Account[] $accounts */
			$curls = [];
			foreach ($accounts as $key => $account) {
				if ($account->message === Account::PASSWORD_ERROR || $account->message === Account::CHECKPOINT_ERROR) {
					continue;
				}
				// TODO надесюсь всё правльно подставил
				if (empty($account->server)){
					$account->server = RPCHelper::getIdleServer();
					if (!$account->isNewRecord) {
						$account->save();
					}
				}
				$curl = curl_init($account->server . '/zengramInfo.php?action=info');
				//$curl = curl_init(Yii::$app->params['serverUrl'] . '/zengramInfo.php?action=info');
				curl_setopt($curl, CURLOPT_HEADER, false);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_POST, true);
				curl_setopt($curl, CURLOPT_TIMEOUT, 10);
				curl_setopt($curl, CURLOPT_POSTFIELDS, $account->getPostInfoFields($account->user_id));
				$curls[$key] = $curl;
				curl_multi_add_handle($multicurlHandle, $curl);
			}

			$running = null;
			do {
				curl_multi_exec($multicurlHandle, $running);
			}
			while ($running > 0);
			// Get content and remove handles.
			foreach ($curls as $key => $curl) {
				$result = curl_multi_getcontent($curl);
				preg_match("#{.*}#", $result, $matches);
				if ($matches) {
					$result = $matches[0];
				}
				$accountInfo = json_decode($result, true);
				curl_multi_remove_handle($multicurlHandle, $curl);
				$account = $accounts[$key];
				$strAcc = Console::ansiFormat(str_pad($account->id, 11, ' '), [Console::BOLD]);
				if (empty($accountInfo['user'])){
					$this->stdout('Account ' . $strAcc . ' update result: ' . Console::ansiFormat('Error', [Console::FG_RED]) . PHP_EOL);
					continue;
				}
				$result_update = $account->updateStatsFromResult($accountInfo['user']);
				$strRes = Console::ansiFormat($result_update ? 'Success' : 'Error', [$result_update ? Console::FG_GREEN : Console::FG_RED]);
				$this->stdout('Account ' . $strAcc . ' update result: ' . $strRes . PHP_EOL);
			}
		}
	}

	public function actionOverallStats()
	{

	}

	public function actionCalcActionTime()
	{
		$date_to = date('Y-m-d');
		$date_from = date('Y-m-d', strtotime($date_to) - 86400);

		$query = AccountStats::find()
			->where(['date' => $date_from]);

		/** @var AccountStats $account_stat */
		foreach ($query->each() as $account_stat) {
			$result = AccountActionLog::getActionsTime($account_stat, $date_from, $date_to);

			echo $account_stat->account_id . PHP_EOL;
			foreach ($result as $action => &$time) {
				$time_filed = $action . '_time';
				echo $action . ' - ' . $time . PHP_EOL;
				if (!isset($account_stat->$time_filed)) {
					continue;
				}

				$account_stat->$time_filed = $time;
			}
			echo PHP_EOL;

			$account_stat->save();
		}
	}

	public function actionUpdateMedia()
	{
		set_time_limit(7200);
		file_put_contents(Yii::getAlias('@app/cron.log'), date('H:i:s d-m-Y').'; cron/update-media start'.PHP_EOL, FILE_APPEND);
		/** @var array $result */
		$result = Yii::$app->db->createCommand(
			"SELECT a.user_id
 			FROM accounts a
 			INNER JOIN users u ON a.user_id = u.id
 			WHERE a.monitoring_status = 'work' AND u.balance > 0
 			GROUP BY a.user_id;"
		)->query();
		$users = ArrayHelper::getColumn($result, 'user_id');
		$query = Account::find()->where(['user_id' => $users])->with('user');
		$i = 1;
		foreach ($query->each() as $account) {
			/* @type \app\models\Account $account */
			if ($account->message === Account::PASSWORD_ERROR || $account->message === Account::CHECKPOINT_ERROR) {
				continue;
			}

			/* @type \app\models\Account $account */
			RPCHelper::countComments($account);
			$this->stdout('Account @' . $account->login . ' updated' . PHP_EOL);

			if ($i++ % 20 === 0) {
				sleep(10);
			}
		}
		file_put_contents(Yii::getAlias('@app/cron.log'), date('H:i:s d-m-Y').'; cron/update-media complete'.PHP_EOL, FILE_APPEND);
	}

	public function actionCheckProxies()
	{
		/* @var $list Proxy[] */
		$list = Proxy::find()->all();

		$hasError = false;
		$errStr = Console::ansiFormat('Error: ', [Console::FG_RED]);
		foreach ($list as $proxy) {
			$proxy->updateStatus();
			if ($proxy->status === Proxy::STATUS_ERROR) {
				$strProxy = Console::ansiFormat($proxy->proxy, [Console::BOLD]);
				$this->stdout($errStr.'no response from '.$strProxy.PHP_EOL);
				$hasError = true;
			}
		}

		if ($hasError) {
			$this->spreadProxies();
		}
	}

	private function spreadProxies()
	{
		/* @var $list Proxy[] */
		$list = Proxy::find()->where(['status' => Proxy::STATUS_ERROR])->all();
		foreach ($list as $proxy) {
			if ($proxy->status === Proxy::STATUS_ERROR) {
				$this->spreadProxy($proxy);
			}
		}

		Yii::$app->mailer->compose()
			->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['company']])
			->setTo(Yii::$app->params['adminEmail'])
			->setSubject('Не работает прокси-сервер')
			->setHtmlBody(
				'Один или несколько <a href="'.Url::to(['admin/proxy'], 'https').'">прокси-серверов</a> вышли из строя:<br><br>'.
				implode('<br>', ArrayHelper::getColumn($list, 'proxy'))
			)->send();
	}

	/**
	 * @param $proxy Proxy
	 */
	private function spreadProxy($proxy)
	{
		$strProxy = Console::ansiFormat($proxy->proxy, [Console::BOLD]);
		$this->stdout('Try to spread clients from '.$strProxy.PHP_EOL);
		$errStr = Console::ansiFormat('Error: ', [Console::FG_RED]);
		/* @var $list Account[] */
		$list = Account::find()->where(['proxy_id' => $proxy->id])->andWhere(['monitoring_status' => Account::STATUS_WORK])->all();
		foreach ($list as $account) {
			$free = Proxy::getFree($proxy->cat_id);
			if ($free === null) {
				$this->stdout($errStr.' no working proxy available in cat #' . $proxy->cat_id . PHP_EOL);
				break;
			}

			$account->proxy_id = $free->id;
			RPCHelper::stopAccount($account, 'cron/spread-proxy');
			AccountProcessLog::log([
				'worker_ip' => ip2long($account->server),
				'instagram_id' => $account->instagram_id,
				'user_id' => $account->user_id,
				'code' => Logger::Z012_ACCOUNT_STOP_BY_SPREAD_PROXY,
			]);
			RPCHelper::startAccount($account, 'cron/spread-proxy');
			AccountProcessLog::log([
				'worker_ip' => ip2long($account->server),
				'instagram_id' => $account->instagram_id,
				'user_id' => $account->user_id,
				'code' => Logger::Z011_ACCOUNT_START_BY_SPREAD_PROXY,
			]);
		}
	}

	private function stopUser($user)
	{
		$this->stdout(Console::ansiFormat('Stop user: ', [Console::FG_YELLOW]).$user['mail'].PHP_EOL);
		$strAcc = Console::ansiFormat('Stop account: ', [Console::FG_YELLOW]);
		$accounts = Account::findAll(['user_id' => $user['id']]);
		foreach ($accounts as $account) {
			/* @var $account Account */
			$this->stdout($strAcc.$account->id.PHP_EOL);
			//$account->accountStop(true);
			RPCHelper::stopAccount($account, 'cron/stopUser');
		}
		if (!$user['is_payed']) {
			Counters::trigger('user_freeend');
			Counters::roiStatsSendEvent('free_end');
			Counters::triggerMeasurement('Окончание бесплатного периода', $user['id']);
		}
		$this->sendNoBalance($user);
	}

	private function sendLowBalance($user)
	{
		$this->stdout(Console::ansiFormat('Send low balance: ', [Console::FG_YELLOW]).$user['mail'].PHP_EOL);
		try {
			/* @var $mailFind Mail */
			$mailFind = Mail::find()->where(['mail' => $user['mail'], 'type' => 'lowbalance'])->one();
			if ($mailFind !== null) {
				return;
			}

			$token = new UserTokens();
			$token->generate($user['id']);
			$token->save();

			$caption = Yii::t('mails', 'Zengram. Balance come to the end');
			$body = [
				Yii::t('mails', 'Hello!'),
				Yii::t('mails', 'On your account at the zengram.net service means come to the end. We recommend you to %s the balance right now.')
			];

			Yii::$app->mailer->compose()
				->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['company']])
				->setTo($user['mail'])
				->setSubject($caption)
				->setTextBody(
					strip_tags(implode(PHP_EOL, $body))
				)->setHtmlBody(
					Yii::$app->controller->renderPartial('//layouts/mail_'.Yii::$app->language, [
							'caption' => $caption,
							'body' => $body,
							'token' => $token->token,
							'options' => ['price_url' => true]
						]
					)
				)->send();
			$mailModel       = new Mail();
			$mailModel->mail = $user['mail'];
			$mailModel->type = 'lowbalance';
			$mailModel->save();
		} catch (Swift_RfcComplianceException $e) {
			$this->stdout(Console::ansiFormat('Exception: ', [Console::FG_RED]) . $e->getMessage() . PHP_EOL);
		} catch (Swift_TransportException $e) {
			$this->stdout(Console::ansiFormat('Exception: ', [Console::FG_RED]) . $e->getMessage() . PHP_EOL);
		}
	}

	private function sendNoBalance($user)
	{
		try {
			/* @var $mailFind Mail */
			$mailFind = Mail::find()->where(['mail' => $user['mail'], 'type' => 'nobalance'])->one();
			if ($mailFind !== null) {
				return;
			}

			$token = new UserTokens();
			$token->generate($user['id']);
			$token->save();

			$caption = Yii::t('mails', 'Zengram. Not enough funds');
			$body = [
				Yii::t('mails', 'Hello!'),
				Yii::t('mails', 'On your account for service zengram.net not enough funds. Your projects are temporary stopped.'),
				Yii::t('mails', 'For futher use, please recharge balance.')
			];

			Yii::$app->mailer->compose()
				->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['company']])
				->setTo($user['mail'])
				->setSubject($caption)
				->setTextBody(
					implode(PHP_EOL, $body)
				)->setHtmlBody(
				Yii::$app->controller->renderPartial('//layouts/mail_'.Yii::$app->language, [
						'caption' => $caption,
						'body' => $body,
						'token' => $token->token,
						'options' => ['price_url' => true]
					]
				))->send();

			$mailModel       = new Mail();
			$mailModel->mail = $user['mail'];
			$mailModel->type = 'nobalance';
			$mailModel->save();
		} catch (Swift_RfcComplianceException $e) {
			$this->stdout(Console::ansiFormat('Exception: ', [Console::FG_RED]).$e->getMessage().PHP_EOL);
		} catch (Swift_TransportException $e) {
			$this->stdout(Console::ansiFormat('Exception: ', [Console::FG_RED]).$e->getMessage().PHP_EOL);
		}
	}


	public function actionProcessingUnfollow()
	{
		// Очистка неиспользованных отписок
		UnfollowQueue::deleteAll();

		$query = Account::find()
			->select([
				Account::tableName().'.id',
				Account::tableName().'.account_follows',
				Options::tableName().'.follows_limit_to_unfollow',
				Options::tableName().'.mutual',
			])
			->asArray()
			->leftJoin(Options::tableName(), Options::tableName().'.account_id = '.Account::tableName().'.id')
			->leftJoin(Users::tableName(), Account::tableName().'.user_id = '.Users::tableName().'.id')
			->where([
				Users::tableName().'.parallel_unfollows' => 1
			])
			->andWhere(Account::tableName().'.account_follows > '.Options::tableName().'.follows_limit_to_unfollow')
			->andWhere([
				'monitoring_status' => Account::STATUS_WORK
			])
			->andWhere([
				Options::tableName().'.follow' => 1
			])
		;

		if (!empty($this->argv[3])) {
			$query->andWhere([Account::tableName().'.instagram_id' => $this->argv[3]]);
		}

		self::log('Выбрано аккаунтов: '.$query->count());

		foreach ($query->each() as $data) {

			$count = (int) (($data['account_follows'] - $data['follows_limit_to_unfollow']) * 1.2);

			$time_increment = (int) (24 * 60 * 60 / ceil($count / 10));
			self::log('Интервал, сек: '.$time_increment);
			$time = time();

			// Получение списка подписок, от которых будем отписываться
			$account = Account::findOne($data['id']);

			self::log('Запрашиваемое количество отписок для аккаунта '.$account->login.': '.$count);

			$unfollows = $account->getTopUnfollows($count, $data['mutual']);

			self::log('Полученное количество отписок: '.count($unfollows));

			AccountProcessLog::log([
				'worker_ip' => ip2long($account->server),
				'instagram_id' => $account->instagram_id,
				'user_id' => $account->user_id,
				'code' => Logger::Z131_PARALLEL_INIT,
				'environment' => Logger::Z_ENV_PARALLEL_UNFOLLOWS,
				'description' => 'Обработка параллельных отписок. Интервал ' . $time_increment .
					' сек. Запрошено/получено: ' . $count . '/' . count($unfollows) .
					'. Текущее количество подписок: ' . $data['account_follows'],
			]);

			if (count($unfollows) < $count) $count = count($unfollows);

			$block_size = mt_rand(10, 30);
			self::log('Блок: '.$block_size);

			for ($i = 0; $i < $count; $i++) {

				if (empty($unfollows[$i])) break;

				$unfollow = $unfollows[$i];

				$queue = new UnfollowQueue([
					'account_id' => (string) $account->id,
					'unfollow_id' => (string) $unfollow,
					'date' => $time,
				]);

				if (!$queue->save()) {
					var_dump($queue->getErrors());
				}

				if (--$block_size <= 0) {
					$time += $time_increment;
					$block_size = mt_rand(10, 30);
					self::log('Блок: '.$block_size);
				}
			}

		}

	}

	public function actionFillWhitelist()
	{
		set_time_limit(0);

		/** @var Account $account */
		$account = Account::find()
			->where([
				'whitelist_added' => Whitelist::WHITELIST_EMPTY,
				'monitoring_status' => Account::STATUS_WORK
			])
			->orderBy(['id' => SORT_DESC])
			->one();

		if (is_null($account)) {
			self::log('Аккаунтов не найдено');
			return false;
		}

		self::log('Обработка аккаунта: '.$account->login);

		$account->whitelist_added = Whitelist::WHITELIST_FILLED;
		$account->save();

		if (!$account->fillWhitelist()) {
			self::log('Ошибка при обработке аккаунта: '.$account->login);
			$account->whitelist_added = Whitelist::WHITELIST_DELAY;
			$account->save();
		}

		return true;
	}

	public function actionCheckWhitelist()
	{
		// TODO: временный? костыль
		set_time_limit(0);

		$accounts = Account::find()
			->where([
				'whitelist_added' => Whitelist::WHITELIST_DELAY
			])
			->orderBy(['id' => SORT_DESC])
			->all();

		if (count($accounts) == 0) {
			self::log('Аккаунтов не найдено');
			return false;
		}

		/** @var Account $account */
		foreach ($accounts as $account) {
			self::log('Обработка аккаунта: '.$account->login);

			$account->whitelist_added = Whitelist::WHITELIST_FILLED;
			$account->save();

			if (!$account->fillWhitelist()) {
				self::log('Ошибка при обработке аккаунта: '.$account->login);
				$account->whitelist_added = Whitelist::WHITELIST_ERROR;
				$account->save();
			}
		}

		return true;
	}

	public function actionFillCameGone()
	{
		Yii::$app->db->createCommand(
			'INSERT INTO account_stats (account_id, date, trial, user_id, instagram_id, came, gone, mongo_came, mongo_gone, speed, proxy_id)
			(
				SELECT
					account_id,
					date,
					trial,
					user_id,
					instagram_id,
					came,
					gone,
					mongo_came,
					mongo_gone,
					1 AS speed,
					0 AS proxy_id
				FROM
					account_stats_server 
				WHERE
					date = date(now() - \'1 day\'::interval)
			)
			ON CONFLICT (account_id, date, trial) DO UPDATE SET
				came = EXCLUDED.came,
				gone = EXCLUDED.gone,
				mongo_came = EXCLUDED.mongo_came,
				mongo_gone = EXCLUDED.mongo_gone;'
		)->execute();
	}

	public function actionFillUsageStats()
	{
		$today = date('Y-m-') . '01';
		UsageStatsDetail::fillData($today);

		$query = UsageStatsDetail::find()->where(['date' => $today]);
		$sum = $query->sum('used');
		$count = $query->count();
		$avg = $count > 0 ? round($sum / $count) : 0;

		$date = new DateTime($today);
		$date->sub(new \DateInterval('P1M'));
		$query = Users::find()->where(['>=', 'regdate', $date->format('Y-m-d')]);
		$regs = $query->count();
		$payed = $query->andWhere(['is_payed' => 1])->count();

		$stat = new UsageStats([
			'date'      => $today,
			'usage_avg' => $avg,
			'usage_sum' => $sum,
			'regs'      => $regs,
			'trial'     => $regs - $payed,
			'payed'     => $payed,
		]);
		$stat->save();
	}
}
